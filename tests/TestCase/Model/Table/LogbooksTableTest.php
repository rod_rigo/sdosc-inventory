<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LogbooksTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LogbooksTable Test Case
 */
class LogbooksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LogbooksTable
     */
    protected $Logbooks;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Logbooks',
        'app.Users',
        'app.Supplies',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Logbooks') ? [] : ['className' => LogbooksTable::class];
        $this->Logbooks = $this->getTableLocator()->get('Logbooks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Logbooks);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\LogbooksTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\LogbooksTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
