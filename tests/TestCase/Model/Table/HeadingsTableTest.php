<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HeadingsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HeadingsTable Test Case
 */
class HeadingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HeadingsTable
     */
    protected $Headings;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Headings',
        'app.Users',
        'app.Signatories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Headings') ? [] : ['className' => HeadingsTable::class];
        $this->Headings = $this->getTableLocator()->get('Headings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Headings);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\HeadingsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\HeadingsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
