<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PersonelsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PersonelsTable Test Case
 */
class PersonelsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PersonelsTable
     */
    protected $Personels;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Personels',
        'app.Users',
        'app.Positions',
        'app.Custodians',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Personels') ? [] : ['className' => PersonelsTable::class];
        $this->Personels = $this->getTableLocator()->get('Personels', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Personels);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PersonelsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\PersonelsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
