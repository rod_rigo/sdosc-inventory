<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SuppliesFixture
 */
class SuppliesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'supply' => 'Lorem ipsum dolor sit amet',
                'stocks' => 1,
                'issued' => 1,
                'brand_id' => 1,
                'category_id' => 1,
                'unit_id' => 1,
                'account_id' => 1,
                'ledger_id' => 1,
                'inventory_number' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'is_expired' => 1,
                'expired_at' => '2024-06-23',
                'created' => 1719130417,
                'modified' => 1719130417,
                'deleted' => '2024-06-23 08:13:37',
            ],
        ];
        parent::init();
    }
}
