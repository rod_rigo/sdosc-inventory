<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ItemsFixture
 */
class ItemsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'custodian_id' => 1,
                'logical_id' => 1,
                'slip_id' => 1,
                'supply_id' => 1,
                'account_id' => 1,
                'legder_id' => 1,
                'office_id' => 1,
                'no' => 1,
                'year' => 1,
                'month' => 1,
                'quantity' => 1,
                'price' => 1,
                'serial_number' => 'Lorem ipsum dolor sit amet',
                'remarks' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'is_returned' => 1,
                'created' => 1719283444,
                'modified' => 1719283444,
                'deleted' => '2024-06-25 02:44:04',
            ],
        ];
        parent::init();
    }
}
