<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CategoriesFixture
 */
class CategoriesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'category' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'created' => 1719122642,
                'modified' => 1719122642,
                'deleted' => '2024-06-23 06:04:02',
            ],
        ];
        parent::init();
    }
}
