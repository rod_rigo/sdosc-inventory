<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LedgersFixture
 */
class LedgersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'account_id' => 1,
                'ledger' => 'Lorem ipsum dolor sit amet',
                'legend' => 'Lorem ipsum dolor sit amet',
                'code' => 'Lorem ipsum dolor sit amet',
                'sub_code' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'created' => 1719127379,
                'modified' => 1719127379,
                'deleted' => '2024-06-23 07:22:59',
            ],
        ];
        parent::init();
    }
}
