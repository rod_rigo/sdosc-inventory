<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PersonelsFixture
 */
class PersonelsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'position_id' => 1,
                'personels' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'created' => 1719124560,
                'modified' => 1719124560,
                'deleted' => '2024-06-23 06:36:00',
            ],
        ];
        parent::init();
    }
}
