<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PositionsFixture
 */
class PositionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'position' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'created' => 1719124137,
                'modified' => 1719124137,
                'deleted' => '2024-06-23 06:28:57',
            ],
        ];
        parent::init();
    }
}
