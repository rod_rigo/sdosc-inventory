<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ExportsFixture
 */
class ExportsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'export' => 'Lorem ipsum dolor sit amet',
                'mime' => 'Lorem ipsum dolor sit amet',
                'created' => 1719369341,
                'modified' => 1719369341,
                'deleted' => '2024-06-26 02:35:41',
            ],
        ];
        parent::init();
    }
}
