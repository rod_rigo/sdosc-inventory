<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DatabasesFixture
 */
class DatabasesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'filename' => 'Lorem ipsum dolor sit amet',
                'created' => 1719468609,
                'modified' => 1719468609,
                'deleted' => '2024-06-27 06:10:09',
            ],
        ];
        parent::init();
    }
}
