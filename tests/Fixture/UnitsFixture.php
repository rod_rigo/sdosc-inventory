<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UnitsFixture
 */
class UnitsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'unit' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'created' => 1719123201,
                'modified' => 1719123201,
                'deleted' => '2024-06-23 06:13:21',
            ],
        ];
        parent::init();
    }
}
