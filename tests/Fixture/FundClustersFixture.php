<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FundClustersFixture
 */
class FundClustersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'fund_cluster' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'created' => 1719647464,
                'modified' => 1719647464,
                'deleted' => '2024-06-29 07:51:04',
            ],
        ];
        parent::init();
    }
}
