<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LogbooksFixture
 */
class LogbooksFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'supply_id' => 1,
                'logbook' => 'Lorem ipsum dolor sit amet',
                'quantity' => 1,
                'old_stock' => 1,
                'total_stock' => 1,
                'is_strored' => 1,
                'created' => 1719466626,
                'modified' => 1719466626,
                'deleted' => '2024-06-27 05:37:06',
            ],
        ];
        parent::init();
    }
}
