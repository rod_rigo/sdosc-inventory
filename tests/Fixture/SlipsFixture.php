<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SlipsFixture
 */
class SlipsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'custodian_id' => 1,
                'account_id' => 1,
                'code' => 'Lorem ipsum dolor sit amet',
                'no' => 1,
                'month' => 1,
                'year' => 1,
                'created' => 1719283436,
                'modified' => 1719283436,
                'deleted' => '2024-06-25 02:43:56',
            ],
        ];
        parent::init();
    }
}
