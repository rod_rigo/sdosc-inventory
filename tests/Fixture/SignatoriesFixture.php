<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SignatoriesFixture
 */
class SignatoriesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'position_id' => 1,
                'heading_id' => 1,
                'signatory' => 'Lorem ipsum dolor sit amet',
                'order_position' => 1,
                'is_active' => 1,
                'created' => 1719385500,
                'modified' => 1719385500,
                'deleted' => '2024-06-26 07:05:00',
            ],
        ];
        parent::init();
    }
}
