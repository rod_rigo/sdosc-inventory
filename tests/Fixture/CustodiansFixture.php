<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CustodiansFixture
 */
class CustodiansFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'personel_id' => 1,
                'office_id' => 1,
                'no' => 'Lorem ipsum dolor sit amet',
                'year' => 1,
                'month' => 1,
                'code' => 1,
                'created' => 1719212676,
                'modified' => 1719212676,
                'deleted' => '2024-06-24 07:04:36',
            ],
        ];
        parent::init();
    }
}
