<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * HeadingsFixture
 */
class HeadingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'heading' => 'Lorem ipsum dolor sit amet',
                'total' => 1,
                'order_position' => 1,
                'is_active' => 1,
                'created' => 1719385432,
                'modified' => 1719385432,
                'deleted' => '2024-06-26 07:03:52',
            ],
        ];
        parent::init();
    }
}
