<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AccountsFixture
 */
class AccountsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'account' => 'Lorem ipsum dolor sit amet',
                'legend' => 'Lorem ipsum dolor sit amet',
                'code' => 'Lorem ipsum dolor sit amet',
                'start_price' => 1,
                'end_price' => 1,
                'is_active' => 1,
                'created' => 1719125567,
                'modified' => 1719125567,
                'deleted' => '2024-06-23 06:52:47',
            ],
        ];
        parent::init();
    }
}
