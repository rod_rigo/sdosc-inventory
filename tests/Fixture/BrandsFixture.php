<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BrandsFixture
 */
class BrandsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'brand' => 'Lorem ipsum dolor sit amet',
                'is_active' => 1,
                'created' => 1719122144,
                'modified' => 1719122144,
                'deleted' => '2024-06-23 05:55:44',
            ],
        ];
        parent::init();
    }
}
