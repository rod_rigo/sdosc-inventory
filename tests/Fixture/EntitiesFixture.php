<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EntitiesFixture
 */
class EntitiesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'entity' => 'Lorem ipsum dolor sit amet',
                'is_default' => 1,
                'is_active' => 1,
                'created' => 1719648240,
                'modified' => 1719648240,
                'deleted' => '2024-06-29 08:04:00',
            ],
        ];
        parent::init();
    }
}
