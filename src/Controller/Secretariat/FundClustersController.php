<?php
declare(strict_types=1);

namespace App\Controller\Secretariat;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * FundClusters Controller
 *
 * @property \App\Model\Table\FundClustersTable $FundClusters
 * @method \App\Model\Entity\FundCluster[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FundClustersController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('secretariat');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->FundClusters->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getFundClusters(){
        $data = $this->FundClusters->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->whereNull('FundClusters.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fundCluster = $this->FundClusters->newEmptyEntity();
        if ($this->request->is('post')) {
            $fundCluster = $this->FundClusters->patchEntity($fundCluster, $this->request->getData());
            $fundCluster->user_id = $this->Auth->user('id');
            if ($this->FundClusters->save($fundCluster)) {
                $result = ['message' => ucwords('The fund Cluster has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($fundCluster->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $fundCluster->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Office id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fundCluster = $this->FundClusters->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fundCluster = $this->FundClusters->patchEntity($fundCluster, $this->request->getData());
            $fundCluster->user_id = $this->Auth->user('id');
            if ($this->FundClusters->save($fundCluster)) {
                $result = ['message' => ucwords('The fund Cluster has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($fundCluster->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $fundCluster->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($fundCluster));
    }

}
