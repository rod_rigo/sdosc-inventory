<?php
declare(strict_types=1);

namespace App\Controller\Secretariat;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 * @method \App\Model\Entity\Category[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('secretariat');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Categories->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getCategories(){
        $data = $this->Categories->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ])
            ->whereNull('Categories.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $category = $this->Categories->newEmptyEntity();
        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            $category->user_id = $this->Auth->user('id');
            if ($this->Categories->save($category)) {
                $result = ['message' => ucwords('The category has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($category->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $category->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Office id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $category = $this->Categories->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            $category->user_id = $this->Auth->user('id');
            if ($this->Categories->save($category)) {
                $result = ['message' => ucwords('The category has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($category->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $category->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($category));
    }

}
