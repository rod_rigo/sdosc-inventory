<?php
declare(strict_types=1);

namespace App\Controller\Secretariat;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Moment\Moment;

/**
 * Logbooks Controller
 *
 * @property \App\Model\Table\LogbooksTable $Logbooks
 * @method \App\Model\Entity\Logbook[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LogbooksController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('secretariat');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

    }

    public function getLogbooks(){
        $startDate = ($this->request->getQuery('start_date'))? (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))? (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $limit = ($this->request->getQuery('limit'))? intval($this->request->getQuery('limit')): intval(10000);
        $data = $this->Logbooks->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Logbooks.created >=' => $startDate,
                'Logbooks.created <=' => $endDate
            ])
            ->order([
                'Logbooks.created' => 'DESC'
            ],true)
            ->limit(intval($limit));
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function today()
    {

    }

    public function getLogbooksToday(){
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $data = $this->Logbooks->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Logbooks.created >=' => $now,
            ])
            ->order([
                'Logbooks.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function week()
    {

    }

    public function getLogbooksWeek(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('week')->format('Y-m-d');
        $data = $this->Logbooks->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Logbooks.created >=' => $startDate,
                'Logbooks.created <=' => $endDate
            ])
            ->order([
                'Logbooks.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function month()
    {

    }

    public function getLogbooksMonth(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('month')->format('Y-m-d');
        $data = $this->Logbooks->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Logbooks.created >=' => $startDate,
                'Logbooks.created <=' => $endDate
            ])
            ->order([
                'Logbooks.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function year()
    {

    }

    public function getLogbooksYear(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $data = $this->Logbooks->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Logbooks.created >=' => $startDate,
                'Logbooks.created <=' => $endDate
            ])
            ->order([
                'Logbooks.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($supplyId = null)
    {
        $logbook = $this->Logbooks->newEmptyEntity();
        $supply = TableRegistry::getTableLocator()->get('Supplies')->get($supplyId);
        $isStored = [
            'Deduct',
            'Stored'
        ];
        $stocks = intval(0);
        if ($this->request->is('post')) {
            $logbook = $this->Logbooks->patchEntity($logbook, $this->request->getData(),[
                'validate' => false
            ]);
            $logbook->user_id = $this->Auth->user('id');
            $logbook->supply_id = intval($supply->id);

            $validator = new Validator();

            $validator
                ->numeric('user_id')
                ->requirePresence('user_id', true)
                ->notEmptyString('user_id', ucwords('please fill out this field'),false);

            $validator
                ->numeric('supply_id')
                ->requirePresence('supply_id', true)
                ->notEmptyString('supply_id', ucwords('please fill out this field'),false);

            $validator
                ->numeric('quantity')
                ->requirePresence('quantity', true)
                ->notEmptyString('quantity', ucwords('please fill out this field'),false)
                ->add('quantity', 'quantity',[
                    'rule' => function($value) use($supply){

                        if(!boolval($this->request->getData('is_stored')) && intval($value) > intval($supply->stocks)){
                            return ucwords('This Supply Has Only '. intval($supply->stocks).' To Pull Out');
                        }

                        return true;
                    }
                ]);

            $validator
                ->requirePresence('is_stored',true)
                ->notEmptyString('is_stored', ucwords('please fill out this field'),false)
                ->add('is_stored','is_stored',[
                    'rule' => function($value){

                        $isStored = [intval(0), intval(1)];
                        if(!in_array(intval($value), $isStored)){
                            return ucwords('invalid stored value');
                        }

                        return true;
                    }
                ]);

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            $logbook->old_stock = intval($supply->stocks);
            $logbook->logbook = strtoupper($isStored[intval($this->request->getData('is_stored'))]);

            if(boolval($this->request->getData('is_stored'))){
                $stocks = intval($supply->stocks) + intval($this->request->getData('quantity'));
            }else{
                $stocks = intval($supply->stocks) - intval($this->request->getData('quantity'));
            }

            $logbook->total_stock = intval($stocks);

            $supply->stocks = intval($stocks);

            if ($this->Logbooks->save($logbook) && TableRegistry::getTableLocator()->get('Supplies')->save($supply)) {
                $result = ['message' => ucwords('The logbook has been saved'), 'result' => ucwords('success'),
                    'redirect' => Router::url(['prefix' => 'Secretariat', 'controller' => 'Supplies', 'action' => 'index'])];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($logbook->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $logbook->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }

        $this->set(compact('logbook', 'supply'));
    }

}
