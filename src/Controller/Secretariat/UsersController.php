<?php
declare(strict_types=1);

namespace App\Controller\Secretariat;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('secretariat');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        $actions = [strtolower('account'), strtolower('changepassword')];
        if (in_array(strtolower($this->request->getParam('action')), $actions)) {

            $paramId = (int)$this->request->getParam('pass.0');
            if ($this->Users->isOwnedBy(intval($paramId), intval($user['id']))) {
                return true;
            }

            throw new ForbiddenException();
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    public function account($id = null)
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->token = uniqid().uniqid();
            if ($this->Users->save($user)) {
                $result = ['message' => ucwords('The account has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($user->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $user->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $positions = TableRegistry::getTableLocator()->get('Positions')
            ->find('list',[
                'valueField' => function($query){
                    return ucwords($query->position);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->whereNull('Positions.deleted')
            ->where([
                'Positions.is_active =' => intval(1)
            ])
            ->order([
                'Positions.position' => 'ASC'
            ],true);
        $this->set(compact('user', 'positions'));
    }

    public function changepassword($id = null)
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData(),[
                'validate' => false
            ]);

            $validator = new Validator();

            $validator
                ->requirePresence('confirm_password',true)
                ->notEmptyString('confirm_password', ucwords('please fill out this field'),false)
                ->sameAs('confirm_password','password', ucwords('password do not matched'));

            $validator
                ->maxLength('password', 255)
                ->requirePresence('password', true)
                ->notEmptyString('password', ucwords('please fill out this field'),false)
                ->add('password', 'password', [
                    'rule' => function($value){

                        $regex = '/^(.){6,}$/';

                        if(!preg_match_all($regex, $value)){
                            return ucwords('Password Must Have 6 Characters');
                        }

                        return true;
                    }
                ]);

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            $user->token = uniqid().uniqid();

            if ($this->Users->save($user)) {
                $result = ['message' => ucwords('The password has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($user->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $user->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

}
