<?php
declare(strict_types=1);

namespace App\Controller\Secretariat;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Moment\Moment;

/**
 * Slips Controller
 *
 * @property \App\Model\Table\SlipsTable $Slips
 * @method \App\Model\Entity\Slip[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SlipsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('secretariat');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && boolval($user['is_admin'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

    }

    public function getSlips(){
        $startDate = ($this->request->getQuery('start_date'))? (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($this->request->getQuery('end_date'))? (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $limit = ($this->request->getQuery('limit'))? intval($this->request->getQuery('limit')): intval(10000);
        $data = $this->Slips->find()
            ->contain([
                'Custodians.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Institutions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ])
            ->where([
                'Slips.created >=' => $startDate,
                'Slips.created <=' => $endDate
            ])
            ->whereNull('Slips.deleted')
            ->order([
                'Slips.created' => 'DESC'
            ],true)
            ->limit($limit);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function today()
    {

    }

    public function getSlipsToday(){
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $data = $this->Slips->find()
            ->contain([
                'Custodians.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Institutions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ])
            ->where([
                'Slips.created >=' => $now,
            ])
            ->whereNull('Slips.deleted')
            ->order([
                'Slips.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function week()
    {

    }

    public function getSlipsWeek(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('week')->format('Y-m-d');
        $data = $this->Slips->find()
            ->contain([
                'Custodians.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Institutions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ])
            ->where([
                'Slips.created >=' => $startDate,
                'Slips.created <=' => $endDate
            ])
            ->whereNull('Slips.deleted')
            ->order([
                'Slips.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function month()
    {

    }

    public function getSlipsMonth(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('month')->format('Y-m-d');
        $data = $this->Slips->find()
            ->contain([
                'Custodians.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Institutions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ])
            ->where([
                'Slips.created >=' => $startDate,
                'Slips.created <=' => $endDate
            ])
            ->whereNull('Slips.deleted')
            ->order([
                'Slips.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function year()
    {

    }

    public function getSlipsYear(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $data = $this->Slips->find()
            ->contain([
                'Custodians.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Institutions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ])
            ->where([
                'Slips.created >=' => $startDate,
                'Slips.created <=' => $endDate
            ])
            ->whereNull('Slips.deleted')
            ->order([
                'Slips.created' => 'DESC'
            ],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * View method
     *
     * @param string|null $id Slip id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $slip = $this->Slips->get($id, [
            'contain' => [
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Ledgers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ],
        ]);
        $personels = TableRegistry::getTableLocator()->get('Personels')
            ->find('list',[
                'valueField' => function($query){
                    return strtoupper($query->personel);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Personels.is_active =' => intval(1),
                'Personels.id =' => intval($slip->custodian->personel_id)
            ])
            ->order([
                'Personels.personel' => 'ASC'
            ],true);
        $offices = TableRegistry::getTableLocator()->get('Offices')
            ->find('list',[
                'valueField' => function($query){
                    return strtoupper($query->office);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Offices.is_active =' => intval(1),
                'Offices.id =' => intval($slip->custodian->office_id),
            ])
            ->order([
                'Offices.office' => 'ASC'
            ],true);
        $fundClusters = TableRegistry::getTableLocator()->get('FundClusters')
            ->find('list',[
                'valueField' => function($query){
                    return ucwords($query->fund_cluster);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'FundClusters.is_active =' => intval(1),
                'FundClusters.id =' => intval($slip->custodian->fund_cluster_id),
            ])
            ->order([
                'FundClusters.fund_cluster' => 'ASC'
            ],true);
        $institutions = TableRegistry::getTableLocator()->get('Institutions')
            ->find('list',[
                'valueField' => function($query){
                    return ucwords($query->institution);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->where([
                'Institutions.is_active =' => intval(1)
            ])
            ->order([
                'Institutions.institution' => 'ASC',
                'Institutions.id =' => intval($slip->custodian->institution_id),
            ],true);
        $this->set(compact('slip', 'personels', 'offices', 'fundClusters', 'institutions'));
    }

    public function pdf($id = null)
    {
        $slip = $this->Slips->get($id, [
            'contain' => [
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Supplies.Brands' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Ledgers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ],
        ]);
        $this->set(compact('slip'));
    }

    public function xlsx($id = null)
    {
        $slip = $this->Slips->get($id, [
            'contain' => [
                'Custodians' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Institutions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Users.Position' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Personels.Positions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Custodians.Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Supplies' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Supplies.Brands' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Supplies.Units' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Ledgers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Items.Accounts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ],
        ]);

        $filename = 'Appendix-59-ICS-'.(strtoupper($slip->account->legend)).'-'.($slip->code);

        $this->set(compact('slip', 'filename'));
    }

}
