<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Items Model
 *
 * @property \App\Model\Table\CustodiansTable&\Cake\ORM\Association\BelongsTo $Custodians
 * @property \App\Model\Table\SlipsTable&\Cake\ORM\Association\BelongsTo $Slips
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\LedgersTable&\Cake\ORM\Association\BelongsTo $Ledgers
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 *
 * @method \App\Model\Entity\Item newEmptyEntity()
 * @method \App\Model\Entity\Item newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ItemsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('items');
        $this->setDisplayField('serial_number');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Custodians', [
            'foreignKey' => 'custodian_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Slips', [
            'foreignKey' => 'slip_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Ledgers', [
            'foreignKey' => 'ledger_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){

            $connection->begin();

            try{

                $account = TableRegistry::getTableLocator()->get('Accounts')->get($entity->account_id);
                $ledger = TableRegistry::getTableLocator()->get('Ledgers')->get($entity->ledger_id);
                $office = TableRegistry::getTableLocator()->get('Offices')->get($entity->office_id);
                $max = @TableRegistry::getTableLocator()->get('Items')->find()
                    ->where([
                        'Items.created >=' => (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
                        'Items.created <=' => (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
                    ])
                    ->max('no')
                    ->no;
                $entity->no = (intval($max) + intval(1));
                $no = (str_pad(strval($entity->no), 4, '0000', STR_PAD_LEFT));
                $label = (boolval($account->is_labeled))? strtoupper($account->legend).'-': '';
                $entity->property_number = ($label).strval((date('Y')).'-'.($ledger->code).'-'.($ledger->sub_code).'-'.($no).'-'.($office->code));
                $this->save($entity);

                $supply = TableRegistry::getTableLocator()->get('Supplies')->get($entity->supply_id);
                $oldStock = doubleval($supply->stocks);
                $stocks = doubleval($supply->stocks) - intval($entity->quantity);
                $issued = doubleval($supply->issued) + intval($entity->quantity);
                if(intval($supply->stocks) >= intval($entity->quantity)){
                    $supply->stocks = doubleval($stocks);
                    $supply->issued = doubleval($issued);
                    TableRegistry::getTableLocator()->get('Supplies')->save($supply);
                }

                $logbook = TableRegistry::getTableLocator()->get('Logbooks')->newEmptyEntity();
                $logbook->user_id = intval($_SESSION['Auth']['User']['id']);
                $logbook->supply_id = intval($supply->id);
                $logbook->logbook = strtoupper('issuance');
                $logbook->quantity = intval($entity->quantity);
                $logbook->old_stock = intval($oldStock);
                $logbook->total_stock = intval($stocks);
                $logbook->is_stored = intval(0);
                TableRegistry::getTableLocator()->get('Logbooks')->save($logbook);

                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        if($entity->isDirty() && intval($i) == intval(0)){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');

        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('account_id')
            ->requirePresence('account_id', true)
            ->notEmptyString('account_id', ucwords('please fill out this field'),false);

        $validator
            ->requirePresence('logical_id', true)
            ->notEmptyString('logical_id');

        $validator
            ->numeric('slip_id')
            ->requirePresence('slip_id', true)
            ->notEmptyString('slip_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('supply_id')
            ->requirePresence('supply_id', true)
            ->notEmptyString('supply_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('account_id')
            ->requirePresence('account_id', true)
            ->notEmptyString('account_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('ledger_id')
            ->requirePresence('ledger_id', true)
            ->notEmptyString('ledger_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('office_id')
            ->requirePresence('office_id', true)
            ->notEmptyString('office_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('no')
            ->requirePresence('no', true)
            ->notEmptyString('no', ucwords('please fill out this field'),false);

        $validator
            ->numeric('year')
            ->requirePresence('year', true)
            ->notEmptyString('year', ucwords('please fill out this field'),false);

        $validator
            ->numeric('month')
            ->requirePresence('month', true)
            ->notEmptyString('month', ucwords('please fill out this field'),false);

        $validator
            ->numeric('quantity')
            ->requirePresence('quantity', true)
            ->notEmptyString('quantity', ucwords('please fill out this field'),false);

        $validator
            ->numeric('price')
            ->requirePresence('price', true)
            ->notEmptyString('price', ucwords('please fill out this field'),false);

        $validator
            ->numeric('total')
            ->requirePresence('total', true)
            ->notEmptyString('total', ucwords('please fill out this field'),false);

        $validator
            ->scalar('property_number')
            ->maxLength('property_number', 255)
            ->requirePresence('property_number', true)
            ->notEmptyString('property_number', ucwords('please fill out this field'),false);

        $validator
            ->scalar('serial_number')
            ->maxLength('serial_number', 255)
            ->requirePresence('serial_number', true)
            ->notEmptyString('serial_number', ucwords('please fill out this field'),false);

        $validator
            ->scalar('inventory_number')
            ->maxLength('inventory_number', 255)
            ->requirePresence('inventory_number', false)
            ->allowEmptyString('inventory_number');

        $validator
            ->scalar('lifespan')
            ->maxLength('lifespan', 255)
            ->requirePresence('lifespan', true)
            ->notEmptyString('lifespan', ucwords('please fill out this field'), false);

        $validator
            ->scalar('remarks')
            ->requirePresence('remarks', true)
            ->notEmptyString('remarks', ucwords('please fill out this field'),false);

        $validator
            ->requirePresence('is_returned',true)
            ->notEmptyString('is_returned', ucwords('please fill out this field'),false)
            ->add('is_returned','is_returned',[
                'rule' => function($value){

                    $isReturned = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isReturned)){
                        return ucwords('invalid returned value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('custodian_id', 'Custodians'), ['errorField' => 'custodian_id']);
        $rules->add($rules->existsIn('slip_id', 'Slips'), ['errorField' => 'slip_id']);
        $rules->add($rules->existsIn('supply_id', 'Supplies'), ['errorField' => 'supply_id']);
        $rules->add($rules->existsIn('account_id', 'Accounts'), ['errorField' => 'account_id']);
        $rules->add($rules->existsIn('legder_id', 'Ledgers'), ['errorField' => 'legder_id']);
        $rules->add($rules->existsIn('office_id', 'Offices'), ['errorField' => 'office_id']);

        return $rules;
    }
}
