<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Ledgers Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\HasMany $Supplies
 *
 * @method \App\Model\Entity\Ledger newEmptyEntity()
 * @method \App\Model\Entity\Ledger newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Ledger[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ledger get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ledger findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Ledger patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ledger[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ledger|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ledger saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ledger[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ledger[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ledger[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ledger[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LedgersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('ledgers');
        $this->setDisplayField('ledger');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Supplies', [
            'foreignKey' => 'ledger_id',
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'ledger_id',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        if($entity->isDirty() && intval($i) == intval(0)){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');

        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('user_id')
            ->requirePresence('user_id', true)
            ->notEmptyString('user_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('account_id')
            ->requirePresence('account_id', true)
            ->notEmptyString('account_id', ucwords('please fill out this field'),false);

        $validator
            ->scalar('ledger')
            ->maxLength('ledger', 255)
            ->requirePresence('ledger', true)
            ->notEmptyString('ledger', ucwords('please fill out this field'),false);

        $validator
            ->scalar('legend')
            ->maxLength('legend', 255)
            ->requirePresence('legend', true)
            ->notEmptyString('legend', ucwords('please fill out this field'),false);

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', true)
            ->notEmptyString('code', ucwords('please fill out this field'),false);

        $validator
            ->scalar('sub_code')
            ->maxLength('sub_code', 255)
            ->requirePresence('sub_code', true)
            ->notEmptyString('sub_code', ucwords('please fill out this field'),false);

        $validator
            ->requirePresence('is_active',true)
            ->notEmptyString('is_active', ucwords('please fill out this field'),false)
            ->add('is_active','is_active',[
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('invalid active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn('account_id', 'Accounts'), ['errorField' => 'account_id']);
        $rules->add($rules->isUnique(['account_id', 'code', 'sub_code'],ucwords('this sub_code is already exists in this account & code')), ['errorField' => 'sub_code']);
        $rules->add($rules->isUnique(['account_id', 'ledger'],ucwords('this ledger is already exists in this account')), ['errorField' => 'ledger']);
        $rules->add($rules->isUnique(['account_id', 'ledger', 'legend'],ucwords('this legend is already exists in this ledger & account')), ['errorField' => 'legend']);

        return $rules;
    }
}
