<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Slips Model
 *
 * @property \App\Model\Table\CustodiansTable&\Cake\ORM\Association\BelongsTo $Custodians
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\HasMany $Items
 *
 * @method \App\Model\Entity\Slip newEmptyEntity()
 * @method \App\Model\Entity\Slip newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Slip[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slip get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slip findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Slip patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slip[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slip|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slip saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slip[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Slip[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Slip[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Slip[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SlipsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('slips');
        $this->setDisplayField('code');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Custodians', [
            'foreignKey' => 'custodian_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'slip_id',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        if($entity->isDirty() && intval($i) == intval(0)){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');

        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('custodian_id')
            ->requirePresence('custodian_id', true)
            ->notEmptyString('custodian_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('account_id')
            ->requirePresence('account_id', true)
            ->notEmptyString('account_id', ucwords('please fill out this field'),false);

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', true)
            ->notEmptyString('code', ucwords('please fill out this field'), false);

        $validator
            ->numeric('no')
            ->requirePresence('no', true)
            ->notEmptyString('no', ucwords('please fill out this field'), false);

        $validator
            ->numeric('year')
            ->requirePresence('year', true)
            ->notEmptyString('year', ucwords('please fill out this field'), false);

        $validator
            ->numeric('month')
            ->requirePresence('month', true)
            ->notEmptyString('month', ucwords('please fill out this field'), false);

        $validator
            ->numeric('total')
            ->requirePresence('total', true)
            ->notEmptyString('total', ucwords('please fill out this field'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('custodian_id', 'Custodians'), ['errorField' => 'custodian_id']);
        $rules->add($rules->existsIn('account_id', 'Accounts'), ['errorField' => 'account_id']);

        return $rules;
    }
}
