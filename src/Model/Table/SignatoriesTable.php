<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Signatories Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\BelongsTo $Positions
 * @property \App\Model\Table\HeadingsTable&\Cake\ORM\Association\BelongsTo $Headings
 *
 * @method \App\Model\Entity\Signatory newEmptyEntity()
 * @method \App\Model\Entity\Signatory newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Signatory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Signatory get($primaryKey, $options = [])
 * @method \App\Model\Entity\Signatory findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Signatory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Signatory[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Signatory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Signatory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Signatory[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SignatoriesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('signatories');
        $this->setDisplayField('signatory');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Positions', [
            'foreignKey' => 'position_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Headings', [
            'foreignKey' => 'heading_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('user_id')
            ->requirePresence('user_id', true)
            ->notEmptyString('user_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('position_id')
            ->requirePresence('position_id', true)
            ->notEmptyString('position_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('heading_id')
            ->requirePresence('heading_id', true)
            ->notEmptyString('heading_id', ucwords('please fill out this field'),false);

        $validator
            ->scalar('signatory')
            ->maxLength('signatory', 255)
            ->requirePresence('signatory', true)
            ->notEmptyString('signatory', ucwords('please fill out this field'),false);

        $validator
            ->scalar('description')
            ->maxLength('description', 65535)
            ->requirePresence('description', true)
            ->notEmptyString('description', ucwords('please fill out this field'),false);

        $validator
            ->numeric('gap')
            ->requirePresence('gap', true)
            ->notEmptyString('gap', ucwords('please fill out this field'),false)
            ->add('gap', 'gap',[
                'rule' => function($value){

                    if(intval($value) < intval(0)){
                        return ucwords('this field must not beh lower to 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('merge')
            ->requirePresence('merge', true)
            ->notEmptyString('merge', ucwords('please fill out this field'),false)
            ->add('merge', 'merge',[
                'rule' => function($value){

                    if(intval($value) < intval(0)){
                        return ucwords('this field must not beh lower to 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('order_position')
            ->requirePresence('order_position', true)
            ->notEmptyString('order_position', ucwords('please fill out this field'),false);

        $validator
            ->requirePresence('is_active',true)
            ->notEmptyString('is_active', ucwords('please fill out this field'),false)
            ->add('is_active','is_active',[
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('invalid active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn('position_id', 'Positions'), ['errorField' => 'position_id']);
        $rules->add($rules->existsIn('heading_id', 'Headings'), ['errorField' => 'heading_id']);
        $rules->add($rules->isUnique(['signatory'],ucwords('this signatory is already exists')), ['errorField' => 'signatory']);

        return $rules;
    }
}
