<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Custodians Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PersonelsTable&\Cake\ORM\Association\BelongsTo $Personels
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\HasMany $Items
 * @property \App\Model\Table\SlipsTable&\Cake\ORM\Association\HasMany $Slips
 *
 * @method \App\Model\Entity\Custodian newEmptyEntity()
 * @method \App\Model\Entity\Custodian newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Custodian[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Custodian get($primaryKey, $options = [])
 * @method \App\Model\Entity\Custodian findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Custodian patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Custodian[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Custodian|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Custodian saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Custodian[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Custodian[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Custodian[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Custodian[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustodiansTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('custodians');
        $this->setDisplayField('code');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Personels', [
            'foreignKey' => 'personel_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FundClusters', [
            'foreignKey' => 'fund_cluster_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Institutions', [
            'foreignKey' => 'institution_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'custodian_id',
        ]);
        $this->hasMany('Slips', [
            'foreignKey' => 'custodian_id',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        if($entity->isDirty() && intval($i) == intval(0)){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');

        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('user_id')
            ->requirePresence('user_id', true)
            ->notEmptyString('user_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('personel_id')
            ->requirePresence('personel_id', true)
            ->notEmptyString('personel_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('office_id')
            ->requirePresence('office_id', true)
            ->notEmptyString('office_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('fund_cluster_id')
            ->requirePresence('fund_cluster_id', true)
            ->notEmptyString('fund_cluster_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('institution_id')
            ->requirePresence('institution_id', true)
            ->notEmptyString('institution_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('no')
            ->requirePresence('no', true)
            ->notEmptyString('no', ucwords('please fill out this field'),false);

        $validator
            ->numeric('year')
            ->requirePresence('year', true)
            ->notEmptyString('year', ucwords('please fill out this field'),false);

        $validator
            ->numeric('month')
            ->requirePresence('month', true)
            ->notEmptyString('month', ucwords('please fill out this field'),false);

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', true)
            ->notEmptyString('code', ucwords('please fill out this field'),false);

        $validator
            ->numeric('total')
            ->requirePresence('total', true)
            ->notEmptyString('total', ucwords('please fill out this field'),false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn('personel_id', 'Personels'), ['errorField' => 'personel_id']);
        $rules->add($rules->existsIn('office_id', 'Offices'), ['errorField' => 'office_id']);
        $rules->add($rules->existsIn('fund_cluster_id', 'FundClusters'), ['errorField' => 'fund_cluster_id']);
        $rules->add($rules->existsIn('institution_id', 'Institutions'), ['errorField' => 'institution_id']);
        return $rules;
    }
}
