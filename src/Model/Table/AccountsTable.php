<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Accounts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\HasMany $Items
 * @property \App\Model\Table\LedgersTable&\Cake\ORM\Association\HasMany $Ledgers
 * @property \App\Model\Table\SlipsTable&\Cake\ORM\Association\HasMany $Slips
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\HasMany $Supplies
 *
 * @method \App\Model\Entity\Account newEmptyEntity()
 * @method \App\Model\Entity\Account newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Account[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Account get($primaryKey, $options = [])
 * @method \App\Model\Entity\Account findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Account patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Account[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Account|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Account saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AccountsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('accounts');
        $this->setDisplayField('account');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'account_id',
        ]);
        $this->hasMany('Ledgers', [
            'foreignKey' => 'account_id',
        ]);
        $this->hasMany('Slips', [
            'foreignKey' => 'account_id',
        ]);
        $this->hasMany('Supplies', [
            'foreignKey' => 'account_id',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        if($entity->isDirty() && intval($i) == intval(0)){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');

        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('user_id')
            ->requirePresence('user_id', true)
            ->notEmptyString('user_id', ucwords('please fill out this field'),false);

        $validator
            ->scalar('account')
            ->maxLength('account', 255)
            ->requirePresence('account', true)
            ->notEmptyString('account', ucwords('please fill out this field'),false);

        $validator
            ->scalar('legend')
            ->maxLength('legend', 255)
            ->requirePresence('legend', true)
            ->notEmptyString('legend', ucwords('please fill out this field'),false);

        $validator
            ->scalar('color')
            ->maxLength('color', 255)
            ->requirePresence('color', true)
            ->notEmptyString('color', ucwords('please fill out this field'),false);

        $validator
            ->numeric('start_price')
            ->requirePresence('start_price', true)
            ->notEmptyString('start_price', ucwords('please fill out this field'),false)
            ->add('start_price','start_price',[
                'rule' => function($value){

                    if(doubleval($value) <= doubleval(0)){
                        return ucwords('this field must be higher than 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->numeric('end_price')
            ->requirePresence('end_price', true)
            ->notEmptyString('end_price', ucwords('please fill out this field'),false)
            ->add('end_price','end_price',[
                'rule' => function($value){

                    if(doubleval($value) <= doubleval(0)){
                        return ucwords('this field must be higher than 0');
                    }

                    return true;
                }
            ]);

        $validator
            ->requirePresence('is_labeled',true)
            ->notEmptyString('is_labeled', ucwords('please fill out this field'),false)
            ->add('is_labeled','is_labeled',[
                'rule' => function($value){

                    $isType = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isType)){
                        return ucwords('invalid labeled value');
                    }

                    return true;
                }
            ]);

        $validator
            ->requirePresence('is_type',true)
            ->notEmptyString('is_type', ucwords('please fill out this field'),false)
            ->add('is_type','is_type',[
                'rule' => function($value){

                    $isType = [intval(0), intval(1), intval(2)];
                    if(!in_array(intval($value), $isType)){
                        return ucwords('invalid type value');
                    }

                    return true;
                }
            ]);

        $validator
            ->requirePresence('is_appendix',true)
            ->notEmptyString('is_appendix', ucwords('please fill out this field'),false)
            ->add('is_appendix','is_appendix',[
                'rule' => function($value){

                    $isAppendix = [intval(0), intval(1), intval(2)];
                    if(!in_array(intval($value), $isAppendix)){
                        return ucwords('invalid appendix value');
                    }

                    return true;
                }
            ]);

        $validator
            ->requirePresence('is_active',true)
            ->notEmptyString('is_active', ucwords('please fill out this field'),false)
            ->add('is_active','is_active',[
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('invalid active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['account'],ucwords('this account is already exists')), ['errorField' => 'account']);
        $rules->add($rules->isUnique(['account', 'legend'],ucwords('this legend is already exists')), ['errorField' => 'legend']);

        return $rules;
    }
}
