<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;
use YoHang88\LetterAvatar\LetterAvatar;

/**
 * Users Model
 *
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\HasMany $Accounts
 * @property \App\Model\Table\ActivitiesTable&\Cake\ORM\Association\HasMany $Activities
 * @property \App\Model\Table\BrandsTable&\Cake\ORM\Association\HasMany $Brands
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\HasMany $Categories
 * @property \App\Model\Table\CustodiansTable&\Cake\ORM\Association\HasMany $Custodians
 * @property \App\Model\Table\DatabasesTable&\Cake\ORM\Association\HasMany $Databases
 * @property \App\Model\Table\ExportsTable&\Cake\ORM\Association\HasMany $Exports
 * @property \App\Model\Table\LedgersTable&\Cake\ORM\Association\HasMany $Ledgers
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\HasMany $Offices
 * @property \App\Model\Table\PersonelsTable&\Cake\ORM\Association\HasMany $Personels
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\HasMany $Positions
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\HasMany $Supplies
 * @property \App\Model\Table\UnitsTable&\Cake\ORM\Association\HasMany $Units
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Position', [
            'foreignKey' => 'position_id',
        ])->setClassName('Positions');
        $this->hasMany('Accounts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Activities', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Brands', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Categories', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Custodians', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Databases', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Exports', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Ledgers', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Offices', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Personels', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Positions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Supplies', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Units', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Logos', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Headings', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Signatories', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Logbooks', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('FundClusters', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Institutions', [
            'foreignKey' => 'user_id',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){
            $connection->begin();

            try{

                $path = WWW_ROOT. 'img'. DS. 'user-avatar';
                $folder = new Folder();
                if(!$folder->cd($path)){
                    $folder->create($path);
                }

                $filepath = WWW_ROOT. 'img'. DS. 'user-avatar'. DS. strval($entity->id).'.png';

                $avatar = new LetterAvatar($entity->name,'circle', intval(400));
                $avatar->setColor('#317fbe', '#fff');
                $avatar->saveAs($filepath, LetterAvatar::MIME_TYPE_PNG);

            }catch (\Exception $exception){

            }

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        if($entity->isDirty() && intval($i) == intval(0)){
            $connection->begin();

            try{

                $path = WWW_ROOT. 'img'. DS. 'user-avatar';
                $folder = new Folder();
                if(!$folder->cd($path)){
                    $folder->create($path);
                }

                $filepath = WWW_ROOT. 'img'. DS. 'user-avatar'. DS. strval($entity->id).'.png';

                $avatar = new LetterAvatar($entity->name,'circle', intval(400));
//                $avatar->setColor('#ec3125', '#ffffff');
                $avatar->saveAs($filepath, LetterAvatar::MIME_TYPE_PNG);

            }catch (\Exception $exception){

            }

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }
        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');
        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('position_id')
            ->requirePresence('position_id', true)
            ->notEmptyString('position_id', ucwords('please fill out this field'),false);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', true)
            ->notEmptyString('name', ucwords('please fill put this field'),false);

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', true)
            ->notEmptyString('username', ucwords('please fill put this field'),false);

        $validator
            ->email('email')
            ->requirePresence('email', true)
            ->notEmptyString('email', ucwords('please fill put this field'),false);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password', ucwords('please fill out this field'),false)
            ->add('password', 'password', [
                'rule' => function($value){

                    $regex = '/^(.){6,}$/';

                    if(!preg_match_all($regex, $value)){
                        return ucwords('Password Must Have 6 Characters');
                    }

                    return true;
                }
            ]);

        $validator
            ->scalar('token')
            ->maxLength('token', 255)
            ->requirePresence('token', true)
            ->notEmptyString('token', ucwords('please fill put this field'),false);

        $validator
            ->requirePresence('is_admin',true)
            ->notEmptyString('is_admin', ucwords('please fill out this field'),false)
            ->add('is_admin','is_admin',[
                'rule' => function($value){

                    $isAdmin = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isAdmin)){
                        return ucwords('invalid admin value');
                    }

                    return true;
                }
            ]);

        $validator
            ->requirePresence('is_active',true)
            ->notEmptyString('is_active', ucwords('please fill out this field'),false)
            ->add('is_active','is_active',[
                'rule' => function($value){

                    $isActive = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isActive)){
                        return ucwords('invalid active value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $userId)
    {
        return $this->exists(['id' => $paramId, 'Users.id' => $userId]);
    }

    public function findAuth(Query $query, array $options)
    {
        $query
            ->where([
                'OR' => [
                    'username like' => $options['username'],
                    'email like' => $options['username'],
                ],
                'is_active =' => intval(1)], [], true); // <-- true here means overwrite original query !IMPORTANT.
        return $query;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('position_id', 'Positions'), ['errorField' => 'position_id']);
        $rules->add($rules->isUnique(['username'], ucwords('this username is already exists')), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email'], ucwords('this email is already exists')), ['errorField' => 'email']);

        return $rules;
    }
}
