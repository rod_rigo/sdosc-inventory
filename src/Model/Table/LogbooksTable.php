<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Logbooks Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 *
 * @method \App\Model\Entity\Logbook newEmptyEntity()
 * @method \App\Model\Entity\Logbook newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Logbook[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Logbook get($primaryKey, $options = [])
 * @method \App\Model\Entity\Logbook findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Logbook patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Logbook[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Logbook|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Logbook saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Logbook[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Logbook[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Logbook[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Logbook[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LogbooksTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('logbooks');
        $this->setDisplayField('logbook');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER',
        ]);
    }

    public function afterSave(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $i = intval(0);

        $connection = ConnectionManager::get('default');

        if($entity->isNew()){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('add');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
                $i++;
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        if($entity->isDirty() && intval($i) == intval(0)){

            $connection->begin();

            try{
                $activity = TableRegistry::getTableLocator()->get('Activities')
                    ->newEmptyEntity();
                $activity->user_id = intval($_SESSION['Auth']['User']['id']);
                $activity->model = strtoupper($this->getAlias());
                $activity->action = strtoupper('edit');
                $activity->original = json_encode($entity->getOriginalValues());
                $activity->data = json_encode($entity);
                TableRegistry::getTableLocator()->get('Activities')->save($activity);
                $connection->commit();
            }catch (\Exception $exception){
                $connection->rollback();
            }

        }

        return true;
    }

    public function afterDelete(\Cake\Event\EventInterface $event, \Cake\Datasource\EntityInterface $entity, \ArrayObject $options){

        $connection = ConnectionManager::get('default');

        $connection->begin();

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')
                ->newEmptyEntity();
            $activity->user_id = intval($_SESSION['Auth']['User']['id']);
            $activity->model = strtoupper($this->getAlias());
            $activity->action = strtoupper('delete');
            $activity->original = json_encode($entity->getOriginalValues());
            $activity->data = json_encode($entity);
            TableRegistry::getTableLocator()->get('Activities')->save($activity);
            $connection->commit();
        }catch (\Exception $exception){
            $connection->rollback();
        }

        return true;
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->numeric('user_id')
            ->requirePresence('user_id', true)
            ->notEmptyString('user_id', ucwords('please fill out this field'),false);

        $validator
            ->numeric('supply_id')
            ->requirePresence('supply_id', true)
            ->notEmptyString('supply_id', ucwords('please fill out this field'),false);

        $validator
            ->scalar('logbook')
            ->maxLength('logbook', 255)
            ->requirePresence('logbook', true)
            ->notEmptyString('logbook', ucwords('please fill out this field'),false);

        $validator
            ->numeric('quantity')
            ->requirePresence('quantity', true)
            ->notEmptyString('quantity', ucwords('please fill out this field'),false);

        $validator
            ->numeric('old_stock')
            ->requirePresence('old_stock', true)
            ->notEmptyString('old_stock', ucwords('please fill out this field'),false);

        $validator
            ->numeric('total_stock')
            ->requirePresence('total_stock', true)
            ->notEmptyString('total_stock', ucwords('please fill out this field'),false);

        $validator
            ->requirePresence('is_stored',true)
            ->notEmptyString('is_stored', ucwords('please fill out this field'),false)
            ->add('is_stored','is_stored',[
                'rule' => function($value){

                    $isStored = [intval(0), intval(1)];
                    if(!in_array(intval($value), $isStored)){
                        return ucwords('invalid stored value');
                    }

                    return true;
                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn('supply_id', 'Supplies'), ['errorField' => 'supply_id']);

        return $rules;
    }
}
