<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Logbook Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $supply_id
 * @property string $logbook
 * @property float $quantity
 * @property float $old_stock
 * @property float $total_stock
 * @property int $is_stored
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Supply $supply
 */
class Logbook extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'supply_id' => true,
        'logbook' => true,
        'quantity' => true,
        'old_stock' => true,
        'total_stock' => true,
        'is_stored' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'supply' => true,
    ];

    protected function _setLogbook($value){
        return strtoupper($value);
    }

}
