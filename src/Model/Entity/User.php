<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $position_id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $token
 * @property int $is_admin
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Account[] $accounts
 * @property \App\Model\Entity\Activity[] $activities
 * @property \App\Model\Entity\Brand[] $brands
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Custodian[] $custodians
 * @property \App\Model\Entity\Database[] $databases
 * @property \App\Model\Entity\Export[] $exports
 * @property \App\Model\Entity\Ledger[] $ledgers
 * @property \App\Model\Entity\Office[] $offices
 * @property \App\Model\Entity\Personel[] $personels
 * @property \App\Model\Entity\Position[] $positions
 * @property \App\Model\Entity\Supply[] $supplies
 * @property \App\Model\Entity\Unit[] $units
 * @property \App\Model\Entity\Logo[] $logos
 * @property \App\Model\Entity\Heading[] $headings
 * @property \App\Model\Entity\Signatory[] $signatories
 * @property \App\Model\Entity\Logbook[] $logbooks
 * @property \App\Model\Entity\FundCluster[] $fund_clusters
 * @property \App\Model\Entity\Institution[] $institutions
 * @property \App\Model\Entity\Position $position
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'position_id' => true,
        'name' => true,
        'username' => true,
        'email' => true,
        'password' => true,
        'token' => true,
        'is_admin' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'accounts' => true,
        'activities' => true,
        'brands' => true,
        'categories' => true,
        'custodians' => true,
        'databases' => true,
        'exports' => true,
        'ledgers' => true,
        'offices' => true,
        'personels' => true,
        'positions' => true,
        'supplies' => true,
        'units' => true,
        'logos' => true,
        'headings' => true,
        'signatories' => true,
        'logbooks' => true,
        'fund_clusters' => true,
        'institutions' => true,
        'position' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array<string>
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setName($value){
        return strtoupper($value);
    }

    protected function _setPassword($value){
        return (new DefaultPasswordHasher())->hash($value);
    }

}
