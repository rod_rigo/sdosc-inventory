<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Office Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $office
 * @property string $legend
 * @property string $code
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\Custodian[] $custodians
 * @property \App\Model\Entity\Personel[] $personels
 */
class Office extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'office' => true,
        'legend' => true,
        'code' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'items' => true,
        'custodians' => true,
        'personels' => true,
    ];

    protected function _setOffice($value){
        return strtoupper($value);
    }

    protected function _setLegend($value){
        return strtoupper($value);
    }

}
