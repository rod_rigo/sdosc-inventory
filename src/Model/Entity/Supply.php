<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supply Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $supply
 * @property float $price
 * @property float $stocks
 * @property float $issued
 * @property int $brand_id
 * @property int $category_id
 * @property int $unit_id
 * @property int $account_id
 * @property int $ledger_id
 * @property string $lifespan
 * @property int $is_active
 * @property int $is_expired
 * @property \Cake\I18n\FrozenDate|null $expired_at
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Brand $brand
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Ledger $ledger
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\Logbook[] $logbooks
 */
class Supply extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'supply' => true,
        'price' => true,
        'stocks' => true,
        'issued' => true,
        'brand_id' => true,
        'category_id' => true,
        'unit_id' => true,
        'account_id' => true,
        'ledger_id' => true,
        'lifespan' => true,
        'is_active' => true,
        'is_expired' => true,
        'expired_at' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'brand' => true,
        'category' => true,
        'unit' => true,
        'account' => true,
        'ledger' => true,
        'items' => true,
        'logbooks' => true,
    ];

    protected function _setSupply($value){
        return strtoupper($value);
    }

    protected function _setLifespan($value){
        return strtoupper($value);
    }

}
