<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Custodian Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $personel_id
 * @property int $office_id
 * @property int $fund_cluster_id
 * @property int $institution_id
 * @property string $code
 * @property float $no
 * @property float $year
 * @property float $month
 * @property float $total
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Personel $personel
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\Slip[] $slips
 * @property \App\Model\Entity\FundCluster $fund_cluster
 * @property \App\Model\Entity\Institution $institution
 */
class Custodian extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'personel_id' => true,
        'office_id' => true,
        'fund_cluster_id' => true,
        'institution_id' => true,
        'code' => true,
        'no' => true,
        'year' => true,
        'month' => true,
        'total' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'personel' => true,
        'office' => true,
        'items' => true,
        'slips' => true,
        'fund_cluster' => true,
        'institution' => true,
    ];
}
