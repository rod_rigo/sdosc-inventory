<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Slip Entity
 *
 * @property int $id
 * @property int $custodian_id
 * @property int $account_id
 * @property string $code
 * @property float $no
 * @property float $month
 * @property float $year
 * @property float $total
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Custodian $custodian
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Item[] $items
 */
class Slip extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'custodian_id' => true,
        'account_id' => true,
        'code' => true,
        'no' => true,
        'year' => true,
        'month' => true,
        'total' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'custodian' => true,
        'account' => true,
        'items' => true,
    ];
}
