<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property int $custodian_id
 * @property int $logical_id
 * @property int $slip_id
 * @property int $supply_id
 * @property int $account_id
 * @property int $ledger_id
 * @property int $office_id
 * @property float $no
 * @property float $year
 * @property float $month
 * @property float $quantity
 * @property float $price
 * @property float $total
 * @property string $property_number
 * @property string $serial_number
 * @property null|string $inventory_number
 * @property string $lifespan
 * @property string $remarks
 * @property int $is_returned
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Custodian $custodian
 * @property \App\Model\Entity\Slip $slip
 * @property \App\Model\Entity\Supply $supply
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Ledger $ledger
 * @property \App\Model\Entity\Office $office
 */
class Item extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'custodian_id' => true,
        'logical_id' => true,
        'slip_id' => true,
        'supply_id' => true,
        'account_id' => true,
        'ledger_id' => true,
        'office_id' => true,
        'no' => true,
        'year' => true,
        'month' => true,
        'quantity' => true,
        'price' => true,
        'total' => true,
        'property_number' => true,
        'serial_number' => true,
        'inventory_number' => true,
        'lifespan' => true,
        'remarks' => true,
        'is_returned' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'custodian' => true,
        'slip' => true,
        'supply' => true,
        'account' => true,
        'ledger' => true,
        'office' => true,
    ];

    protected function _setLifespan($value){
        return ucwords($value);
    }
}
