<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Account Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $account
 * @property string $legend
 * @property string $color
 * @property float $start_price
 * @property float $end_price
 * @property int $is_labeled
 * @property int $is_type
 * @property int $is_appendix
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\Ledger[] $ledgers
 * @property \App\Model\Entity\Slip[] $slips
 * @property \App\Model\Entity\Supply[] $supplies
 */
class Account extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'account' => true,
        'legend' => true,
        'color' => true,
        'start_price' => true,
        'end_price' => true,
        'is_labeled' => true,
        'is_type' => true,
        'is_appendix' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'items' => true,
        'ledgers' => true,
        'slips' => true,
        'supplies' => true,
    ];

    protected function _setAccount($value){
        return strtoupper($value);
    }

    protected function _setLegend($value){
        return strtoupper($value);
    }

}
