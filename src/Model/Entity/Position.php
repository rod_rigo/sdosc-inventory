<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Position Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $position
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Personel[] $personels
 * @property \App\Model\Entity\Signatory[] $signatories
 * @property \App\Model\Entity\Institution[] $institutions
 */
class Position extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'position' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'personels' => true,
        'signatories' => true,
        'institutions' => true,
    ];

    protected function _setPosition($value){
        return ucwords($value);
    }

}
