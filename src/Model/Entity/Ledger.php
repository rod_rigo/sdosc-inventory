<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ledger Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $account_id
 * @property string $ledger
 * @property string $legend
 * @property string $code
 * @property string $sub_code
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Ledger[] $ledgers
 * @property \App\Model\Entity\Supply[] $supplies
 */
class Ledger extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'account_id' => true,
        'ledger' => true,
        'legend' => true,
        'code' => true,
        'sub_code' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'account' => true,
        'ledgers' => true,
        'supplies' => true,
    ];

    protected function _setLedger($value){
        return strtoupper($value);
    }

    protected function _setLegend($value){
        return strtoupper($value);
    }

}
