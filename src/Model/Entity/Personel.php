<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Personel Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $position_id
 * @property int $office_id
 * @property string $personel
 * @property int $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Position $position
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\Custodian[] $custodians
 * @property \App\Model\Entity\Institution[] $institutions
 */
class Personel extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'position_id' => true,
        'office_id' => true,
        'personel' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'position' => true,
        'office' => true,
        'custodians' => true,
        'institutions' => true,
    ];

    protected function _setPersonel($value){
        return strtoupper($value);
    }

}
