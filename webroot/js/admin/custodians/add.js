'use strict';
$(document).ready(function () {

    var url = window.location.href;
    var scanner;
    var supply;

    var select = $('#personel-id').select2({
        placeholder: 'Personel',
        allowClear: true,
        width: '100%'
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            Swal.fire({
                title: 'Make Another Custodian?',
                text: data.message,
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                allowOutsideClick:false,
            }).then(function (result) {
                if (result.isConfirmed) {
                    $('#form')[0].reset();
                    select.val(null).trigger('change');
                    $('#items-tbody').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                }else{
                    $('#form')[0].reset();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Turbolinks.visit(data.redirect,{action:'replace'});
                }
            });
            window.open(mainurl+'custodians/pdf/'+(data.id));
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });


        });
    });

    $('button[type="reset"]').click(function (e) {
        select.val(null).trigger('change');
    });

    $('#scan').click(function (e) {
        scanner = new Html5QrcodeScanner('scanner', {
            fps: 10,
            qrbox: {
                width: 250,
                height: 250
            },
            rememberLastUsedCamera: true,
            aspectRatio: 1.0,
            disableFlip:false,
            supportedScanTypes: [
                Html5QrcodeScanType.SCAN_TYPE_CAMERA,
                Html5QrcodeScanType.SCAN_TYPE_FILE,
            ],
        });

        scanner.render(function(decodedText, decodedResult) {
            getSupply(parseInt(decodedText));
            $('#quantity').prop('disabled', false);
            $('#modal').modal('toggle');
        }, function (error) {

        });

        $('#modal').modal('toggle');
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        scanner.clear();
    });

    select.on('select2:select', function (e) {
        var value = $(this).val();
        personel(value);
    }).on('select2:unselect', function (e) {
        $('#select-personel-id').text('Please Fill Out This Field');
    }).on('select2:close', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $('#select-personel-id').text('Please Fill Out This Field');
            return true;
        }

        $('#select-personel-id').empty();
    });

    $('#supply').on('input', function (e) {
        var value = $(this).val();
        if(value){
            setTimeout(function () {
                getSuppliesList(value);
            }, parseInt(500));
        }
    });

    $('#office-id').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#quantity').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        if(parseInt(value) < parseInt(0)){
            $(this).addClass('is-invalid').next('small').text('This Field Must Be Higher Than 0');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    }).keypress(function (e) {
        var key = e.key;
        var regex = /^([0-9\.]){1,}$/;

        if(!key.match(regex)){
            e.preventDefault();
        }

    });

    $('#password').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $(document).on('keypress', 'input[type="number"]', function (e) {
        var key = e.key;
        var regex = /^([0-9\.]){1,}$/;

        if(!key.match(regex)){
            e.preventDefault();
        }
    });

    $('#append-row').click(function (e) {
        itemsTbody();
    });

    $(document).on('click', '.remove', function (e) {
        var dataId = $(this).attr('data-id');
        $('#tr-'+(dataId)+'').fadeOut(300, function (e) {
            $('#tr-'+(dataId)+'').remove();
        });
        trNo();
        total();
    });

    $(document).on('input', '.items-price', function (e) {
        total();
    });

    $(document).on('dblclick', 'input[class*="items"]', function (e) {
        var value = $(this).val();
        var dataAttr = $(this).attr('data-attr');
        $('input[data-attr="'+(dataAttr)+'"]').val(value);
        trNo();
        total();
    });

    function getSuppliesList(search) {
        $.ajax({
            url:mainurl+'supplies/getSuppliesList?search='+(search),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend:function (e) {

            },
        }).done(function (data, status, xhr) {
            $('#supply').autocomplete({
                delay: 100,
                minLength: 2,
                autoFocus: true,
                source: function (request, response) {
                    response($.map(data, function (value, key) {
                        var name = value.supply.toUpperCase();
                        if (name.indexOf(request.term.toUpperCase()) !== -1) {
                            return {
                                label: value.supply +' ('+(parseInt(value.stocks))+')',
                                supply: (value.supply).toUpperCase(),
                                id: parseInt(value.id),
                                account_id: parseInt(value.account_id),
                                category_id: parseInt(value.category_id),
                                ledger_id: parseInt(value.ledger_id),
                                price: parseFloat(value.price),
                                stocks: parseInt(value.stocks),
                                lifespan: value.lifespan,
                            }
                        } else {
                            return null;
                        }
                    }));
                },
                create: function (event, ui) {
                    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                        var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                        return $('<li></li>')
                            .data('item.autocomplete', item)
                            .append('<li>'+(label)+'</li>')
                            .appendTo(ul);
                    };
                },
                focus: function(e, ui) {
                    e.preventDefault();
                },
                select: function(e, ui) {
                    supply = ui.item;
                    $('#quantity').prop('disabled', false);
                },
                change: function(e, ui ) {
                    e.preventDefault();
                },
            });

            $('#supply').autocomplete('search',search);
        }).fail(function (data, status, xhr) {

        });
    }

    function getSupply(id) {
        $.ajax({
            url:mainurl+'supplies/getSupply/'+(parseInt(id)),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend:function (e) {

            },
        }).done(function (data, status, xhr) {
            supply = data;
            $('#supply').val((data.supply).toUpperCase());
            Swal.fire({
                icon: 'success',
                title: 'Scan Success',
                text: null,
                timer: 1500,
                timerProgressBar:true,
                toast:true,
                position:'top-right',
                showConfirmButton:false,
            });
        }).fail(function (data, status, xhr) {
            Swal.fire({
                icon: 'error',
                title: 'Scan Error',
                text: null,
                timer: 1500,
                timerProgressBar:true,
                toast:true,
                position:'top-right',
                showConfirmButton:false,
            });
        });
    }

    function itemsTbody() {
        var quantity = parseInt($('#quantity').val());

        if(parseInt(Object.keys(supply).length) > parseInt(0) && parseInt(quantity) > parseInt(0)){
            var crement = (parseInt(supply.stocks) >= parseInt(quantity))? parseInt(quantity): parseInt(supply.stocks);
            for(var i = 0; i < parseInt(crement); i++){
                var range = (parseInt(supply.id))+'-'+(Math.floor(Math.random() * parseInt(1000)))+'-'+(Math.floor(Math.random() * parseInt(1000)));
                $('#items-tbody').append('<tr id="tr-'+(range)+'">'+
                    '<td class="tr-no"></td>'+
                    '<td>'+((supply.supply).toUpperCase()+' ('+(supply.stocks)+')')+'<input type="hidden" name="" class="items-supply-id" value="'+(parseInt(supply.id))+'" id="supply-id-'+(range)+'" data-attr="'+(parseInt(supply.id))+'" data-id="'+(parseInt(supply.id))+'" required></td>' +
                    '<td><input type="text" name="" class="form-control form-control-sm form-control-border rounded-0 items-serial-number" id="serial-number-'+(range)+'" placeholder="Serial Number" data-attr="serial-number-'+(parseInt(supply.id))+'" data-id="'+(parseInt(supply.id))+'" required></td>'+
                    '<td><input type="text" name="" class="form-control form-control-sm form-control-border rounded-0 items-inventory-number" id="inventory-number-'+(range)+'" data-attr="inventory-number-'+(parseInt(supply.id))+'" data-id="'+(parseInt(supply.id))+'" placeholder="Inventory Number"></td>'+
                    '<td><input type="number" name="" class="form-control form-control-sm form-control-border rounded-0 items-price" id="price-'+(range)+'" value="'+(parseFloat(supply.price))+'" data-attr="price-'+(parseInt(supply.id))+'" data-id="'+(parseInt(supply.id))+'" step="any" placeholder="Price" min="0" required></td>'+
                    '<td><input type="text" name="" class="form-control form-control-sm form-control-border rounded-0 items-lifespan" id="lifespan-'+(range)+'" value="'+(supply.lifespan)+'" data-attr="lifespan-'+(parseInt(supply.id))+'" data-id="'+(parseInt(supply.id))+'" placeholder="Lifespan" required></td>'+
                    '<td><input type="text" name="" class="form-control form-control-sm form-control-border rounded-0 items-remarks" id="remarks-'+(range)+'" value="-" placeholder="Remarks" data-attr="remarks-'+(parseInt(supply.id))+'" data-id="'+(parseInt(supply.id))+'" required></td>'+
                    '<td><button type="button" class="btn btn-danger rounded-0 remove" data-id="'+(range)+'" data-supply-id="'+(parseInt(supply.id))+'" title="Remove"><i class="fa fa-trash"></i></button></td>'+
                    '</tr>');

            }

            supply = {};
            $('#quantity').val(0).prop('disabled', true);
            $('#supply').val(null);
        }

        trNo();
        total();
    }

    function trNo() {
        var no = 0;
        var supplyId = document.querySelectorAll('.items-supply-id');
        var price = document.querySelectorAll('.items-price');
        var serialNumber = document.querySelectorAll('.items-serial-number');
        var inventoryNumber = document.querySelectorAll('.items-inventory-number');
        var lifespan = document.querySelectorAll('.items-lifespan');
        var remarks = document.querySelectorAll('.items-remarks');
        $('.tr-no').each(function () {
            no++;
            var index = (parseInt(no) - parseInt(1));
            $(this).text(no);
            supplyId[index].name = 'items['+(index)+'][supply_id]';
            price[index].name = 'items['+(index)+'][price]';
            serialNumber[index].name = 'items['+(index)+'][serial_number]';
            inventoryNumber[index].name = 'items['+(index)+'][inventory_number]';
            lifespan[index].name = 'items['+(index)+'][lifespan]';
            remarks[index].name = 'items['+(index)+'][remarks]';
        });
    }

    function total() {
        setTimeout(function (e) {
            var total = 0;
            $('.items-price').each(function (e) {
                total += parseFloat($(this).val());
            });
            $('#total').val(parseFloat(total));
        });
    }

    function personel(id) {
        $.ajax({
            url:mainurl+'personels/view/'+(parseInt(id)),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend:function (e) {
                $('#office-id').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#office-id').val(data.office_id).prop('disabled', false);
        }).fail(function (data, status, xhr) {

        });
    }

});