'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'accounts/';
    var url = '';
    var isActive = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];
    var isLabeled = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];
    var isType = [
        'None',
        'A',
        'B'
    ];
    var isAppendix = [
        'None',
        'Annex A.8',
        'Appendix-73',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getAccountsDeleted',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(4)').addClass('d-flex justify-content-center align-items-center');
            $(row).find('td:nth-child(6)').addClass('text-center');
            $(row).find('td:nth-child(7)').addClass('text-center');
            $(row).find('td:nth-child(8)').addClass('text-center');
            $(row).find('td:nth-child(9)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return '<div class="color-palette-set" style="height: 40px; width: 40px; background-color: '+(row.color)+';cursor: pointer; border: 1px dashed black;"></div>';
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.start_price).toFixed(2)))+ ' - ' +(new Intl.NumberFormat().format(parseFloat(row.end_price).toFixed(2)));
                }
            },
            {
                targets: [5],
                data: null,
                render: function(data,type,row,meta){
                    return isType[parseInt(row.is_labeled)];
                }
            },
            {
                targets: [6],
                data: null,
                render: function(data,type,row,meta){
                    return isType[parseInt(row.is_type)];
                }
            },
            {
                targets: [7],
                data: null,
                render: function(data,type,row,meta){
                    return isAppendix[parseInt(row.is_appendix)];
                }
            },
            {
                targets: [8],
                data: null,
                render: function(data,type,row,meta){
                    return isActive[parseInt(row.is_active)];
                }
            },
            {
                targets: [10],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.deleted).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [11],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white restore" title="Restore">Restore</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'account'},
            { data: 'legend'},
            { data: 'color'},
            { data: 'start_price'},
            { data: 'is_labeled'},
            { data: 'is_type'},
            { data: 'is_appendix'},
            { data: 'is_active'},
            { data: 'user.name'},
            { data: 'deleted'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.restore',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'restore/'+(parseInt(dataId));
        Swal.fire({
            title: 'Restore Data',
            text: 'Are You Sure?',
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'POST',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'hardDelete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

});