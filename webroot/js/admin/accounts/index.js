'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'accounts/';
    var url = '';
    var isActive = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];
    var isLabeled = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];
    var isType = [
        'None',
        'A',
        'B'
    ];
    var isAppendix = [
        'None',
        'Annex A.8',
        'Appendix-73',
    ];

    var color = $('#color').spectrum({
        color:'#fff',
        chooseText: 'Choose',
        cancelText: 'Close',
        preferredFormat: 'hex',
        flat: false,
        showInput: true,
        allowEmpty:true,
        showPalette: true,
        showAlpha: true,
        showInitial: true,
        clickoutFiresChange: true,
        change: function(e) {
            e.toHexString();
        },
    });

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getAccounts',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(4)').addClass('d-flex justify-content-center align-items-center');
            $(row).find('td:nth-child(6)').addClass('text-center');
            $(row).find('td:nth-child(7)').addClass('text-center');
            $(row).find('td:nth-child(8)').addClass('text-center');
            $(row).find('td:nth-child(9)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return '<div class="color-palette-set" style="height: 40px; width: 40px; background-color: '+(row.color)+';cursor: pointer; border: 1px dashed black;"></div>';
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.start_price).toFixed(2)))+ ' - ' +(new Intl.NumberFormat().format(parseFloat(row.end_price).toFixed(2)));
                }
            },
            {
                targets: [5],
                data: null,
                render: function(data,type,row,meta){
                    return isLabeled[parseInt(row.is_labeled)];
                }
            },
            {
                targets: [6],
                data: null,
                render: function(data,type,row,meta){
                    return isType[parseInt(row.is_type)];
                }
            },
            {
                targets: [7],
                data: null,
                render: function(data,type,row,meta){
                    return isAppendix[parseInt(row.is_appendix)];
                }
            },
            {
                targets: [8],
                data: null,
                render: function(data,type,row,meta){
                    return isActive[parseInt(row.is_active)];
                }
            },
            {
                targets: [10],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [11],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-primary rounded-0 text-white edit" title="Edit">Edit</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'account'},
            { data: 'legend'},
            { data: 'color'},
            { data: 'start_price'},
            { data: 'is_type'},
            { data: 'is_appendix'},
            { data: 'is_active'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.edit',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'edit/'+(parseInt(dataId));
        $.ajax({
            url:href,
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                url = 'edit/'+(parseInt(dataId));
                $('button[type="reset"]').fadeOut(100);
                $('.types').prop('checked', false);
            },
        }).done(function (data, status, xhr) {
            $('#account').val(data.account);
            $('#legend').val(data.legend);
            $('#color').spectrum('set', data.color);
            $('#start-price').val(data.start_price);
            $('#end-price').val(data.end_price);
            $('#is-labeled').val(data.is_labeled);
            $('#labeled').prop('checked', data.is_labeled);
            $('#is-type').val(data.is_type);
            $('.types[value="'+(data.is_type)+'"]').prop('checked', true);
            $('#is-appendix').val(data.is_appendix);
            $('.appendixes[value="'+(data.is_appendix)+'"]').prop('checked', true);
            $('#is-active').val(data.is_active);
            $('#active').prop('checked', data.is_active);
            $('#modal').modal('toggle');
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });

    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl + url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('#modal').modal('toggle');
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#toggle-modal').click(function (e) {
        url = 'add';
        $('#modal').modal('toggle');
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#form')[0].reset();
        $('small').empty();
        $('#color').spectrum('set', '#fff');
        $('.form-control').removeClass('is-invalid');
        $('button[type="reset"]').fadeIn(100);
        $('button[type="reset"], button[type="submit"]').prop('disabled', false);
    });

    $('#account').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#legend').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#start-price, #end-price').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        if(parseInt(value) <= parseInt(0)){
            $(this).addClass('is-invalid').next('small').text('This Field Must Be Higher Than 0');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    }).keypress(function (e) {
        var key = e.key;
        var regex = /^([0-9\.]){1,}$/;

        if(!value.match(regex)){
            e.preventDefault();
        }

    });

    $('#labeled').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-labeled').val(Number(checked));
    });

    $('.types').change(function (e) {
        var value = $(this).val();
        $(this).prop('checked', true);
        $('.types').not(this).prop('checked', false);
        $('#is-type').val(parseInt(value));
    });

    $('.appendixes').change(function (e) {
        var value = $(this).val();
        $(this).prop('checked', true);
        $('.appendixes').not(this).prop('checked', false);
        $('#is-appendix').val(parseInt(value));
    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

});