'use strict';
$(document).ready(function () {

    var url = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            Turbolinks.visit(data.redirect,{action:'replace'});
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

        });
    });

    $('#quantity').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();
        var stored = $('#stored').prop('checked');

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        if(stored && (parseInt(value) < parseInt(0))){
            $(this).addClass('is-invalid').next('small').text('This Field Must Be Higher Than 0');
            return true;
        }

        if(!stored && parseInt(value) > parseInt(stocks)){
            $(this).addClass('is-invalid').next('small').text('This Supply Has Only '+(parseInt(stocks))+' To Pull Out');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    }).keypress(function (e) {
        var key = e.key;
        var regex = /^([0-9\.]){1,}$/;

        if(!key.match(regex)){
            e.preventDefault();
        }

    });

    $('#stored').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-stored').val(Number(checked));
    });

});