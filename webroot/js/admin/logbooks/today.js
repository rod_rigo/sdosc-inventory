'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'logbooks/';
    var url = '';
    var title = function (e) {
        return 'Logbooks Of '+ (moment().format('Y-MM-DD'));
    };
    var isStored = [
        '<span class="text-danger"><i class="fas fa-times"></i> No</span>',
        '<span class="text-success"><i class="fas fa-check"></i> Yes</span>',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getLogbooksToday',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(7)').addClass('text-center');
        },
        buttons: [
            {
                extend: 'print',
                title: 'Print',
                text: 'Print',
                attr:  {
                    id: 'print',
                    class:'btn btn-secondary rounded-0',
                },
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10px' )
                        .prepend('');
                    $(win.document.body).find( 'table tbody' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'excelHtml5',
                attr:  {
                    id: 'excel',
                    class:'btn btn-success rounded-0',
                },
                title: title,
                text: 'Excel',
                tag: 'button',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'Excel',
                        text:'Export To Excel?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed && exports(title(), 'excel')) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'pdfHtml5',
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                text: 'PDF',
                title: title,
                tag: 'button',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'PDF',
                        text:'Export To PDF?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed && exports(title(), 'pdf')) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                customize: function(doc) {
                    doc.pageMargins = [2, 2, 2, 2 ];
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.styles.tableHeader.fontSize = 7;
                    doc.styles.tableBodyEven.fontSize = 7;
                    doc.styles.tableBodyOdd.fontSize = 7;
                    doc.styles.tableFooter.fontSize = 7;
                },
                download: 'open',
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.quantity)));
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.old_stock)));
                }
            },
            {
                targets: [5],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.total_stock)));
                }
            },
            {
                targets: [6],
                data: null,
                render: function(data,type,row,meta){
                    return isStored[parseInt(row.is_stored)];
                }
            },
            {
                targets: [8],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
        ],
        columns: [
            { data: 'id'},
            { data: 'supply.supply'},
            { data: 'logbook'},
            { data: 'quantity'},
            { data: 'old_stock'},
            { data: 'total_stock'},
            { data: 'is_stored'},
            { data: 'user.name'},
            { data: 'modified'},
        ]
    });

});