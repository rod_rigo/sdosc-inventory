'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'dashboards/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    $('#form').submit(function (e) {
        e.preventDefault();
        custodian_month_chart.destroy();
        getCustodiansMonthChart();
        custodian_week_chart.destroy();
        getCustodiansWeekChart();
        item_ledger_chart.destroy();
        getItemLedgersChart();
        item_office_chart.destroy();
        getItemOffices();
        item_slip_chart.destroy();
        getItemSlipsChart();
    });

    function getCounts() {
        $.ajax({
            url: baseurl+'getCounts',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#total-supplies, #total-offices, #total-personels, #total-users').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-supplies').text(parseFloat(data.supplies));
            $('#total-offices').text(parseFloat(data.offices));
            $('#total-personels').text(parseFloat(data.personels));
            $('#total-users').text(parseFloat(data.users));
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getItemCounts() {
        $.ajax({
            url: baseurl+'getItemCounts',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#items-today, #items-week, #items-month, #items-year').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#items-today').text(parseFloat(data.today));
            $('#items-week').text(parseFloat(data.week));
            $('#items-month').text(parseFloat(data.month));
            $('#items-year').text(parseFloat(data.year));
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    var custodian_month_canvas = document.querySelector('#custodian-month-chart').getContext('2d');
    var custodian_month_chart;

    function getCustodiansMonth() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getCustodiansMonth?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.month_name).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getCustodiansMonthChart() {
        custodian_month_chart =  new Chart(custodian_month_canvas, {
            type: 'bar',
            data: {
                labels: getCustodiansMonth().label,
                datasets: [
                    {
                        label:'Total',
                        data: getCustodiansMonth().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 1
                    },
                    title: {
                        display: true,
                        text: 'Custodian By Month',
                        font: {
                            size: 14,
                            family: 'Poppins-Regular'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index),
                                        font: {
                                            size: 14,
                                            family: 'Poppins-Regular'
                                        }
                                    };
                                });
                            },
                            font: {
                                size: 14,
                                family: 'Poppins-Regular'
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var custodian_week_canvas = document.querySelector('#custodian-week-chart').getContext('2d');
    var custodian_week_chart;

    function getCustodiansWeek() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getCustodiansWeek',
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.day).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getCustodiansWeekChart() {
        custodian_week_chart =  new Chart(custodian_week_canvas, {
            type: 'bar',
            data: {
                labels: getCustodiansWeek().label,
                datasets: [
                    {
                        type: 'bar',
                        label:'Total',
                        data: getCustodiansWeek().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 2
                    },
                    title: {
                        display: true,
                        text: 'Custodian This Week',
                        font: {
                            size: 14,
                            family: 'Poppins-Regular'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index),
                                        font: {
                                            size: 14,
                                            family: 'Poppins-Regular'
                                        }
                                    };
                                });
                            },
                            font: {
                                size: 14,
                                family: 'Poppins-Regular'
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var item_ledger_canvas = document.querySelector('#item-ledger-chart').getContext('2d');
    var item_ledger_chart;

    function getItemLedgers() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getItemLedgers?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.ledger).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getItemLedgersChart() {
        item_ledger_chart =  new Chart(item_ledger_canvas, {
            data: {
                labels: getItemLedgers().label,
                datasets: [
                    {
                        type: 'bar',
                        label:'Total',
                        data: getItemLedgers().data
                    },
                    {
                        type: 'line',
                        label:'Total',
                        data: getItemLedgers().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 3
                    },
                    title: {
                        display: true,
                        text: 'Items By Ledger',
                        font: {
                            size: 14,
                            family: 'Poppins-Regular'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index),
                                        font: {
                                            size: 14,
                                            family: 'Poppins-Regular'
                                        }
                                    };
                                });
                            },
                            font: {
                                size: 14,
                                family: 'Poppins-Regular'
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var item_office_canvas = document.querySelector('#item-office-chart').getContext('2d');
    var item_office_chart;

    function getItemOffices() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getItemOffices?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.office).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getItemOfficesChart() {
        item_office_chart =  new Chart(item_office_canvas, {
            type: 'polarArea',
            data: {
                labels: getItemOffices().label,
                datasets: [
                    {
                        label:'Total',
                        data: getItemOffices().data
                    },
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 4
                    },
                    title: {
                        display: true,
                        text: 'Items By Office',
                        font: {
                            size: 14,
                            family: 'Poppins-Regular'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index),
                                        font: {
                                            size: 14,
                                            family: 'Poppins-Regular'
                                        }
                                    };
                                });
                            },
                            font: {
                                size: 14,
                                family: 'Poppins-Regular'
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var item_slip_canvas = document.querySelector('#item-slip-chart').getContext('2d');
    var item_slip_chart;

    function getItemSlips() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getSlips?start_date='+(moment($('#start-date').val()).format('Y-MM-DD'))+'&end_date='+(moment($('#end-date').val()).format('Y-MM-DD')),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.account).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getItemSlipsChart() {
        item_slip_chart =  new Chart(item_slip_canvas, {
            type: 'doughnut',
            data: {
                labels: getItemSlips().label,
                datasets: [
                    {
                        label:'Total',
                        data: getItemSlips().data
                    },
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Slips Per Account',
                        font: {
                            size: 14,
                            family: 'Poppins-Regular'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index),
                                        font: {
                                            size: 14,
                                            family: 'Poppins-Regular'
                                        }
                                    };
                                });
                            },
                            font: {
                                size: 14,
                                family: 'Poppins-Regular'
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    getCounts();
    getItemCounts();
    getCustodiansMonthChart();
    getCustodiansWeekChart();
    getItemLedgersChart();
    getItemOfficesChart();
    getItemSlipsChart();

});