'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'supplies/';
    var url = '';
    var isActive = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getSupplies',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(13)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [2],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.price)));
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.stocks)));
                }
            },
            {
                targets: [12],
                data: null,
                render: function(data,type,row,meta){
                    return isActive[parseInt(row.is_active)];
                }
            },
            {
                targets: [14],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [15],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-secondary rounded-0 text-white manage" title="Manage">Manage</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'supply'},
            { data: 'price'},
            { data: 'stocks'},
            { data: 'issued'},
            { data: 'brand.brand'},
            { data: 'category.category'},
            { data: 'unit.unit'},
            { data: 'account.account'},
            { data: 'ledger.ledger'},
            { data: 'lifespan'},
            { data: 'expired_at'},
            { data: 'is_active'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'view/'+(dataId);
        Swal.fire({
            title: 'Open In New Window',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            denyButtonColor: '#808080',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            denyButtonText: 'Cancel',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }else if(result.isDenied){
                Swal.close();
            }else{
                Turbolinks.visit(href, {action: 'advance'});
            }
        });
    });

    datatable.on('click','.manage',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = mainurl+'logbooks/add/'+(dataId);
        Swal.fire({
            title: 'Manage Stock',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }
        });
    });

});