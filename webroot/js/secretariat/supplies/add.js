'use strict';
$(document).ready(function () {

    var url = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            Swal.fire({
                title: 'Add Another Supply?',
                text: data.message,
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $('#form')[0].reset();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                }else{
                    $('#form')[0].reset();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    Turbolinks.visit(data.redirect,{action:'replace'});
                }
            });
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

        });
    });

    $('#account-id').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        getLedgersList(parseInt(value));
        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#supply, #lifespan,  #category-id, #brand-id, #unit-id').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#stocks, #issued, #price').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        if(parseInt(value) < parseInt(0)){
            $(this).addClass('is-invalid').next('small').text('This Field Must Be Higher Than 0');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    }).keypress(function (e) {
        var key = e.key;
        var regex = /^([0-9\.]){1,}$/;

        if(!key.match(regex)){
            e.preventDefault();
        }

    });

    $('#expired-at').change(function (e) {
        var value = $(this).val();
        var date = moment(value);
        var now = moment();

        if(date < now){
            $(this).addClass('is-invalid').next('small').text('This Field Must Not Be Higher Today');
        }else{
            $(this).removeClass('is-invalid').next('small').empty();
        }

    });

    $('#expired').change(function (e) {
        var checked = $(this).prop('checked');
        if(checked){
            $('#expired-at').prop('required', true).prop('readonly', false).addClass('is-invalid').next('small').text('Please Fill Out This Field');
        }else{
            $('#expired-at').prop('required', false).prop('readonly', true).removeClass('is-invalid').val(null).next('small').empty();
        }
        $('#is-expired').val(Number(checked));
    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

    function getLedgersList(accountId) {
        $.ajax({
            url: mainurl+'ledgers/getLedgersList/'+(parseInt(accountId)),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('#ledger-id').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#ledger-id').prop('disabled', false).html('<option value="">Ledger</option>');
            $.map(data, function (data, key) {
                $('#ledger-id').append('<option value="'+(parseInt(key))+'">'+((data).toUpperCase())+'</option>');
            });
            Swal.close();
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;
            swal('warning', null, data.responseJSON.message);
        });
    }


});