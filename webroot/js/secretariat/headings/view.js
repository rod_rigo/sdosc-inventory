'use strict';
$(document).ready(function () {

    var url = mainurl+'headings/edit/'+(id);

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            Turbolinks.visit(data.redirect,{action:'replace'});
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

        });
    });

    $('#heading').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#total').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        if(parseInt(value) < parseInt(1)){
            $(this).addClass('is-invalid').next('small').text('This Field Must Be Higher Than 0');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    }).keypress(function (e) {
        var key = e.key;
        var regex = /^([0-9\.]){1,}$/;

        if(!key.match(regex)){
            e.preventDefault();
        }

    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(Number(checked));
    });

});
$(document).ready(function () {
    const baseurl = mainurl+'signatories/';
    var url = '';
    var isActive = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getSignatoriesList/'+(id),
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(4)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1 + ('<input type="hidden" class="order-position" value="'+(parseInt(row+1))+'"  data-id="'+(parseInt(full.id))+'" readonly required/>');
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return isActive[parseInt(row.is_active)];
                }
            },
            {
                targets: [5],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
        ],
        columns: [
            { data: 'id'},
            { data: 'position.position'},
            { data: 'signatory'},
            { data: 'is_active'},
            { data: 'user.name'},
            { data: 'modified'},
        ]
    });

    $(document).find('#datatable tbody').sortable({
        revert: true,
        handle: 'td:nth-child(1)',
        start:function( event, ui ){

        },
        update: function( event, ui ) {
            console.log(table.data().toArray());
            orderPosition();
        },
    });

    function orderPosition() {
        var data = [];
        var counter = 0;
        $('.order-position').each(function () {
            var dataId = $(this).attr('data-id');
            counter++;
            data.push({
                id : parseInt(dataId),
                order_position : parseInt(counter)
            });
        });
        $.ajax({
            url: baseurl+'orderPosition',
            type: 'POST',
            method: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            table.ajax.reload(null, false);
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });
    }

});