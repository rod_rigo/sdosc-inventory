'use strict';
$(document).ready(function () {

    var url = window.location.href;

    var select = $('#personel-id').select2({
        placeholder: 'Personel',
        allowClear: true,
        width: '100%'
    });

    $('.remarks, .serial-number, .lifespan').blur(function (e) {
        var dataId = $(this).attr('data-id');
        var value = $(this).val();
        var name = $(this).attr('name');
        var dataOldValue = $(this).attr('data-old-value');
        if(value && (dataOldValue != value)){
            setTimeout(function (e) {
                update(parseInt(dataId), name, value, $(this));
            }, parseInt(500));
        }else{
            $(this).val(dataOldValue);
        }
    }).dblclick(function (e) {
        $(this).prop('disabled', false);
    });

    $('.inventory-number').blur(function (e) {
        var dataId = $(this).attr('data-id');
        var value = $(this).val();
        var name = $(this).attr('name');
        var dataOldValue = $(this).attr('data-old-value');
        if(dataOldValue != value){
            setTimeout(function (e) {
                update(parseInt(dataId), name, value, $(this));
            }, parseInt(500));
        }else{
            $(this).val(dataOldValue);
        }
    }).dblclick(function (e) {
        $(this).prop('disabled', false);
    });

    $('.pdf').click(function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = mainurl+'items/pdf/'+(dataId);
        Swal.fire({
            title: 'Export To PDF',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }
        });
    });

    $('.returned').click(function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = mainurl+'items/returned/'+(dataId);
        Swal.fire({
            title: 'Mark As Returned',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        $('#is-returned-'+(dataId)+'').remove();
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            }
        });
    });

    function update(id, name, value, element) {
        var data = {};
        data[name] = value;
        $.ajax({
            url: mainurl+'items/update/'+(id),
            type: 'POST',
            method: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'JSON',
            global:false,
            async:false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function (e) {
                element.prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            element.prop('disabled', false).attr('data-old-value', value);
            toast('success', null, data.message);
        }).fail(function (data, status, xhr) {

        });
    }

    function toast(icon, result, message){
        Swal.fire({
            icon: icon,
            title: message,
            text: null,
            timer: 1500,
            toast:true,
            position: 'top-right',
            showConfirmButton:false,
            timerProgressBar:true,
        });
    }

});