'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'items/';
    var url = '';
    var title = function (e) {
        return 'Items Of '+ (moment($('#start-date').val()).format('Y-MM-DD'))+ ' - ' +(moment($('#end-date').val()).format('Y-MM-DD'));
    };
    var isReturned = [
        '<span class="text-danger"><i class="fas fa-times"></i> No</span>',
        '<span class="text-success"><i class="fas fa-check"></i> Yes</span>',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getItems',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(13)').addClass('text-center');
        },
        buttons: [
            {
                extend: 'print',
                title: 'Print',
                text: 'Print',
                attr:  {
                    id: 'print',
                    class:'btn btn-secondary rounded-0',
                },
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10px' )
                        .prepend('');
                    $(win.document.body).find( 'table tbody' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'excelHtml5',
                attr:  {
                    id: 'excel',
                    class:'btn btn-success rounded-0',
                },
                title: title,
                text: 'Excel',
                tag: 'button',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'Excel',
                        text:'Export To Excel?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed && exports(title(), 'excel')) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'pdfHtml5',
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                text: 'PDF',
                title: title,
                tag: 'button',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'PDF',
                        text:'Export To PDF?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed && exports(title(), 'pdf')) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                customize: function(doc) {
                    doc.pageMargins = [2, 2, 2, 2 ];
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.styles.tableHeader.fontSize = 7;
                    doc.styles.tableBodyEven.fontSize = 7;
                    doc.styles.tableBodyOdd.fontSize = 7;
                    doc.styles.tableFooter.fontSize = 7;
                },
                download: 'open',
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [9],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.price)));
                }
            },
            {
                targets: [10],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.quantity)));
                }
            },
            {
                targets: [11],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.total)));
                }
            },
            {
                targets: [12],
                data: null,
                render: function(data,type,row,meta){
                    return isReturned[parseInt(row.is_returned)];
                }
            },
            {
                targets: [14],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [15],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    var isReturned = (parseInt(row.is_returned))? '':
                        ' | <a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-success rounded-0 text-white returned" title="Returned">Returned</a>'
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white pdf" title="PDF">PDF</a>' + (isReturned);
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'property_number'},
            { data: 'serial_number'},
            { data: 'inventory_number'},
            { data: 'supply.supply'},
            { data: 'custodian.personel.personel'},
            { data: 'office.office'},
            { data: 'account.account'},
            { data: 'ledger.ledger'},
            { data: 'price'},
            { data: 'quantity'},
            { data: 'total'},
            { data: 'is_returned'},
            { data: 'custodian.user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.pdf',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'pdf/'+(dataId);
        Swal.fire({
            title: 'Export To PDF',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }
        });
    });

    datatable.on('click','.returned',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'returned/'+(dataId);
        Swal.fire({
            title: 'Mark As Returned',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', null, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            }
        });
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var startDate = $('#start-date').val();
        var endDate = $('#end-date').val();
        var limit = $('#limit').val();
        $('#start-date, #end-date, #limit, button[type="submit"]').prop('disabled', true);
        table.ajax.url(baseurl+'getItems?start_date='+(moment(startDate).format('Y-MM-DD'))+'&end_date='+(moment(endDate).format('Y-MM-DD'))+'&limit='+(parseInt(limit))+'').load(function () {
            $('#start-date, #end-date, #limit, button[type="submit"]').prop('disabled', false);
        }, true);
    });

});