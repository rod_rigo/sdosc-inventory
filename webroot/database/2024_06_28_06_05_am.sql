-- mysqldump-php https://github.com/ifsnop/mysqldump-php
--
-- Host: localhost	Database: sdosc_inventory_db
-- ------------------------------------------------------
-- Server version 	8.0.31
-- Date: Fri, 28 Jun 2024 06:05:34 +0000

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40101 SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `legend` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `color` varchar(255) NOT NULL,
  `start_price` double NOT NULL,
  `end_price` double NOT NULL,
  `is_type` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `accounts` VALUES (1,2,'SEMI-EXPENDABLE EQUIPMENT','SPHV','#ffff00',1,4999,2,1,'2024-06-23 07:59:49','2024-06-28 05:12:55',NULL),(2,2,'PROPERTY, PLANT AND EQUIPMENT','PPE','#ffffff',5000,49999,1,1,'2024-06-23 07:59:49','2024-06-28 04:33:03',NULL);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `accounts` with 2 row(s)
--

--
-- Table structure for table `activities`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `model` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `original` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `id` (`user_id`) USING BTREE,
  CONSTRAINT `id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `activities` VALUES (1,2,'ACCOUNTS','EDIT','{\"color\":\"#4b0082\",\"user_id\":1,\"modified\":\"2024-06-27T03:45:03+00:00\",\"id\":1,\"account\":\"SEMI-EXPENDABLE EQUIPMENT\",\"legend\":\"SPHV\",\"start_price\":1,\"end_price\":4999,\"is_type\":0,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT\",\"legend\":\"SPHV\",\"color\":\"#ffff00\",\"start_price\":1,\"end_price\":4999,\"is_type\":0,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"modified\":\"2024-06-28T03:55:20+00:00\",\"deleted\":null}','2024-06-28 03:55:20','2024-06-28 03:55:20',NULL),(2,2,'ACCOUNTS','EDIT','{\"is_type\":0,\"modified\":\"2024-06-28T03:55:20+00:00\",\"id\":1,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT\",\"legend\":\"SPHV\",\"color\":\"#ffff00\",\"start_price\":1,\"end_price\":4999,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT\",\"legend\":\"SPHV\",\"color\":\"#ffff00\",\"start_price\":1,\"end_price\":4999,\"is_type\":1,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"modified\":\"2024-06-28T04:00:38+00:00\",\"deleted\":null}','2024-06-28 04:00:38','2024-06-28 04:00:38',NULL),(3,2,'ACCOUNTS','EDIT','{\"user_id\":1,\"is_type\":0,\"modified\":\"2024-06-27T01:56:50+00:00\",\"id\":2,\"account\":\"PROPERTY, PLANT AND EQUIPMENT\",\"legend\":\"PPE\",\"color\":\"#1b8f4e\",\"start_price\":5000,\"end_price\":49999,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"account\":\"PROPERTY, PLANT AND EQUIPMENT\",\"legend\":\"PPE\",\"color\":\"#1b8f4e\",\"start_price\":5000,\"end_price\":49999,\"is_type\":1,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"modified\":\"2024-06-28T04:00:42+00:00\",\"deleted\":null}','2024-06-28 04:00:42','2024-06-28 04:00:42',NULL),(4,2,'ACCOUNTS','EDIT','{\"color\":\"#1b8f4e\",\"modified\":\"2024-06-28T04:00:42+00:00\",\"id\":2,\"user_id\":2,\"account\":\"PROPERTY, PLANT AND EQUIPMENT\",\"legend\":\"PPE\",\"start_price\":5000,\"end_price\":49999,\"is_type\":1,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"account\":\"PROPERTY, PLANT AND EQUIPMENT\",\"legend\":\"PPE\",\"color\":\"#ffffff\",\"start_price\":5000,\"end_price\":49999,\"is_type\":1,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"modified\":\"2024-06-28T04:33:03+00:00\",\"deleted\":null}','2024-06-28 04:33:03','2024-06-28 04:33:03',NULL),(5,2,'SUPPLIES','EDIT','{\"serial_number\":\"\",\"user_id\":1,\"modified\":\"2024-06-27T07:31:59+00:00\",\"id\":1,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":42,\"issued\":215,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":42,\"issued\":215,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"aaaa\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:04:50+00:00\",\"deleted\":null}','2024-06-28 05:04:50','2024-06-28 05:04:50',NULL),(6,2,'SUPPLIES','EDIT','{\"serial_number\":\"\",\"user_id\":1,\"modified\":\"2024-06-27T06:54:06+00:00\",\"id\":3,\"supply\":\"BOND PAPER\",\"price\":200,\"stocks\":160,\"issued\":41,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":4,\"inventory_number\":\"ssssss\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-25T01:08:05+00:00\",\"deleted\":null}','{\"id\":3,\"user_id\":2,\"supply\":\"BOND PAPER\",\"price\":200,\"stocks\":160,\"issued\":41,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":4,\"serial_number\":\"01\",\"inventory_number\":\"ssssss\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-25T01:08:05+00:00\",\"modified\":\"2024-06-28T05:06:04+00:00\",\"deleted\":null}','2024-06-28 05:06:04','2024-06-28 05:06:04',NULL),(7,2,'SUPPLIES','EDIT','{\"serial_number\":\"\",\"user_id\":1,\"modified\":\"2024-06-26T05:43:49+00:00\",\"id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":78,\"issued\":41,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":78,\"issued\":41,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:06:09+00:00\",\"deleted\":null}','2024-06-28 05:06:09','2024-06-28 05:06:09',NULL),(8,2,'SUPPLIES','EDIT','{\"serial_number\":\"aaaa\",\"modified\":\"2024-06-28T05:04:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":42,\"issued\":215,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":42,\"issued\":215,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:06:15+00:00\",\"deleted\":null}','2024-06-28 05:06:15','2024-06-28 05:06:15',NULL),(9,2,'ACCOUNTS','EDIT','{\"is_type\":1,\"modified\":\"2024-06-28T04:00:38+00:00\",\"id\":1,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT\",\"legend\":\"SPHV\",\"color\":\"#ffff00\",\"start_price\":1,\"end_price\":4999,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT\",\"legend\":\"SPHV\",\"color\":\"#ffff00\",\"start_price\":1,\"end_price\":4999,\"is_type\":2,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"modified\":\"2024-06-28T05:12:55+00:00\",\"deleted\":null}','2024-06-28 05:12:55','2024-06-28 05:12:55',NULL),(10,2,'CUSTODIANS','ADD','{\"no\":4,\"month\":6,\"personel_id\":1,\"office_id\":1,\"total\":20000,\"user_id\":2,\"year\":2024,\"code\":\"2024-06-0004\",\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":4}','{\"personel_id\":1,\"office_id\":1,\"total\":20000,\"user_id\":2,\"no\":4,\"year\":2024,\"month\":\"06\",\"code\":\"2024-06-0004\",\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":4}','2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(11,2,'SLIPS','ADD','{\"no\":4,\"year\":2024,\"month\":6,\"custodian_id\":4,\"account_id\":1,\"code\":\"SPHV-2024-06-0004\",\"total\":10000,\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":5}','{\"custodian_id\":4,\"account_id\":1,\"year\":2024,\"month\":\"06\",\"no\":4,\"code\":\"SPHV-2024-06-0004\",\"total\":10000,\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":5}','2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(12,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":31}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":31,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-31-01\",\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":31}','2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(13,2,'SUPPLIES','EDIT','{\"stocks\":42,\"issued\":215,\"modified\":\"2024-06-28T05:06:15+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":41,\"issued\":216,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"deleted\":null}','2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(14,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":42,\"total_stock\":41,\"is_stored\":0,\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":18}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":42,\"total_stock\":41,\"is_stored\":0,\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":18}','2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(15,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":31,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-31-01\",\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":31}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":31,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-31-01\",\"created\":\"2024-06-28T05:41:49+00:00\",\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":31}','2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(16,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":32}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":32,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-32-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":32}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(17,2,'SUPPLIES','EDIT','{\"stocks\":41,\"issued\":216,\"modified\":\"2024-06-28T05:41:49+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":40,\"issued\":217,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(18,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":41,\"total_stock\":40,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":19}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":41,\"total_stock\":40,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":19}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(19,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":32,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-32-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":32}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":32,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-32-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":32}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(20,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":33}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":33,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-33-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":33}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(21,2,'SUPPLIES','EDIT','{\"stocks\":40,\"issued\":217,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":39,\"issued\":218,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(22,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":40,\"total_stock\":39,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":20}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":40,\"total_stock\":39,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":20}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(23,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":33,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-33-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":33}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":33,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-33-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":33}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(24,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":34}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":34,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-34-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":34}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(25,2,'SUPPLIES','EDIT','{\"stocks\":39,\"issued\":218,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":38,\"issued\":219,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(26,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":39,\"total_stock\":38,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":21}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":39,\"total_stock\":38,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":21}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(27,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":34,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-34-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":34}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":34,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-34-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":34}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(28,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":35}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":35,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-35-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":35}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(29,2,'SUPPLIES','EDIT','{\"stocks\":38,\"issued\":219,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":37,\"issued\":220,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(30,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":38,\"total_stock\":37,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":22}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":38,\"total_stock\":37,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":22}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(31,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":35,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-35-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":35}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":35,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-35-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":35}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(32,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":36}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":36,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-36-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":36}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(33,2,'SUPPLIES','EDIT','{\"stocks\":37,\"issued\":220,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":36,\"issued\":221,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(34,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":37,\"total_stock\":36,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":23}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":37,\"total_stock\":36,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":23}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(35,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":36,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-36-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":36}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":36,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-36-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":36}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(36,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":37}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":37,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-37-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":37}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(37,2,'SUPPLIES','EDIT','{\"stocks\":36,\"issued\":221,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":35,\"issued\":222,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(38,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":36,\"total_stock\":35,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":24}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":36,\"total_stock\":35,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":24}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(39,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":37,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-37-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":37}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":37,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-37-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":37}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(40,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":38}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":38,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-38-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":38}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(41,2,'SUPPLIES','EDIT','{\"stocks\":35,\"issued\":222,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":34,\"issued\":223,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(42,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":35,\"total_stock\":34,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":25}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":35,\"total_stock\":34,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":25}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(43,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":38,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-38-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":38}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":38,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-38-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":38}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(44,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":39}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":39,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-39-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":39}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(45,2,'SUPPLIES','EDIT','{\"stocks\":34,\"issued\":223,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":33,\"issued\":224,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(46,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":34,\"total_stock\":33,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":26}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":34,\"total_stock\":33,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":26}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(47,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":39,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-39-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":39}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":39,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-39-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":39}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(48,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":40}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":40,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-40-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":40}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(49,2,'SUPPLIES','EDIT','{\"stocks\":33,\"issued\":224,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"supply\":\"SHOVEL\",\"price\":1000,\"stocks\":32,\"issued\":225,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":1,\"ledger_id\":2,\"serial_number\":\"0111\",\"inventory_number\":\"\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:07:17+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(50,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":33,\"total_stock\":32,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":27}','{\"user_id\":2,\"supply_id\":1,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":33,\"total_stock\":32,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":27}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(51,2,'ITEMS','ADD','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":40,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-40-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":40}','{\"supply_id\":1,\"price\":1000,\"remarks\":\"-\",\"no\":40,\"account_id\":1,\"ledger_id\":2,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":5,\"logical_id\":5,\"serial_number\":\"2024-06-02-05-40-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":40}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(52,2,'SLIPS','ADD','{\"no\":2,\"year\":2024,\"month\":6,\"custodian_id\":4,\"account_id\":2,\"code\":\"PPE-2024-06-0002\",\"total\":10000,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":6}','{\"custodian_id\":4,\"account_id\":2,\"year\":2024,\"month\":\"06\",\"no\":2,\"code\":\"PPE-2024-06-0002\",\"total\":10000,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":6}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(53,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":41}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":41,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-41-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":41}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(54,2,'SUPPLIES','EDIT','{\"stocks\":78,\"issued\":41,\"modified\":\"2024-06-28T05:06:09+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":77,\"issued\":42,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(55,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":78,\"total_stock\":77,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":28}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":78,\"total_stock\":77,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":28}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(56,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":41,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-41-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":41}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":41,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-41-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":41}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(57,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":42}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":42,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-42-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":42}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(58,2,'SUPPLIES','EDIT','{\"stocks\":77,\"issued\":42,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":76,\"issued\":43,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(59,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":77,\"total_stock\":76,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":29}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":77,\"total_stock\":76,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":29}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(60,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":42,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-42-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":42}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":42,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-42-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":42}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(61,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":43}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":43,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-43-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":43}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(62,2,'SUPPLIES','EDIT','{\"stocks\":76,\"issued\":43,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":75,\"issued\":44,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(63,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":76,\"total_stock\":75,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":30}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":76,\"total_stock\":75,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":30}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(64,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":43,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-43-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":43}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":43,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-43-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":43}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(65,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":44}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":44,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-44-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":44}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(66,2,'SUPPLIES','EDIT','{\"stocks\":75,\"issued\":44,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":74,\"issued\":45,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(67,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":75,\"total_stock\":74,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":31}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":75,\"total_stock\":74,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":31}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(68,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":44,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-44-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":44}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":44,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-44-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":44}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(69,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":45}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":45,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-45-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":45}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(70,2,'SUPPLIES','EDIT','{\"stocks\":74,\"issued\":45,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":73,\"issued\":46,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(71,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":74,\"total_stock\":73,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":32}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":74,\"total_stock\":73,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":32}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(72,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":45,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-45-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":45}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":45,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-45-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":45}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(73,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":46}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":46,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-46-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":46}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(74,2,'SUPPLIES','EDIT','{\"stocks\":73,\"issued\":46,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":72,\"issued\":47,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(75,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":73,\"total_stock\":72,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":33}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":73,\"total_stock\":72,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":33}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(76,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":46,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-46-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":46}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":46,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-46-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":46}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(77,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":47}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":47,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-47-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":47}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(78,2,'SUPPLIES','EDIT','{\"stocks\":72,\"issued\":47,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":71,\"issued\":48,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(79,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":72,\"total_stock\":71,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":34}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":72,\"total_stock\":71,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":34}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(80,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":47,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-47-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":47}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":47,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-47-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":47}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(81,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":48}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":48,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-48-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":48}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(82,2,'SUPPLIES','EDIT','{\"stocks\":71,\"issued\":48,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":70,\"issued\":49,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(83,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":71,\"total_stock\":70,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":35}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":71,\"total_stock\":70,\"is_stored\":0,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":35}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(84,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":48,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-48-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":48}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":48,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-48-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":48}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(85,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":49}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":49,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-49-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":49}','2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(86,2,'SUPPLIES','EDIT','{\"stocks\":70,\"issued\":49,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":69,\"issued\":50,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"deleted\":null}','2024-06-28 05:41:51','2024-06-28 05:41:51',NULL),(87,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":70,\"total_stock\":69,\"is_stored\":0,\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":36}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":70,\"total_stock\":69,\"is_stored\":0,\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":36}','2024-06-28 05:41:51','2024-06-28 05:41:51',NULL),(88,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":49,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-49-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":49}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":49,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-49-01\",\"created\":\"2024-06-28T05:41:50+00:00\",\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":49}','2024-06-28 05:41:51','2024-06-28 05:41:51',NULL),(89,2,'ITEMS','EDIT','{\"no\":0,\"serial_number\":\"-\",\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":50}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":50,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-50-01\",\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":50}','2024-06-28 05:41:51','2024-06-28 05:41:51',NULL),(90,2,'SUPPLIES','EDIT','{\"stocks\":69,\"issued\":50,\"modified\":\"2024-06-28T05:41:50+00:00\",\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"deleted\":null}','{\"id\":2,\"user_id\":2,\"supply\":\"BULDOZER\",\"price\":1000,\"stocks\":68,\"issued\":51,\"brand_id\":1,\"category_id\":1,\"unit_id\":1,\"account_id\":2,\"ledger_id\":20,\"serial_number\":\"01\",\"inventory_number\":\"asdasdasd\",\"is_active\":1,\"is_expired\":0,\"expired_at\":null,\"created\":\"2024-06-24T03:09:41+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"deleted\":null}','2024-06-28 05:41:51','2024-06-28 05:41:51',NULL),(91,2,'LOGBOOKS','ADD','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":69,\"total_stock\":68,\"is_stored\":0,\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":37}','{\"user_id\":2,\"supply_id\":2,\"logbook\":\"ISSUANCE\",\"quantity\":1,\"old_stock\":69,\"total_stock\":68,\"is_stored\":0,\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":37}','2024-06-28 05:41:51','2024-06-28 05:41:51',NULL),(92,2,'ITEMS','ADD','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":50,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-50-01\",\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":50}','{\"supply_id\":2,\"price\":1000,\"remarks\":\"-\",\"no\":50,\"account_id\":2,\"ledger_id\":20,\"office_id\":1,\"year\":2024,\"month\":6,\"quantity\":1,\"total\":1000,\"is_returned\":0,\"custodian_id\":4,\"slip_id\":6,\"logical_id\":6,\"serial_number\":\"2024-06-07-05-50-01\",\"created\":\"2024-06-28T05:41:51+00:00\",\"modified\":\"2024-06-28T05:41:51+00:00\",\"id\":50}','2024-06-28 05:41:51','2024-06-28 05:41:51',NULL);
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `activities` with 92 row(s)
--

--
-- Table structure for table `brands`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `brand` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `brands to users` (`user_id`) USING BTREE,
  CONSTRAINT `brands to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `brands` VALUES (1,1,'Acer',1,'2024-06-23 06:01:06','2024-06-23 06:01:18',NULL);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `brands` with 1 row(s)
--

--
-- Table structure for table `categories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `categories to users` (`user_id`) USING BTREE,
  CONSTRAINT `categories to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `categories` VALUES (1,1,'Laptop',1,'2024-06-23 06:08:27','2024-06-23 06:12:53',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `categories` with 1 row(s)
--

--
-- Table structure for table `custodians`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custodians` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `personel_id` bigint unsigned NOT NULL,
  `office_id` bigint unsigned NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `no` double NOT NULL,
  `year` double NOT NULL,
  `month` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `custodians to users` (`user_id`) USING BTREE,
  KEY `custodians to personels` (`personel_id`) USING BTREE,
  KEY `custodians to offices` (`office_id`) USING BTREE,
  CONSTRAINT `custodians to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`),
  CONSTRAINT `custodians to personels` FOREIGN KEY (`personel_id`) REFERENCES `personels` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `custodians to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custodians`
--

LOCK TABLES `custodians` WRITE;
/*!40000 ALTER TABLE `custodians` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `custodians` VALUES (1,1,1,1,'2024-06-0001',1,2024,6,5000,'2024-06-26 02:07:31','2024-06-26 02:07:31',NULL),(2,1,1,1,'2024-06-0002',2,2024,6,11000,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(3,1,1,1,'2024-06-0003',3,2024,6,10000,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(4,2,1,1,'2024-06-0004',4,2024,6,20000,'2024-06-28 05:41:49','2024-06-28 05:41:49',NULL);
/*!40000 ALTER TABLE `custodians` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `custodians` with 4 row(s)
--

--
-- Table structure for table `databases`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databases` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `databases to users` (`user_id`) USING BTREE,
  CONSTRAINT `databases to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databases`
--

LOCK TABLES `databases` WRITE;
/*!40000 ALTER TABLE `databases` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `databases` VALUES (1,1,'database/2024_06_27_06_13_am.sql','2024-06-27 06:13:36','2024-06-27 06:13:36',NULL),(2,1,'database/2024_06_27_07_16_am.sql','2024-06-27 07:16:28','2024-06-27 07:16:28',NULL);
/*!40000 ALTER TABLE `databases` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `databases` with 2 row(s)
--

--
-- Table structure for table `exports`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exports` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `export` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `mime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `exports to users` (`user_id`) USING BTREE,
  CONSTRAINT `exports to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exports`
--

LOCK TABLES `exports` WRITE;
/*!40000 ALTER TABLE `exports` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `exports` VALUES (1,2,'ITEMS OF 2024-01-01 - 2024-12-31','EXCEL','2024-06-27 07:40:49','2024-06-27 07:40:49',NULL),(2,2,'.PDF','PDF','2024-06-28 05:52:13','2024-06-28 05:52:13',NULL),(3,2,'2024-06-07-05-50-01.PDF','PDF','2024-06-28 05:53:57','2024-06-28 05:53:57',NULL),(4,2,'2024-06-07-05-50-01.PDF','PDF','2024-06-28 05:54:47','2024-06-28 05:54:47',NULL),(5,2,'2024-06-0004.PDF','PDF','2024-06-28 05:55:48','2024-06-28 05:55:48',NULL),(6,2,'PPE-2024-06-0002.PDF','PDF','2024-06-28 06:01:40','2024-06-28 06:01:40',NULL),(7,2,'SPHV-2024-06-0004.PDF','PDF','2024-06-28 06:02:13','2024-06-28 06:02:13',NULL),(8,2,'PPE-2024-06-0002.PDF','PDF','2024-06-28 06:04:35','2024-06-28 06:04:35',NULL);
/*!40000 ALTER TABLE `exports` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `exports` with 8 row(s)
--

--
-- Table structure for table `headings`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `headings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `heading` varchar(255) NOT NULL,
  `total` double NOT NULL,
  `order_position` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `headings to users` (`user_id`),
  CONSTRAINT `headings to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `headings`
--

LOCK TABLES `headings` WRITE;
/*!40000 ALTER TABLE `headings` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `headings` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `headings` with 0 row(s)
--

--
-- Table structure for table `items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `custodian_id` bigint unsigned NOT NULL,
  `logical_id` bigint unsigned NOT NULL,
  `slip_id` bigint unsigned NOT NULL,
  `supply_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `ledger_id` bigint unsigned NOT NULL,
  `office_id` bigint unsigned NOT NULL,
  `no` double NOT NULL,
  `year` double NOT NULL,
  `month` double NOT NULL,
  `quantity` double NOT NULL,
  `price` double NOT NULL,
  `total` double NOT NULL,
  `serial_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_returned` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `items to slips` (`slip_id`) USING BTREE,
  KEY `items to supplies` (`supply_id`) USING BTREE,
  KEY `items to accounts` (`account_id`) USING BTREE,
  KEY `items to offices` (`office_id`) USING BTREE,
  KEY `items to custodians` (`custodian_id`) USING BTREE,
  KEY `items to ledgers` (`ledger_id`) USING BTREE,
  CONSTRAINT `items to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `items to custodians` FOREIGN KEY (`custodian_id`) REFERENCES `custodians` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `items to ledgers` FOREIGN KEY (`ledger_id`) REFERENCES `ledgers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `items to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `items to slips` FOREIGN KEY (`slip_id`) REFERENCES `slips` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `items to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `items` VALUES (1,1,1,1,1,1,2,1,1,2024,6,1,1000,1000,'2024-06-02-05-1-01','-',1,'2024-06-26 02:07:31','2024-06-27 07:13:36',NULL),(2,1,1,1,1,1,2,1,2,2024,6,1,1000,1000,'2024-06-02-05-2-01','-',0,'2024-06-26 02:07:31','2024-06-26 02:07:31',NULL),(3,1,1,1,1,1,2,1,3,2024,6,1,1000,1000,'2024-06-02-05-3-01','-',0,'2024-06-26 02:07:31','2024-06-26 02:07:31',NULL),(4,1,1,1,1,1,2,1,4,2024,6,1,1000,1000,'2024-06-02-05-4-01','-',0,'2024-06-26 02:07:31','2024-06-26 02:07:31',NULL),(5,1,1,1,1,1,2,1,5,2024,6,1,1000,1000,'2024-06-02-05-5-01','-',0,'2024-06-26 02:07:31','2024-06-26 02:07:31',NULL),(6,2,2,2,2,2,20,1,6,2024,6,1,1000,1000,'2024-06-07-05-6-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(7,2,2,2,2,2,20,1,7,2024,6,1,1000,1000,'2024-06-07-05-7-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(8,2,2,2,2,2,20,1,8,2024,6,1,1000,1000,'2024-06-07-05-8-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(9,2,2,2,2,2,20,1,9,2024,6,1,1000,1000,'2024-06-07-05-9-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(10,2,2,2,2,2,20,1,10,2024,6,1,1000,1000,'2024-06-07-05-10-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(11,2,3,3,3,1,4,1,11,2024,6,1,200,200,'2024-06-07-05-11-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(12,2,3,3,3,1,4,1,12,2024,6,1,200,200,'2024-06-07-05-12-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(13,2,3,3,3,1,4,1,13,2024,6,1,200,200,'2024-06-07-05-13-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(14,2,3,3,3,1,4,1,14,2024,6,1,200,200,'2024-06-07-05-14-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(15,2,3,3,3,1,4,1,15,2024,6,1,200,200,'2024-06-07-05-15-01','-',0,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(16,2,3,3,1,1,2,1,16,2024,6,1,1000,1000,'2024-06-02-05-16-01','-',0,'2024-06-26 05:43:50','2024-06-26 05:43:50',NULL),(17,2,3,3,1,1,2,1,17,2024,6,1,1000,1000,'2024-06-02-05-17-01','-',0,'2024-06-26 05:43:50','2024-06-26 05:43:50',NULL),(18,2,3,3,1,1,2,1,18,2024,6,1,1000,1000,'2024-06-02-05-18-01','-',0,'2024-06-26 05:43:50','2024-06-26 05:43:50',NULL),(19,2,3,3,1,1,2,1,19,2024,6,1,1000,1000,'2024-06-02-05-19-01','-',0,'2024-06-26 05:43:50','2024-06-26 05:43:50',NULL),(20,2,3,3,1,1,2,1,20,2024,6,1,1000,1000,'2024-06-02-05-20-01','-',0,'2024-06-26 05:43:50','2024-06-26 05:43:50',NULL),(21,3,4,4,1,1,2,1,21,2024,6,1,1000,1000,'2024-06-02-05-21-01','-',1,'2024-06-27 05:53:20','2024-06-27 07:31:59',NULL),(22,3,4,4,1,1,2,1,22,2024,6,1,1000,1000,'2024-06-02-05-22-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(23,3,4,4,1,1,2,1,23,2024,6,1,1000,1000,'2024-06-02-05-23-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(24,3,4,4,1,1,2,1,24,2024,6,1,1000,1000,'2024-06-02-05-24-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(25,3,4,4,1,1,2,1,25,2024,6,1,1000,1000,'2024-06-02-05-25-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(26,3,4,4,1,1,2,1,26,2024,6,1,1000,1000,'2024-06-02-05-26-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(27,3,4,4,1,1,2,1,27,2024,6,1,1000,1000,'2024-06-02-05-27-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(28,3,4,4,1,1,2,1,28,2024,6,1,1000,1000,'2024-06-02-05-28-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(29,3,4,4,1,1,2,1,29,2024,6,1,1000,1000,'2024-06-02-05-29-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(30,3,4,4,1,1,2,1,30,2024,6,1,1000,1000,'2024-06-02-05-30-01','-',0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(31,4,5,5,1,1,2,1,31,2024,6,1,1000,1000,'2024-06-02-05-31-01','-',0,'2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(32,4,5,5,1,1,2,1,32,2024,6,1,1000,1000,'2024-06-02-05-32-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(33,4,5,5,1,1,2,1,33,2024,6,1,1000,1000,'2024-06-02-05-33-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(34,4,5,5,1,1,2,1,34,2024,6,1,1000,1000,'2024-06-02-05-34-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(35,4,5,5,1,1,2,1,35,2024,6,1,1000,1000,'2024-06-02-05-35-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(36,4,5,5,1,1,2,1,36,2024,6,1,1000,1000,'2024-06-02-05-36-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(37,4,5,5,1,1,2,1,37,2024,6,1,1000,1000,'2024-06-02-05-37-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(38,4,5,5,1,1,2,1,38,2024,6,1,1000,1000,'2024-06-02-05-38-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(39,4,5,5,1,1,2,1,39,2024,6,1,1000,1000,'2024-06-02-05-39-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(40,4,5,5,1,1,2,1,40,2024,6,1,1000,1000,'2024-06-02-05-40-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(41,4,6,6,2,2,20,1,41,2024,6,1,1000,1000,'2024-06-07-05-41-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(42,4,6,6,2,2,20,1,42,2024,6,1,1000,1000,'2024-06-07-05-42-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(43,4,6,6,2,2,20,1,43,2024,6,1,1000,1000,'2024-06-07-05-43-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(44,4,6,6,2,2,20,1,44,2024,6,1,1000,1000,'2024-06-07-05-44-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(45,4,6,6,2,2,20,1,45,2024,6,1,1000,1000,'2024-06-07-05-45-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(46,4,6,6,2,2,20,1,46,2024,6,1,1000,1000,'2024-06-07-05-46-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(47,4,6,6,2,2,20,1,47,2024,6,1,1000,1000,'2024-06-07-05-47-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(48,4,6,6,2,2,20,1,48,2024,6,1,1000,1000,'2024-06-07-05-48-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(49,4,6,6,2,2,20,1,49,2024,6,1,1000,1000,'2024-06-07-05-49-01','-',0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(50,4,6,6,2,2,20,1,50,2024,6,1,1000,1000,'2024-06-07-05-50-01','-',0,'2024-06-28 05:41:51','2024-06-28 05:41:51',NULL);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `items` with 50 row(s)
--

--
-- Table structure for table `ledgers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ledgers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `ledger` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `legend` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sub_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ledgers to users` (`user_id`) USING BTREE,
  KEY `ledgers to accounts` (`account_id`) USING BTREE,
  CONSTRAINT `ledgers to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ledgers to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ledgers`
--

LOCK TABLES `ledgers` WRITE;
/*!40000 ALTER TABLE `ledgers` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `ledgers` VALUES (1,1,1,'SEMI-EXPENDABLE MACHINERY','SEMI-EXPENDABLE MACHINERY','05','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(2,1,1,'SEMI-EXPENDABLE OFFICE EQUIPMENT','SEMI-EXPENDABLE OFFICE EQUIPMENT','05','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(3,1,1,'SEMI-EXPENDABLE INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','SEMI-EXPENDABLE INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','05','03',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(4,1,1,'SEMI-EXPENDABLE COMMUNICATION EQUIPMENT','SEMI-EXPENDABLE COMMUNICATION EQUIPMENT','05','07',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(5,1,1,'SEMI-EXPENDABLE DISASTER RESPONSE AND RESCUE EQUIPMENT','SEMI-EXPENDABLE DISASTER RESPONSE AND RESCUE EQUIPMENT','05','08',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(6,1,1,'SEMI-EXPENDABLE MEDICAL EQUIPMENT','SEMI-EXPENDABLE MEDICAL EQUIPMENT','05','10',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(7,1,1,'SEMI-EXPENDABLE PRINTING EQUIPMENT','SEMI-EXPENDABLE PRINTING EQUIPMENT','05','11',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(8,1,1,'SEMI-EXPENDABLE SPORTS EQUIPMENT','SEMI-EXPENDABLE SPORTS EQUIPMENT','05','12',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(9,1,1,'SEMI-EXPENDABLE TECHNICAL AND SCIENTIFIC EQUIPMENT','SEMI-EXPENDABLE TECHNICAL AND SCIENTIFIC EQUIPMENT','05','13',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(10,1,1,'SEMI-EXPENDABLE GAMING EQUIPMENT','SEMI-EXPENDABLE GAMING EQUIPMENT','05','15',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(11,1,1,'SEMI-EXPENDABLE OTHER MACHINERY AND EQUIPMENT','SEMI-EXPENDABLE OTHER MACHINERY AND EQUIPMENT','05','19',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(12,1,1,'SEMI-EXPENDABLE FURNITURES AND FIXTURES','SEMI-EXPENDABLE FURNITURES AND FIXTURES','06','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(13,1,1,'SEMI-EXPENDABLE BOOKS','SEMI-EXPENDABLE BOOKS','06','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(14,1,2,'LAND','LAND','01','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(15,1,2,'BUILDINGS','BUILDINGS','04','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(16,1,2,'SCHOOL BUILDINGS','SCHOOL BUILDINGS','04','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(17,1,2,'MACHINERY','MACHINERY','05','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(18,1,2,'OFFICE EQUIPMENT','OFFICE EQUIPMENT','05','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(19,1,2,'INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','05','03',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(20,1,2,'COMMUNICATION EQUIPMENT','COMMUNICATION EQUIPMENT','05','07',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(21,1,2,'DISASTER RESPONSE AND RESCUE EQUIPMENT','DISASTER RESPONSE AND RESCUE EQUIPMENT','05','08',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(22,1,2,'MEDICAL EQUIPMENT','MEDICAL EQUIPMENT','05','10',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(23,1,2,'PRINTING EQUIPMENT','PRINTING EQUIPMENT','05','11',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(24,1,2,'SPORTS EQUIPMENT','SPORTS EQUIPMENT','05','12',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(25,1,2,'OTHER MACHINERY AND EQUIPMENT','OTHER MACHINERY AND EQUIPMENT','05','19',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(26,1,2,'MOTOR VEHICLES','MOTOR VEHICLES','06','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(27,1,2,'FURNITURE AND FIXTURES','FURNITURE AND FIXTURES','07','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL);
/*!40000 ALTER TABLE `ledgers` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `ledgers` with 27 row(s)
--

--
-- Table structure for table `logbooks`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logbooks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `supply_id` bigint unsigned NOT NULL,
  `logbook` varchar(255) NOT NULL,
  `quantity` double NOT NULL,
  `old_stock` double NOT NULL,
  `total_stock` double NOT NULL,
  `is_stored` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logbooks to users` (`user_id`),
  KEY `logbooks to supplies` (`supply_id`),
  CONSTRAINT `logbooks to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`),
  CONSTRAINT `logbooks to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logbooks`
--

LOCK TABLES `logbooks` WRITE;
/*!40000 ALTER TABLE `logbooks` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `logbooks` VALUES (1,1,1,'ISSUANCE',1,47,46,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(2,1,1,'ISSUANCE',1,46,45,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(3,1,1,'ISSUANCE',1,45,44,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(4,1,1,'ISSUANCE',1,44,43,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(5,1,1,'ISSUANCE',1,43,42,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(6,1,1,'ISSUANCE',1,42,41,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(7,1,1,'ISSUANCE',1,41,40,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(8,1,1,'ISSUANCE',1,40,39,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(9,1,1,'ISSUANCE',1,39,38,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(10,1,1,'ISSUANCE',1,38,37,0,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(11,1,3,'STORED',73,73,146,1,'2024-06-27 06:52:05','2024-06-27 06:52:05',NULL),(12,1,3,'STORED',4,146,150,1,'2024-06-27 06:53:36','2024-06-27 06:53:36',NULL),(13,1,3,'STORED',10,150,160,1,'2024-06-27 06:54:06','2024-06-27 06:54:06',NULL),(14,1,1,'STORED',3,37,40,1,'2024-06-27 06:56:49','2024-06-27 06:56:49',NULL),(16,1,1,'',1,40,41,0,'2024-06-27 07:13:36','2024-06-27 07:13:36',NULL),(17,1,1,'',1,41,42,0,'2024-06-27 07:31:59','2024-06-27 07:31:59',NULL),(18,2,1,'ISSUANCE',1,42,41,0,'2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(19,2,1,'ISSUANCE',1,41,40,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(20,2,1,'ISSUANCE',1,40,39,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(21,2,1,'ISSUANCE',1,39,38,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(22,2,1,'ISSUANCE',1,38,37,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(23,2,1,'ISSUANCE',1,37,36,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(24,2,1,'ISSUANCE',1,36,35,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(25,2,1,'ISSUANCE',1,35,34,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(26,2,1,'ISSUANCE',1,34,33,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(27,2,1,'ISSUANCE',1,33,32,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(28,2,2,'ISSUANCE',1,78,77,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(29,2,2,'ISSUANCE',1,77,76,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(30,2,2,'ISSUANCE',1,76,75,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(31,2,2,'ISSUANCE',1,75,74,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(32,2,2,'ISSUANCE',1,74,73,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(33,2,2,'ISSUANCE',1,73,72,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(34,2,2,'ISSUANCE',1,72,71,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(35,2,2,'ISSUANCE',1,71,70,0,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL),(36,2,2,'ISSUANCE',1,70,69,0,'2024-06-28 05:41:51','2024-06-28 05:41:51',NULL),(37,2,2,'ISSUANCE',1,69,68,0,'2024-06-28 05:41:51','2024-06-28 05:41:51',NULL);
/*!40000 ALTER TABLE `logbooks` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `logbooks` with 36 row(s)
--

--
-- Table structure for table `logos`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `logo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `width` double NOT NULL,
  `height` double NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `logos to users` (`user_id`) USING BTREE,
  CONSTRAINT `logos to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logos`
--

LOCK TABLES `logos` WRITE;
/*!40000 ALTER TABLE `logos` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `logos` VALUES (2,1,'SDO','logo-img/6678ed12c233c.jpeg',4096,4096,1,'2024-06-24 03:48:40','2024-06-24 03:50:42',NULL);
/*!40000 ALTER TABLE `logos` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `logos` with 1 row(s)
--

--
-- Table structure for table `offices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `office` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `legend` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `offices to users` (`user_id`) USING BTREE,
  CONSTRAINT `offices to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `offices` VALUES (1,1,'ACCOUNTANT  1','ACCOUNTANT 1','01',1,'2024-06-23 05:48:10','2024-06-23 05:53:05',NULL);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `offices` with 1 row(s)
--

--
-- Table structure for table `personels`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personels` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `position_id` bigint unsigned NOT NULL,
  `personel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `officials to users` (`user_id`) USING BTREE,
  KEY `officials to positions` (`position_id`) USING BTREE,
  CONSTRAINT `officials to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `officials to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personels`
--

LOCK TABLES `personels` WRITE;
/*!40000 ALTER TABLE `personels` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `personels` VALUES (1,1,1,'JEFF',1,'2024-06-23 06:47:31','2024-06-23 06:50:23',NULL);
/*!40000 ALTER TABLE `personels` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `personels` with 1 row(s)
--

--
-- Table structure for table `positions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `positions to users` (`user_id`) USING BTREE,
  CONSTRAINT `positions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `positions` VALUES (1,1,'Administrative Officer IV',1,'2024-06-23 06:33:59','2024-06-23 06:47:18',NULL);
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `positions` with 1 row(s)
--

--
-- Table structure for table `remember_me_phinxlog`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remember_me_phinxlog` (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remember_me_phinxlog`
--

LOCK TABLES `remember_me_phinxlog` WRITE;
/*!40000 ALTER TABLE `remember_me_phinxlog` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013,'CreateRememberMeTokens','2024-06-22 19:02:21','2024-06-22 19:02:21',0);
/*!40000 ALTER TABLE `remember_me_phinxlog` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `remember_me_phinxlog` with 1 row(s)
--

--
-- Table structure for table `remember_me_tokens`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remember_me_tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `U_token_identifier` (`model`,`foreign_id`,`series`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remember_me_tokens`
--

LOCK TABLES `remember_me_tokens` WRITE;
/*!40000 ALTER TABLE `remember_me_tokens` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `remember_me_tokens` VALUES (1,'2024-06-23 05:35:58','2024-06-23 05:35:58','Users','1','2a871623fb8c83b9f4b5940e6dfac34dde0d3282','7ae762dda0a46b1cae6ce4463bd1c60a4f58bc05','2024-07-23 05:35:58'),(2,'2024-06-24 01:22:08','2024-06-25 04:31:47','Users','1','a8c67bab260ab8d9a6732e2733d47ca307928b05','e4d588e04c6069cd8f52f61734ff067dc6c9afdf','2024-07-25 04:31:47'),(5,'2024-06-26 01:26:30','2024-06-26 07:20:44','Users','1','dbe5f788c9c89f57e1cd1c1d8ae09bc6f3ce7222','365f99ad8e86dd64bdb2cdbff54328e20ae5c489','2024-07-26 07:20:44'),(7,'2024-06-27 07:39:21','2024-06-28 02:00:53','Users','2','a159d9c5d30e956748f1b72acea06b08d6960154','d3cbf37d72f3f215d0dcbf615abebf99bd889737','2024-07-28 02:00:53');
/*!40000 ALTER TABLE `remember_me_tokens` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `remember_me_tokens` with 4 row(s)
--

--
-- Table structure for table `signatories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signatories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `position_id` bigint unsigned NOT NULL,
  `heading_id` bigint unsigned NOT NULL,
  `signatory` varchar(255) NOT NULL,
  `order_position` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `signatories to users` (`user_id`),
  KEY `signatories to headings` (`heading_id`),
  KEY `signatories to positions` (`position_id`),
  CONSTRAINT `signatories to headings` FOREIGN KEY (`heading_id`) REFERENCES `headings` (`id`),
  CONSTRAINT `signatories to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`),
  CONSTRAINT `signatories to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signatories`
--

LOCK TABLES `signatories` WRITE;
/*!40000 ALTER TABLE `signatories` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `signatories` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `signatories` with 0 row(s)
--

--
-- Table structure for table `slips`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slips` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `custodian_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `no` double NOT NULL,
  `year` double NOT NULL,
  `month` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `slips to accounts` (`account_id`) USING BTREE,
  KEY `slips to custodians` (`custodian_id`) USING BTREE,
  CONSTRAINT `slips to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `slips to custodians` FOREIGN KEY (`custodian_id`) REFERENCES `custodians` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slips`
--

LOCK TABLES `slips` WRITE;
/*!40000 ALTER TABLE `slips` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `slips` VALUES (1,1,1,'SPHV-2024-06-0001',1,2024,6,5000,'2024-06-26 02:07:31','2024-06-26 02:07:31',NULL),(2,2,2,'PPE-2024-06-0001',1,2024,6,5000,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(3,2,1,'SPHV-2024-06-0002',2,2024,6,6000,'2024-06-26 05:43:49','2024-06-26 05:43:49',NULL),(4,3,1,'SPHV-2024-06-0003',3,2024,6,10000,'2024-06-27 05:53:20','2024-06-27 05:53:20',NULL),(5,4,1,'SPHV-2024-06-0004',4,2024,6,10000,'2024-06-28 05:41:49','2024-06-28 05:41:49',NULL),(6,4,2,'PPE-2024-06-0002',2,2024,6,10000,'2024-06-28 05:41:50','2024-06-28 05:41:50',NULL);
/*!40000 ALTER TABLE `slips` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `slips` with 6 row(s)
--

--
-- Table structure for table `supplies`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `supply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `price` double NOT NULL,
  `stocks` double NOT NULL,
  `issued` double NOT NULL,
  `brand_id` bigint unsigned NOT NULL,
  `category_id` bigint unsigned NOT NULL,
  `unit_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `ledger_id` bigint unsigned NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `inventory_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `is_expired` tinyint NOT NULL DEFAULT '0',
  `expired_at` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `supplies to brands` (`brand_id`) USING BTREE,
  KEY `supplies to categories` (`category_id`) USING BTREE,
  KEY `supplies to units` (`unit_id`) USING BTREE,
  KEY `supplies to users` (`user_id`) USING BTREE,
  KEY `supplies to accounts` (`account_id`) USING BTREE,
  KEY `supplies to ledgers` (`ledger_id`) USING BTREE,
  CONSTRAINT `supplies to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to brands` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to ledgers` FOREIGN KEY (`ledger_id`) REFERENCES `ledgers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supplies to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplies`
--

LOCK TABLES `supplies` WRITE;
/*!40000 ALTER TABLE `supplies` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `supplies` VALUES (1,2,'SHOVEL',1000,32,225,1,1,1,1,2,'0111','',1,0,NULL,'2024-06-24 03:07:17','2024-06-28 05:41:50',NULL),(2,2,'BULDOZER',1000,68,51,1,1,1,2,20,'01','asdasdasd',1,0,NULL,'2024-06-24 03:09:41','2024-06-28 05:41:51',NULL),(3,2,'BOND PAPER',200,160,41,1,1,1,1,4,'01','ssssss',1,0,NULL,'2024-06-25 01:08:05','2024-06-28 05:06:04',NULL);
/*!40000 ALTER TABLE `supplies` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `supplies` with 3 row(s)
--

--
-- Table structure for table `units`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `units to users` (`user_id`) USING BTREE,
  CONSTRAINT `units to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `units` VALUES (1,1,'Units',1,'2024-06-23 06:22:19','2024-06-23 06:23:19',NULL);
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `units` with 1 row(s)
--

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_admin` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `users` VALUES (1,'RODRIGO','rods','cabotaje.rodrigo@gmail.com','$2y$10$ljQoVM5RlGJFFKwButTAruLFffRADz4uBcIK/whAm4OT6EUgBj6oW','667cf58ca53f1667cf58ca53f3',1,1,'2024-06-23 05:25:58','2024-06-27 05:15:56',NULL),(2,'ADMIN','admin','admin@gmail.com','$2y$10$fkQ54jCYXNiHen8J8wHreuClUJ9Hmcx0HZzUFpRPqfY4jsDpmqsgq','667d1729ed535667d1729ed539',1,1,'2024-06-27 04:58:41','2024-06-27 07:39:21',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `users` with 2 row(s)
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET AUTOCOMMIT=@OLD_AUTOCOMMIT */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on: Fri, 28 Jun 2024 06:05:34 +0000
