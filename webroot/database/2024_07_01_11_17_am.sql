-- mysqldump-php https://github.com/ifsnop/mysqldump-php
--
-- Host: localhost	Database: sdosc_inventory_db
-- ------------------------------------------------------
-- Server version 	8.0.31
-- Date: Mon, 01 Jul 2024 03:17:23 +0000

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40101 SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `account` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `legend` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `start_price` double NOT NULL,
  `end_price` double NOT NULL,
  `is_labeled` tinyint NOT NULL DEFAULT '0',
  `is_type` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `accounts` VALUES (1,2,'SEMI-EXPENDABLE EQUIPMENT HIGH VALUE','SPHV','#ffffff',5000,49999,1,1,1,'2024-06-23 07:59:49','2024-07-01 03:17:06',NULL),(2,2,'PROPERTY, PLANT AND EQUIPMENT','PPE','#ffff00',50000,50000000,0,1,1,'2024-06-23 07:59:49','2024-06-29 05:33:21',NULL),(3,2,'SEMI-EXPENDABLE EQUIPMENT LOW VALUE','SPLV','#008000',1,4999,1,2,1,'2024-06-29 05:33:59','2024-07-01 03:17:12',NULL);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `accounts` with 3 row(s)
--

--
-- Table structure for table `activities`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `original` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `id` (`user_id`) USING BTREE,
  CONSTRAINT `id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `activities` VALUES (1,2,'ACCOUNTS','EDIT','{\"is_labeled\":0,\"modified\":\"2024-06-29T05:32:18+00:00\",\"id\":1,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT HIGH VALUE\",\"legend\":\"SPHV\",\"color\":\"#ffffff\",\"start_price\":5000,\"end_price\":49999,\"is_type\":1,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"deleted\":null}','{\"id\":1,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT HIGH VALUE\",\"legend\":\"SPHV\",\"color\":\"#ffffff\",\"start_price\":5000,\"end_price\":49999,\"is_labeled\":1,\"is_type\":1,\"is_active\":1,\"created\":\"2024-06-23T07:59:49+00:00\",\"modified\":\"2024-07-01T03:17:06+00:00\",\"deleted\":null}','2024-07-01 03:17:06','2024-07-01 03:17:06',NULL),(2,2,'ACCOUNTS','EDIT','{\"is_labeled\":0,\"modified\":\"2024-06-29T05:33:59+00:00\",\"id\":3,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT LOW VALUE\",\"legend\":\"SPLV\",\"color\":\"#008000\",\"start_price\":1,\"end_price\":4999,\"is_type\":2,\"is_active\":1,\"created\":\"2024-06-29T05:33:59+00:00\",\"deleted\":null}','{\"id\":3,\"user_id\":2,\"account\":\"SEMI-EXPENDABLE EQUIPMENT LOW VALUE\",\"legend\":\"SPLV\",\"color\":\"#008000\",\"start_price\":1,\"end_price\":4999,\"is_labeled\":1,\"is_type\":2,\"is_active\":1,\"created\":\"2024-06-29T05:33:59+00:00\",\"modified\":\"2024-07-01T03:17:12+00:00\",\"deleted\":null}','2024-07-01 03:17:12','2024-07-01 03:17:12',NULL);
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `activities` with 2 row(s)
--

--
-- Table structure for table `brands`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `brands to users` (`user_id`) USING BTREE,
  CONSTRAINT `brands to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `brands` VALUES (1,1,'Acer',1,'2024-06-23 06:01:06','2024-06-23 06:01:18',NULL);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `brands` with 1 row(s)
--

--
-- Table structure for table `categories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `categories to users` (`user_id`) USING BTREE,
  CONSTRAINT `categories to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `categories` VALUES (1,1,'Laptop',1,'2024-06-23 06:08:27','2024-06-23 06:12:53',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `categories` with 1 row(s)
--

--
-- Table structure for table `custodians`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custodians` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `personel_id` bigint unsigned NOT NULL,
  `office_id` bigint unsigned NOT NULL,
  `fund_cluster_id` bigint unsigned NOT NULL,
  `institution_id` bigint unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `no` double NOT NULL,
  `year` double NOT NULL,
  `month` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `custodians to users` (`user_id`) USING BTREE,
  KEY `custodians to personels` (`personel_id`) USING BTREE,
  KEY `custodians to offices` (`office_id`) USING BTREE,
  KEY `custodians to fund_clusters` (`fund_cluster_id`),
  KEY `custodians to institutuins` (`institution_id`),
  CONSTRAINT `custodians to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`),
  CONSTRAINT `custodians to institutuins` FOREIGN KEY (`institution_id`) REFERENCES `institutions` (`id`),
  CONSTRAINT `custodians to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`),
  CONSTRAINT `custodians to personels` FOREIGN KEY (`personel_id`) REFERENCES `personels` (`id`),
  CONSTRAINT `custodians to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custodians`
--

LOCK TABLES `custodians` WRITE;
/*!40000 ALTER TABLE `custodians` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `custodians` VALUES (1,2,2,1,1,1,'2024-07-0001',1,2024,7,26000,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(2,2,2,1,1,1,'2024-07-0002',2,2024,7,48999,'2024-07-01 03:01:04','2024-07-01 03:01:04',NULL);
/*!40000 ALTER TABLE `custodians` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `custodians` with 2 row(s)
--

--
-- Table structure for table `databases`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databases` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `databases to users` (`user_id`) USING BTREE,
  CONSTRAINT `databases to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databases`
--

LOCK TABLES `databases` WRITE;
/*!40000 ALTER TABLE `databases` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `databases` VALUES (1,1,'database/2024_06_27_06_13_am.sql','2024-06-27 06:13:36','2024-06-27 06:13:36',NULL),(2,1,'database/2024_06_27_07_16_am.sql','2024-06-27 07:16:28','2024-06-27 07:16:28',NULL),(3,2,'database/2024_06_28_06_05_am.sql','2024-06-28 06:05:34','2024-06-28 06:05:34',NULL),(4,2,'database/2024_07_01_01_57_am.sql','2024-07-01 01:57:07','2024-07-01 01:57:07',NULL),(5,2,'database/2024_07_01_09_58_am.sql','2024-07-01 01:58:25','2024-07-01 01:58:25',NULL);
/*!40000 ALTER TABLE `databases` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `databases` with 5 row(s)
--

--
-- Table structure for table `exports`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exports` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `export` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `exports to users` (`user_id`) USING BTREE,
  CONSTRAINT `exports to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exports`
--

LOCK TABLES `exports` WRITE;
/*!40000 ALTER TABLE `exports` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `exports` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `exports` with 0 row(s)
--

--
-- Table structure for table `fund_clusters`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_clusters` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `fund_cluster` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fund_clusters to users` (`user_id`) USING BTREE,
  CONSTRAINT `fund_clusters to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund_clusters`
--

LOCK TABLES `fund_clusters` WRITE;
/*!40000 ALTER TABLE `fund_clusters` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `fund_clusters` VALUES (1,2,'Cental Office',1,'2024-07-01 01:37:57','2024-07-01 01:37:57',NULL);
/*!40000 ALTER TABLE `fund_clusters` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `fund_clusters` with 1 row(s)
--

--
-- Table structure for table `headings`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `headings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `heading` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `total` double NOT NULL,
  `order_position` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `headings to users` (`user_id`),
  CONSTRAINT `headings to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `headings`
--

LOCK TABLES `headings` WRITE;
/*!40000 ALTER TABLE `headings` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `headings` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `headings` with 0 row(s)
--

--
-- Table structure for table `institutions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institutions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `institution` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_default` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `users to institutions` (`user_id`) USING BTREE,
  CONSTRAINT `users to institutions` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institutions`
--

LOCK TABLES `institutions` WRITE;
/*!40000 ALTER TABLE `institutions` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `institutions` VALUES (1,2,'Sdo',1,1,'2024-07-01 01:38:07','2024-07-01 01:38:07',NULL);
/*!40000 ALTER TABLE `institutions` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `institutions` with 1 row(s)
--

--
-- Table structure for table `items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `custodian_id` bigint unsigned NOT NULL,
  `logical_id` bigint unsigned NOT NULL,
  `slip_id` bigint unsigned NOT NULL,
  `supply_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `ledger_id` bigint unsigned NOT NULL,
  `office_id` bigint unsigned NOT NULL,
  `no` double NOT NULL,
  `year` double NOT NULL,
  `month` double NOT NULL,
  `quantity` double NOT NULL,
  `price` double NOT NULL,
  `total` double NOT NULL,
  `property_number` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `serial_number` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `inventory_number` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lifespan` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `remarks` text COLLATE utf8mb4_general_ci NOT NULL,
  `is_returned` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `items to slips` (`slip_id`) USING BTREE,
  KEY `items to supplies` (`supply_id`) USING BTREE,
  KEY `items to accounts` (`account_id`) USING BTREE,
  KEY `items to offices` (`office_id`) USING BTREE,
  KEY `items to custodians` (`custodian_id`) USING BTREE,
  KEY `items to ledgers` (`ledger_id`) USING BTREE,
  CONSTRAINT `items to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `items to custodians` FOREIGN KEY (`custodian_id`) REFERENCES `custodians` (`id`),
  CONSTRAINT `items to ledgers` FOREIGN KEY (`ledger_id`) REFERENCES `ledgers` (`id`),
  CONSTRAINT `items to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`),
  CONSTRAINT `items to slips` FOREIGN KEY (`slip_id`) REFERENCES `slips` (`id`),
  CONSTRAINT `items to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `items` VALUES (1,1,1,1,2,3,28,1,1,2024,7,1,520,520,'2024-05-06-0001-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(2,1,1,1,2,3,28,1,2,2024,7,1,520,520,'2024-05-06-0002-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(3,1,1,1,2,3,28,1,3,2024,7,1,520,520,'2024-05-06-0003-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(4,1,1,1,2,3,28,1,4,2024,7,1,520,520,'2024-05-06-0004-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(5,1,1,1,2,3,28,1,5,2024,7,1,520,520,'2024-05-06-0005-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(6,1,1,1,2,3,28,1,6,2024,7,1,520,520,'2024-05-06-0006-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(7,1,1,1,2,3,28,1,7,2024,7,1,520,520,'2024-05-06-0007-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(8,1,1,1,2,3,28,1,8,2024,7,1,520,520,'2024-05-06-0008-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(9,1,1,1,2,3,28,1,9,2024,7,1,520,520,'2024-05-06-0009-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(10,1,1,1,2,3,28,1,10,2024,7,1,520,520,'2024-05-06-0010-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(11,1,1,1,2,3,28,1,11,2024,7,1,520,520,'2024-05-06-0011-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(12,1,1,1,2,3,28,1,12,2024,7,1,520,520,'2024-05-06-0012-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(13,1,1,1,2,3,28,1,13,2024,7,1,520,520,'2024-05-06-0013-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(14,1,1,1,2,3,28,1,14,2024,7,1,520,520,'2024-05-06-0014-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(15,1,1,1,2,3,28,1,15,2024,7,1,520,520,'2024-05-06-0015-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(16,1,1,1,2,3,28,1,16,2024,7,1,520,520,'2024-05-06-0016-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(17,1,1,1,2,3,28,1,17,2024,7,1,520,520,'2024-05-06-0017-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(18,1,1,1,2,3,28,1,18,2024,7,1,520,520,'2024-05-06-0018-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(19,1,1,1,2,3,28,1,19,2024,7,1,520,520,'2024-05-06-0019-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(20,1,1,1,2,3,28,1,20,2024,7,1,520,520,'2024-05-06-0020-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(21,1,1,1,2,3,28,1,21,2024,7,1,520,520,'2024-05-06-0021-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(22,1,1,1,2,3,28,1,22,2024,7,1,520,520,'2024-05-06-0022-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(23,1,1,1,2,3,28,1,23,2024,7,1,520,520,'2024-05-06-0023-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(24,1,1,1,2,3,28,1,24,2024,7,1,520,520,'2024-05-06-0024-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(25,1,1,1,2,3,28,1,25,2024,7,1,520,520,'2024-05-06-0025-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(26,1,1,1,2,3,28,1,26,2024,7,1,520,520,'2024-05-06-0026-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(27,1,1,1,2,3,28,1,27,2024,7,1,520,520,'2024-05-06-0027-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(28,1,1,1,2,3,28,1,28,2024,7,1,520,520,'2024-05-06-0028-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(29,1,1,1,2,3,28,1,29,2024,7,1,520,520,'2024-05-06-0029-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(30,1,1,1,2,3,28,1,30,2024,7,1,520,520,'2024-05-06-0030-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(31,1,1,1,2,3,28,1,31,2024,7,1,520,520,'2024-05-06-0031-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(32,1,1,1,2,3,28,1,32,2024,7,1,520,520,'2024-05-06-0032-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(33,1,1,1,2,3,28,1,33,2024,7,1,520,520,'2024-05-06-0033-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(34,1,1,1,2,3,28,1,34,2024,7,1,520,520,'2024-05-06-0034-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(35,1,1,1,2,3,28,1,35,2024,7,1,520,520,'2024-05-06-0035-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(36,1,1,1,2,3,28,1,36,2024,7,1,520,520,'2024-05-06-0036-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(37,1,1,1,2,3,28,1,37,2024,7,1,520,520,'2024-05-06-0037-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:06','2024-07-01 02:59:06',NULL),(38,1,1,1,2,3,28,1,38,2024,7,1,520,520,'2024-05-06-0038-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(39,1,1,1,2,3,28,1,39,2024,7,1,520,520,'2024-05-06-0039-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(40,1,1,1,2,3,28,1,40,2024,7,1,520,520,'2024-05-06-0040-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(41,1,1,1,2,3,28,1,41,2024,7,1,520,520,'2024-05-06-0041-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(42,1,1,1,2,3,28,1,42,2024,7,1,520,520,'2024-05-06-0042-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(43,1,1,1,2,3,28,1,43,2024,7,1,520,520,'2024-05-06-0043-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(44,1,1,1,2,3,28,1,44,2024,7,1,520,520,'2024-05-06-0044-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(45,1,1,1,2,3,28,1,45,2024,7,1,520,520,'2024-05-06-0045-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(46,1,1,1,2,3,28,1,46,2024,7,1,520,520,'2024-05-06-0046-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(47,1,1,1,2,3,28,1,47,2024,7,1,520,520,'2024-05-06-0047-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(48,1,1,1,2,3,28,1,48,2024,7,1,520,520,'2024-05-06-0048-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(49,1,1,1,2,3,28,1,49,2024,7,1,520,520,'2024-05-06-0049-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(50,1,1,1,2,3,28,1,50,2024,7,1,520,520,'2024-05-06-0050-01','a','aa','7 YEARS','bond paper',0,'2024-07-01 02:59:07','2024-07-01 02:59:07',NULL),(51,2,2,2,1,1,3,1,51,2024,7,1,48999,48999,'2024-05-03-0051-01','asda','asdasd','3 YEARS','-',0,'2024-07-01 03:01:05','2024-07-01 03:01:05',NULL);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `items` with 51 row(s)
--

--
-- Table structure for table `ledgers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ledgers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `ledger` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `legend` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `sub_code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ledgers to users` (`user_id`) USING BTREE,
  KEY `ledgers to accounts` (`account_id`) USING BTREE,
  CONSTRAINT `ledgers to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `ledgers to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ledgers`
--

LOCK TABLES `ledgers` WRITE;
/*!40000 ALTER TABLE `ledgers` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `ledgers` VALUES (1,1,1,'SEMI-EXPENDABLE MACHINERY','SEMI-EXPENDABLE MACHINERY','05','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(2,1,1,'SEMI-EXPENDABLE OFFICE EQUIPMENT','SEMI-EXPENDABLE OFFICE EQUIPMENT','05','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(3,1,1,'SEMI-EXPENDABLE INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','SEMI-EXPENDABLE INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','05','03',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(4,1,1,'SEMI-EXPENDABLE COMMUNICATION EQUIPMENT','SEMI-EXPENDABLE COMMUNICATION EQUIPMENT','05','07',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(5,1,1,'SEMI-EXPENDABLE DISASTER RESPONSE AND RESCUE EQUIPMENT','SEMI-EXPENDABLE DISASTER RESPONSE AND RESCUE EQUIPMENT','05','08',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(6,1,1,'SEMI-EXPENDABLE MEDICAL EQUIPMENT','SEMI-EXPENDABLE MEDICAL EQUIPMENT','05','10',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(7,1,1,'SEMI-EXPENDABLE PRINTING EQUIPMENT','SEMI-EXPENDABLE PRINTING EQUIPMENT','05','11',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(8,1,1,'SEMI-EXPENDABLE SPORTS EQUIPMENT','SEMI-EXPENDABLE SPORTS EQUIPMENT','05','12',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(9,1,1,'SEMI-EXPENDABLE TECHNICAL AND SCIENTIFIC EQUIPMENT','SEMI-EXPENDABLE TECHNICAL AND SCIENTIFIC EQUIPMENT','05','13',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(10,1,1,'SEMI-EXPENDABLE GAMING EQUIPMENT','SEMI-EXPENDABLE GAMING EQUIPMENT','05','15',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(11,1,1,'SEMI-EXPENDABLE OTHER MACHINERY AND EQUIPMENT','SEMI-EXPENDABLE OTHER MACHINERY AND EQUIPMENT','05','19',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(12,1,1,'SEMI-EXPENDABLE FURNITURES AND FIXTURES','SEMI-EXPENDABLE FURNITURES AND FIXTURES','06','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(13,1,1,'SEMI-EXPENDABLE BOOKS','SEMI-EXPENDABLE BOOKS','06','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(14,1,2,'LAND','LAND','01','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(15,1,2,'BUILDINGS','BUILDINGS','04','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(16,1,2,'SCHOOL BUILDINGS','SCHOOL BUILDINGS','04','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(17,1,2,'MACHINERY','MACHINERY','05','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(18,1,2,'OFFICE EQUIPMENT','OFFICE EQUIPMENT','05','02',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(19,1,2,'INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','INFORMATION AND COMMUNICATION TECHNOLOGY EQUIPMENT','05','03',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(20,1,2,'COMMUNICATION EQUIPMENT','COMMUNICATION EQUIPMENT','05','07',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(21,1,2,'DISASTER RESPONSE AND RESCUE EQUIPMENT','DISASTER RESPONSE AND RESCUE EQUIPMENT','05','08',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(22,1,2,'MEDICAL EQUIPMENT','MEDICAL EQUIPMENT','05','10',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(23,1,2,'PRINTING EQUIPMENT','PRINTING EQUIPMENT','05','11',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(24,1,2,'SPORTS EQUIPMENT','SPORTS EQUIPMENT','05','12',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(25,1,2,'OTHER MACHINERY AND EQUIPMENT','OTHER MACHINERY AND EQUIPMENT','05','19',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(26,1,2,'MOTOR VEHICLES','MOTOR VEHICLES','06','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(27,1,2,'FURNITURE AND FIXTURES','FURNITURE AND FIXTURES','07','01',1,'2024-06-23 08:01:00','2024-06-23 08:01:00',NULL),(28,2,3,'SPLV','SPLV','05','06',1,'2024-07-01 02:11:44','2024-07-01 02:11:44',NULL);
/*!40000 ALTER TABLE `ledgers` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `ledgers` with 28 row(s)
--

--
-- Table structure for table `logbooks`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logbooks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `supply_id` bigint unsigned NOT NULL,
  `logbook` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `quantity` double NOT NULL,
  `old_stock` double NOT NULL,
  `total_stock` double NOT NULL,
  `is_stored` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logbooks to users` (`user_id`),
  KEY `logbooks to supplies` (`supply_id`),
  CONSTRAINT `logbooks to supplies` FOREIGN KEY (`supply_id`) REFERENCES `supplies` (`id`),
  CONSTRAINT `logbooks to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logbooks`
--

LOCK TABLES `logbooks` WRITE;
/*!40000 ALTER TABLE `logbooks` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `logbooks` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `logbooks` with 0 row(s)
--

--
-- Table structure for table `logos`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `logo` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `width` double NOT NULL,
  `height` double NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `logos to users` (`user_id`) USING BTREE,
  CONSTRAINT `logos to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logos`
--

LOCK TABLES `logos` WRITE;
/*!40000 ALTER TABLE `logos` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `logos` VALUES (3,2,'SDO-SC','logo-img/667f9b3d31a47.jpeg',4096,4096,1,'2024-06-28 08:43:51','2024-06-29 05:27:25',NULL);
/*!40000 ALTER TABLE `logos` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `logos` with 1 row(s)
--

--
-- Table structure for table `offices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `office` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `legend` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `offices to users` (`user_id`) USING BTREE,
  CONSTRAINT `offices to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `offices` VALUES (1,1,'ACCOUNTANT  1','ACCOUNTANT 1','01',1,'2024-06-23 05:48:10','2024-06-23 05:53:05',NULL);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `offices` with 1 row(s)
--

--
-- Table structure for table `personels`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personels` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `position_id` bigint unsigned NOT NULL,
  `office_id` bigint unsigned NOT NULL,
  `personel` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `officials to users` (`user_id`) USING BTREE,
  KEY `officials to positions` (`position_id`) USING BTREE,
  KEY `personels to offices` (`office_id`),
  CONSTRAINT `personels to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`),
  CONSTRAINT `personels to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `personels to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personels`
--

LOCK TABLES `personels` WRITE;
/*!40000 ALTER TABLE `personels` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `personels` VALUES (1,2,1,1,'ABIGAIL P. CIELO EDD',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(2,2,2,1,'ARCADIO L. MODUMO JR. LPT',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(3,2,3,1,'BENEDICT B. SANTIAGO, DPA',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(4,2,4,1,'CARLOS G. MATEO MAED',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(5,2,5,1,'CASSY CHARMAINE TAA LPT',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(6,2,6,1,'CATHERINE MADRID RN',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(7,2,7,1,'CHEERILYN SUBA',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(8,2,8,1,'CHERY ANN R. SEGUNDO CPA',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(9,2,6,1,'CHERYL B. DE LUNA RN',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(10,2,9,1,'CHRISTIAN C. ECHIJA LPT',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(11,2,10,1,'CHRIZLE D. PARAGAS',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(12,2,11,1,'CLARISSA F. SOLIVEN MAED',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(13,2,12,1,'DAISYLENE AMOR GALAPIA',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(14,2,10,1,'DIOSSA C. MANGULABNAN LPT',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(15,2,13,1,'DJSON B. JULIO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(16,2,14,1,'ELIZABETH R. BERDADERO PHD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(17,2,15,1,'EMIL RESPONSO RRT, LPT, MPH',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(18,2,9,1,'EMMANUEL THADDEUS B. PASCUAL MBA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(19,2,16,1,'ERIK A. PALOMARES MPA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(20,2,17,1,'ESTELITO C. BALATAN JR. PHD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(21,2,10,1,'FELICISIMO C. SIQUIAN III',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(22,2,18,1,'FLORDELIZA C. GECOBE PHD, CESO V',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(23,2,5,1,'FREDDIE D. BACALI',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(24,2,19,1,'GIDEON OMAR BUTAC MBA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(25,2,20,1,'GLENDA B. AZURIN',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(26,2,13,1,'GRETA S. CALLANG LPT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(27,2,10,1,'HEARTZIEL JOY M. BAUTISTA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(28,2,10,1,'HYLENE MEIGH RAMOS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(29,2,13,1,'IMEE MARIE M. PILLIEN, LPT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(30,2,21,1,'JACQUELINE S. RAMOS PHD CESE',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(31,2,22,1,'JAKE ANTHONY F. SAMBRANO LPT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(32,2,23,1,'JAMES PAUL R. SANTIAGO MBA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(33,2,9,1,'JANET MINA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(34,2,24,1,'JANETTE V. BAUTISTA, EDD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(35,2,15,1,'JAY LORD GALLARDE',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(36,2,25,1,'JENELYN B. BUTAC RL MSLS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(37,2,26,1,'JENNA MAE DULDULAO LPT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(38,2,27,1,'JOFFREY T. TAGUBA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(39,2,19,1,'JONELL C. MANGULABNAN JD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(40,2,27,1,'JUANITO A. SANCHEZ, JR.',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(41,2,23,1,'JULIE ANN T. GALLIEN',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(42,2,28,1,'LEAH R. EDROZO DMD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(43,2,29,1,'LEILANIE P. DOMINIA PHD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(44,2,16,1,'LEONIDA F. CULANG MPA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(45,2,23,1,'MA. ANGELA L. PARAGAS LPT, MAED',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(46,2,30,1,'MARIVEL G. MORALES',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(47,2,13,1,'MARJORIE SISON',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(48,2,31,1,'MARK VINCENT FERNANDEZ LPT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(49,2,32,1,'MARVIELYN JOY P. BARAO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(50,2,13,1,'MARVIN GALLIEN LPT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(51,2,5,1,'MARVIN L. PARAGAS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(52,2,13,1,'MARY ANN V. BONGAT MBA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(53,2,23,1,'MELANIE O. SO MBA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(54,2,33,1,'MENEO M. ARCILLA LLB',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(55,2,34,1,'MERCY M. ANTIMANO MAED',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(56,2,35,1,'PERFECTA M. BAUTISTA, PHD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(57,2,36,1,'RHEANNE SARAOS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(58,2,27,1,'RIC EMMANUEL G. REMIGIO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(59,2,37,1,'RICARDO Q. CABUGAO JR. MIT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(60,2,38,1,'ROBERT T. RUSTIA MAED',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(61,2,9,1,'RONADES KEIGH R. SABIO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(62,2,24,1,'ROSALIA B. GUTIERREZ EDD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(63,2,39,1,'RYDIANT JOY BLESSING MANUEL RN, MD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(64,2,13,1,'SHEILA Q. CAMBA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(65,2,13,1,'SHERWIN O. SO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(66,2,40,1,'SHIRLYN R. MACASPAC LPT, PHD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(67,2,13,1,'VIMSON C. MIJARES',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(68,2,9,1,'WILLIAM B. CARDENAS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(69,2,41,1,'DARIUS C. JULIO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(70,2,41,1,'MARITESS L. QUIPSE',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(71,2,42,1,'LIEZL S. FRANCISCO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(72,2,42,1,'BEMMA JANE L. MARIANO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(73,2,42,1,'CARIDAD V. PASTORES',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(74,2,43,1,'MILAGROS ISLA',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(75,2,43,1,'ROWEL P. LOPEZ ',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(76,2,43,1,'ERLITA G. SADDUL',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(77,2,44,1,'NAIZA DUPINGAY',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(78,2,44,1,'JHONELL ALEJANDRO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL);
/*!40000 ALTER TABLE `personels` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `personels` with 78 row(s)
--

--
-- Table structure for table `positions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `positions to users` (`user_id`) USING BTREE,
  CONSTRAINT `positions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `positions` VALUES (1,2,'SENIOR EDUCATION PROGRAM SPECIALIST-HRD',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(2,2,'EDUCATION PROGRAM SPECIALIS II - SMM&E',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(3,2,'EDUCATION PROGRAM SUPERVISOR-MAPEH',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(4,2,'EDUCATION PROGRAM SUPERVISOR-TLE',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(5,2,'ADMINISTRATIVE AIDE IV',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(6,2,'NURSE II',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(7,2,'ADMINISTRATIVE ASSISTANT III / SSCS',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(8,2,'ACCOUNTANT III',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(9,2,'PROJECT DEVELOPMENT OFFICER I',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(10,2,'ADMINISTRATIVE AIDE VI',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(11,2,'EDUCATION PROGRAM SUPERVISOR-SCIENCE',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(12,2,'ADMINISTRATIVE ASSISTANT II ',1,'2024-06-29 05:22:06','2024-06-29 05:22:06',NULL),(13,2,'ADMINISTRATIVE ASSISTANT III',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(14,2,'EDUCATION PROGRAM SUPERVISOR-FILIPINO',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(15,2,'PROJECT DEVELOPMENT OFFICER II',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(16,2,'ADMINISTRATIVE OFFICER V',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(17,2,'EDUCATION PROGRAM SUPERVISOR-VALUES',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(18,2,'SCHOOLS DIVISION SUPERINTENDENT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(19,2,'ADMINISTRATIVE OFFICER II',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(20,2,'EDUCATION PROGRAM SPECIALIST-ALS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(21,2,'ASSISTANT SCHOOLS DIVISION SUPERINTENDENT',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(22,2,'TEACHER I / PLANNING STAFF / SSCS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(23,2,'ADMINISTRATIVE OFFICER IV',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(24,2,'CHIEF EDUCATION SUPERVISOR',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(25,2,'LIBRARIAN II',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(26,2,'ADMINISTRATIVE ASSISTANT II / DIVISORIA HS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(27,2,'ADMINISTRATIVE AIDE I',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(28,2,'DENTIST II',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(29,2,'EDUCATION PROGRAM SUPERVISOR-SGOD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(30,2,'EDUCATION PROGRAM SUPERVISOR-LRMDS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(31,2,'TEACHER III / RESEARCH FOCAL / RIZAL NHS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(32,2,'PROJECT ENGINEER',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(33,2,'PLANNING OFFICER III',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(34,2,'EDUCATION PROGRAM SUPERVISOR-KINDER',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(35,2,'EDUCATION PROGRAM SUPERVISOR-ENGLISH',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(36,2,'ADMINISTRATIVE ASSISTANT II / SSCS',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(37,2,'INFORMATION TECHNOLOGY OFFICER I',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(38,2,'EDUCATION PROGRAM SUPERVISOR-AP',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(39,2,'MEDICAL OFFICER III',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(40,2,'SENIOR EDUCATION PROGRAM SPECIALIST-SMM&E',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(41,2,'JO CLERK',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(42,2,'OFFICE CLERK - LGU',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(43,2,'UTILITY WORKER - LGU',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL),(44,2,'SECURITY GUARD',1,'2024-06-29 05:22:07','2024-06-29 05:22:07',NULL);
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `positions` with 44 row(s)
--

--
-- Table structure for table `remember_me_phinxlog`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remember_me_phinxlog` (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remember_me_phinxlog`
--

LOCK TABLES `remember_me_phinxlog` WRITE;
/*!40000 ALTER TABLE `remember_me_phinxlog` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `remember_me_phinxlog` VALUES (20170907030013,'CreateRememberMeTokens','2024-06-22 19:02:21','2024-06-22 19:02:21',0);
/*!40000 ALTER TABLE `remember_me_phinxlog` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `remember_me_phinxlog` with 1 row(s)
--

--
-- Table structure for table `remember_me_tokens`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remember_me_tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `U_token_identifier` (`model`,`foreign_id`,`series`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remember_me_tokens`
--

LOCK TABLES `remember_me_tokens` WRITE;
/*!40000 ALTER TABLE `remember_me_tokens` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `remember_me_tokens` VALUES (1,'2024-06-23 05:35:58','2024-06-23 05:35:58','Users','1','2a871623fb8c83b9f4b5940e6dfac34dde0d3282','7ae762dda0a46b1cae6ce4463bd1c60a4f58bc05','2024-07-23 05:35:58'),(2,'2024-06-24 01:22:08','2024-06-25 04:31:47','Users','1','a8c67bab260ab8d9a6732e2733d47ca307928b05','e4d588e04c6069cd8f52f61734ff067dc6c9afdf','2024-07-25 04:31:47'),(5,'2024-06-26 01:26:30','2024-06-26 07:20:44','Users','1','dbe5f788c9c89f57e1cd1c1d8ae09bc6f3ce7222','365f99ad8e86dd64bdb2cdbff54328e20ae5c489','2024-07-26 07:20:44'),(7,'2024-06-27 07:39:21','2024-06-28 08:28:05','Users','2','a159d9c5d30e956748f1b72acea06b08d6960154','8eaa51e5f6b363a8bb8193312d49b81ce7db1bbd','2024-07-28 08:28:05'),(10,'2024-06-29 03:56:30','2024-06-29 04:23:40','Users','2','32db44b6051ec6922677874f0cd288f798ef5971','74b9daf21983e281382d799c7c61ff35ede0162b','2024-07-29 04:23:40'),(11,'2024-06-29 05:26:30','2024-06-29 05:26:30','Users','2','d1468e6a9c63fdd6936e85b4fc7d8093aa71f1d2','50aee779a15a868176f55693336789e075a88fd4','2024-07-29 05:26:29');
/*!40000 ALTER TABLE `remember_me_tokens` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `remember_me_tokens` with 6 row(s)
--

--
-- Table structure for table `signatories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signatories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `position_id` bigint unsigned NOT NULL,
  `heading_id` bigint unsigned NOT NULL,
  `signatory` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `order_position` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `signatories to users` (`user_id`),
  KEY `signatories to headings` (`heading_id`),
  KEY `signatories to positions` (`position_id`),
  CONSTRAINT `signatories to headings` FOREIGN KEY (`heading_id`) REFERENCES `headings` (`id`),
  CONSTRAINT `signatories to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`),
  CONSTRAINT `signatories to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signatories`
--

LOCK TABLES `signatories` WRITE;
/*!40000 ALTER TABLE `signatories` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `signatories` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `signatories` with 0 row(s)
--

--
-- Table structure for table `slips`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slips` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `custodian_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `no` double NOT NULL,
  `year` double NOT NULL,
  `month` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `slips to accounts` (`account_id`) USING BTREE,
  KEY `slips to custodians` (`custodian_id`) USING BTREE,
  CONSTRAINT `slips to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `slips to custodians` FOREIGN KEY (`custodian_id`) REFERENCES `custodians` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slips`
--

LOCK TABLES `slips` WRITE;
/*!40000 ALTER TABLE `slips` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `slips` VALUES (1,1,3,'SPLV-2024-07-0001',1,2024,7,26000,'2024-07-01 02:59:05','2024-07-01 02:59:05',NULL),(2,2,1,'SPHV-2024-07-0001',1,2024,7,48999,'2024-07-01 03:01:05','2024-07-01 03:01:05',NULL);
/*!40000 ALTER TABLE `slips` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `slips` with 2 row(s)
--

--
-- Table structure for table `supplies`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `supply` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `price` double NOT NULL,
  `stocks` double NOT NULL,
  `issued` double NOT NULL,
  `brand_id` bigint unsigned NOT NULL,
  `category_id` bigint unsigned NOT NULL,
  `unit_id` bigint unsigned NOT NULL,
  `account_id` bigint unsigned NOT NULL,
  `ledger_id` bigint unsigned NOT NULL,
  `lifespan` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `is_expired` tinyint NOT NULL DEFAULT '0',
  `expired_at` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `supplies to brands` (`brand_id`) USING BTREE,
  KEY `supplies to categories` (`category_id`) USING BTREE,
  KEY `supplies to units` (`unit_id`) USING BTREE,
  KEY `supplies to users` (`user_id`) USING BTREE,
  KEY `supplies to accounts` (`account_id`) USING BTREE,
  KEY `supplies to ledgers` (`ledger_id`) USING BTREE,
  CONSTRAINT `supplies to accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `supplies to brands` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `supplies to categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `supplies to ledgers` FOREIGN KEY (`ledger_id`) REFERENCES `ledgers` (`id`),
  CONSTRAINT `supplies to units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
  CONSTRAINT `supplies to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplies`
--

LOCK TABLES `supplies` WRITE;
/*!40000 ALTER TABLE `supplies` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `supplies` VALUES (1,2,'ACER SPIN 5',48999,0,1,1,1,1,1,3,'3 YEARS',1,0,NULL,'2024-06-29 05:28:18','2024-07-01 03:01:05',NULL),(2,2,'BOND PAPER',520,0,100,1,1,1,3,28,'7 YEARS',1,0,NULL,'2024-07-01 02:12:07','2024-07-01 02:59:07',NULL);
/*!40000 ALTER TABLE `supplies` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `supplies` with 2 row(s)
--

--
-- Table structure for table `units`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `units` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `units to users` (`user_id`) USING BTREE,
  CONSTRAINT `units to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `units` VALUES (1,1,'Units',1,'2024-06-23 06:22:19','2024-06-23 06:23:19',NULL);
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `units` with 1 row(s)
--

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `position_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `is_admin` tinyint NOT NULL DEFAULT '0',
  `is_active` tinyint NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `users to positions` (`position_id`),
  CONSTRAINT `users to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `users` VALUES (1,1,'RODRIGO','rods','cabotaje.rodrigo@gmail.com','$2y$10$ljQoVM5RlGJFFKwButTAruLFffRADz4uBcIK/whAm4OT6EUgBj6oW','667cf58ca53f1667cf58ca53f3',1,1,'2024-06-23 05:25:58','2024-06-27 05:15:56',NULL),(2,1,'ADMIN','admin','admin@gmail.com','$2y$10$CmLG/zxjkmjqa6Xe0PiZKuvLKsLvWHkAGT1WgRA9mxV3D9O.ZvgCW','667f9b06c8132667f9b06c8136',1,1,'2024-06-27 04:58:41','2024-06-29 05:26:30',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `users` with 2 row(s)
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET AUTOCOMMIT=@OLD_AUTOCOMMIT */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on: Mon, 01 Jul 2024 03:17:24 +0000
