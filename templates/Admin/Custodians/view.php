<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Custodian $custodian
 */
?>


<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'index'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Supply">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($custodian,['class' => 'card card-primary rounded-0', 'type' => 'file', 'id' => 'form']);?>
        <div class="card-header">
            <h3 class="card-title">Custodian Form</h3>
        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-sm-12 col-md-7 col-lg-8 my-2">
                    <?=$this->Form->label('personel_id', ucwords('personel'))?>
                    <?=$this->Form->select('personel_id', $personels,[
                        'class' => 'form-control rounded-0',
                        'id' => 'personel-id',
                        'required' => true,
                        'disabled' => true,
                        'empty' => ucwords('personel'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-5 col-lg-4 my-2">
                    <?=$this->Form->label('office_id', ucwords('office'))?>
                    <?=$this->Form->select('office_id', $offices,[
                        'class' => 'form-control rounded-0',
                        'id' => 'office-id',
                        'required' => true,
                        'disabled' => true,
                        'empty' => ucwords('office'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                    <?=$this->Form->label('fund_cluster_id', ucwords('fund cluster'))?>
                    <?=$this->Form->select('fund_cluster_id', $fundClusters,[
                        'class' => 'form-control rounded-0',
                        'id' => 'fund-cluster-id',
                        'required' => true,
                        'empty' => ucwords('fund cluster'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                    <?=$this->Form->label('institution_id', ucwords('institution'))?>
                    <?=$this->Form->select('institution_id', $institutions,[
                        'class' => 'form-control rounded-0',
                        'id' => 'institution-id',
                        'required' => true,
                        'empty' => ucwords('institution'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <ul class="nav nav-tabs" id="custom-slip-content-tab" role="tablist">
                        <?php foreach ($custodian->toArray()['slips'] as $key => $slip):?>
                            <?php $active = (intval($key) == intval(0))? 'active': null;?>
                            <li class="nav-item">
                                <a class="nav-link <?=($active)?>" id="slip-content-<?=intval($slip['id'])?>-tab-<?=intval($slip['id'])?>" data-toggle="pill" href="#slip-content-<?=intval($slip['id'])?>" role="tab" aria-controls="slip-content-<?=intval($slip['id'])?>" aria-selected="true">
                                    <?=strtoupper($slip['account']['legend'])?>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                    <div class="tab-content" id="custom-slip-content-<?=intval($slip['id'])?>-tabContent">
                        <?php foreach ($custodian->slips as $key => $slip):?>
                            <?php $active = (intval($key) == intval(0))? 'active': null;?>
                            <?php $show = (intval($key) == intval(0))? 'show': null;?>
                            <div class="tab-pane fade <?=($active.' '.$show)?>" id="slip-content-<?=intval($slip['id'])?>" role="tabpanel" aria-labelledby="slip-content-<?=intval($slip['id'])?>-tab-<?=intval($slip['id'])?>">
                                <div class="row p-2">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="table-responsive" style="height: 500px !important;">
                                            <table class="table table-head-fixed text-nowrap">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Property Number</th>
                                                    <th>Serial Number</th>
                                                    <th>Inventory Number</th>
                                                    <th>Supply</th>
                                                    <th>Ledger</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th>Total</th>
                                                    <th>Lifespan</th>
                                                    <th>Remarks</th>
                                                    <th><i class="fa fa-cogs"></i></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = intval(1);?>
                                                    <?php foreach ($slip['items'] as $k => $item):?>
                                                        <tr>
                                                            <td><?=$no++;?></td>
                                                            <td><?=$item['property_number']?></td>
                                                            <td>
                                                                <?=$this->Form->text('serial_number',[
                                                                    'class' => 'form-control form-control-border rounded-0 serial-number',
                                                                    'placeholder' => ucwords('serial number'),
                                                                    'title' => ucwords('Please Fill Out This Field'),
                                                                    'data-id' => intval($item['id']),
                                                                    'id' => 'item-serial-number-'.(intval($item['id'])),
                                                                    'value' => $item['serial_number'],
                                                                    'data-old-value' => $item['serial_number'],
                                                                    'style' => 'width: 250px !important;'
                                                                ])?>
                                                                <small></small>
                                                            </td>
                                                            <td>
                                                                <?=$this->Form->text('inventory_number',[
                                                                    'class' => 'form-control form-control-border rounded-0 inventory-number',
                                                                    'placeholder' => ucwords('inventory number'),
                                                                    'title' => ucwords('Please Fill Out This Field'),
                                                                    'data-id' => intval($item['id']),
                                                                    'id' => 'item-inventory-number-'.(intval($item['id'])),
                                                                    'value' => $item['inventory_number'],
                                                                    'data-old-value' => $item['inventory_number'],
                                                                    'style' => 'width: 250px !important;'
                                                                ])?>
                                                                <small></small>
                                                            </td>
                                                            <td><?=strtoupper($item['supply']['supply'])?></td>
                                                            <td><small><?=strtoupper($item['ledger']['ledger'])?></small></td>
                                                            <td><?=number_format($item['price'],2)?></td>
                                                            <td><?=number_format($item['quantity'],0)?></td>
                                                            <td><?=number_format($item['total'],2)?></td>
                                                            <td>
                                                                <?=$this->Form->text('lifespan',[
                                                                    'class' => 'form-control form-control-border rounded-0 lifespan',
                                                                    'placeholder' => ucwords('lifespan'),
                                                                    'title' => ucwords('Please Fill Out This Field'),
                                                                    'data-id' => intval($item['id']),
                                                                    'id' => 'item-lifespan-'.(intval($item['id'])),
                                                                    'value' => $item['lifespan'],
                                                                    'data-old-value' => $item['lifespan'],
                                                                    'style' => 'width: 250px !important;'
                                                                ])?>
                                                                <small></small>
                                                            </td>
                                                            <td>
                                                                <?=$this->Form->text('remarks',[
                                                                    'class' => 'form-control form-control-border rounded-0 remarks',
                                                                    'placeholder' => ucwords('remarks'),
                                                                    'title' => ucwords('Please Fill Out This Field'),
                                                                    'data-id' => intval($item['id']),
                                                                    'id' => 'item-remarks-'.(intval($item['id'])),
                                                                    'value' => $item['remarks'],
                                                                    'data-old-value' => $item['remarks'],
                                                                    'style' => 'width: 250px !important;'
                                                                ])?>
                                                                <small></small>
                                                            </td>
                                                            <th>
                                                                <button type="button" data-id="<?=intval($item['id'])?>" class="btn btn-sm btn-danger rounded-0 text-white pdf" title="PDF">PDF</button>
                                                                <?php if(!boolval($item['is_returned'])):?>
                                                                    | <a data-id="<?=intval($item['id'])?>" id="is-returned-<?=intval($item['id'])?>" class="btn btn-sm btn-success rounded-0 text-white returned" title="Returned">Returned</a>
                                                                <?php endif;?>
                                                            </th>
                                                        </tr>
                                                    <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>

                <div class="col-sm-12 col-md-5 col-lg-4 my-2">
                    <?=$this->Form->label('total', ucwords('total'))?>
                    <?=$this->Form->text('total',[
                        'class' => 'form-control form-control-border rounded-0',
                        'id' => 'total',
                        'required' => true,
                        'disabled' => true,
                        'placeholder' => ucwords('total'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                    ])?>
                    <small></small>
                </div>

            </div>
        </div>
        <?= $this->Form->end();?>
    </div>
</div>

<?=$this->Html->script('admin/custodians/view')?>
