<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Custodian $custodian
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $personels
 * @var \Cake\Collection\CollectionInterface|string[] $offices
 */
?>

<div class="modal fade" id="modal" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-wrap flex-row justify-content-center align-items-center mb-5">
                        <div id="scanner"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'index'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Supply">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($custodian,['class' => 'card card-primary rounded-0', 'type' => 'file', 'id' => 'form']);?>
        <div class="card-header">
            <h3 class="card-title">Custodian Form</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-7 col-lg-8 my-2">
                    <?=$this->Form->label('supply', ucwords('supply'))?>
                    <div class="input-group">
                        <?=$this->Form->text('supply',[
                            'class' => 'form-control rounded-0',
                            'id' => 'supply',
                            'required' => false,
                            'placeholder' => ucwords('supply'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                        <div class="input-group-append rounded-0" style="cursor: pointer !important;" id="scan">
                            <span class="input-group-text rounded-0 bg-primary">
                                <i class="fa fa-qrcode"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-5 col-lg-4 my-2">
                    <?=$this->Form->label('quantity', ucwords('quantity'))?>
                    <div class="input-group">
                        <?=$this->Form->number('quantity',[
                            'class' => 'form-control rounded-0',
                            'id' => 'quantity',
                            'required' => false,
                            'placeholder' => ucwords('quantity'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'min' => 1,
                            'value' => 0,
                            'disabled' => true,
                        ])?>
                        <small></small>
                        <div class="input-group-append rounded-0" style="cursor: pointer !important;" id="append-row">
                            <span class="input-group-text rounded-0 bg-primary">
                                <i class="fa fa-plus-square"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-7 col-lg-8 my-2">
                    <?=$this->Form->label('personel_id', ucwords('personel'))?>
                    <?=$this->Form->select('personel_id', $personels,[
                        'class' => 'form-control rounded-0',
                        'id' => 'personel-id',
                        'required' => true,
                        'empty' => ucwords('personel'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small id="select-personel-id"></small>
                </div>

                <div class="col-sm-12 col-md-5 col-lg-4 my-2">
                    <?=$this->Form->label('office_id', ucwords('office'))?>
                    <?=$this->Form->select('office_id', $offices,[
                        'class' => 'form-control rounded-0',
                        'id' => 'office-id',
                        'required' => true,
                        'empty' => ucwords('office'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                    <?=$this->Form->label('fund_cluster_id', ucwords('fund cluster'))?>
                    <?=$this->Form->select('fund_cluster_id', $fundClusters,[
                        'class' => 'form-control rounded-0',
                        'id' => 'fund-cluster-id',
                        'required' => true,
                        'empty' => ucwords('fund cluster'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                    <?=$this->Form->label('institution_id', ucwords('institution'))?>
                    <?=$this->Form->select('institution_id', $institutions,[
                        'class' => 'form-control rounded-0',
                        'id' => 'institution-id',
                        'required' => true,
                        'empty' => ucwords('institution'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <div class="table-responsive" style="height: 500px !important;">
                        <table class="table table-head-fixed text-nowrap">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Supply</th>
                                <th>Serial Number</th>
                                <th>Inventory Number</th>
                                <th>Price</th>
                                <th>Lifespan</th>
                                <th>Remarks</th>
                                <th><i class="fa fa-cogs"></i></th>
                            </tr>
                            </thead>
                            <tbody id="items-tbody">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-sm-12 col-md-5 col-lg-4 my-2">
                    <?=$this->Form->label('total', ucwords('total'))?>
                    <?=$this->Form->text('total',[
                        'class' => 'form-control form-control-border rounded-0',
                        'id' => 'total',
                        'required' => true,
                        'readonly' => true,
                        'placeholder' => ucwords('total'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                        'value' => 0
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-7 col-lg-8 my-2">
                    <?=$this->Form->label('password', ucwords('password'))?>
                    <?=$this->Form->password('password',[
                        'class' => 'form-control rounded-0',
                        'id' => 'password',
                        'required' => true,
                        'placeholder' => ucwords('password'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                    ])?>
                    <small></small>
                </div>

            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->hidden('user_id',[
                'id' => 'user-id',
                'required' => true,
                'readonly' => true,
                'value' => intval(@$auth['id'])
            ])?>
            <?=$this->Form->hidden('no',[
                'id' => 'no',
                'required' => true,
                'readonly' => true,
                'value' => intval($max + intval(1))
            ])?>
            <?=$this->Form->hidden('year',[
                'id' => 'year',
                'required' => true,
                'readonly' => true,
                'value' => intval(date('Y'))
            ])?>
            <?=$this->Form->hidden('month',[
                'id' => 'month',
                'required' => true,
                'readonly' => true,
                'value' => date('m')
            ])?>
            <?=$this->Form->hidden('code',[
                'id' => 'code',
                'required' => true,
                'readonly' => true,
                'value' => (date('Y')).'-'.(date('m')).'-'.(str_pad(intval(@$max + intval(1)), 4, '0000', STR_PAD_LEFT))
            ])?>
            <?=$this->Form->button(ucwords('Reset'),[
                'class' => 'btn btn-danger rounded-0 m-1',
                'type' => 'reset',
                'title' => ucwords('Reset')
            ])?>
            <?=$this->Form->button(ucwords('Submit'),[
                'class' => 'btn btn-success rounded-0 m-1',
                'type' => 'submit',
                'title' => ucwords('Submit')
            ])?>
        </div>
        <?= $this->Form->end();?>
    </div>
</div>

<?=$this->Html->script('admin/custodians/add')?>
