<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Heading $heading
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<script>
    var id = parseInt(<?=intval($heading->id)?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Headings', 'action' => 'index'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Supply">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 mb-3">
        <?= $this->Form->create($heading,['class' => 'card card-primary rounded-0', 'type' => 'file', 'id' => 'form']);?>
        <div class="card-header">
            <h3 class="card-title">Heading Form</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                    <?=$this->Form->label('heading', ucwords('heading'))?>
                    <?=$this->Form->text('heading',[
                        'class' => 'form-control rounded-0',
                        'id' => 'heading',
                        'required' => true,
                        'placeholder' => ucwords('heading'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                    <?=$this->Form->label('total', ucwords('total'))?>
                    <?=$this->Form->number('total',[
                        'class' => 'form-control rounded-0',
                        'id' => 'total',
                        'required' => true,
                        'placeholder' => ucwords('total'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                        'min' => 0,
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                    <div class="icheck-primary d-inline">
                        <?=$this->Form->checkbox('active',[
                            'id' => 'active',
                            'label' => false,
                            'hiddenField' => false,
                            'checked' => boolval($heading->is_active),
                        ])?>
                        <?=$this->Form->label('active', ucwords('Active'))?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->hidden('user_id',[
                'id' => 'user-id',
                'required' => true,
                'readonly' => true,
                'value' => intval(@$auth['id'])
            ])?>
            <?= $this->Form->hidden('is_active',[
                'id' => 'is-active',
                'value' => intval($heading->is_active)
            ]);?>
            <?= $this->Form->hidden('order_position',[
                'id' => 'order-position',
                'value' => intval($heading->order_position)
            ]);?>
            <?=$this->Form->button(ucwords('Reset'),[
                'class' => 'btn btn-danger rounded-0 m-1',
                'type' => 'reset',
                'title' => ucwords('Reset')
            ])?>
            <?=$this->Form->button(ucwords('Submit'),[
                'class' => 'btn btn-success rounded-0 m-1',
                'type' => 'submit',
                'title' => ucwords('Submit')
            ])?>
        </div>
        <?= $this->Form->end();?>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Position</th>
                        <th>Signatory</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/headings/view')?>
