<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Signatory> $signatories
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Position</th>
                        <th>Heading</th>
                        <th>Signatory</th>
                        <th>Description</th>
                        <th>Gap</th>
                        <th>Merge</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Deleted</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/signatories/bin')?>
