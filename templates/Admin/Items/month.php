<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Item> $items
 */
?>

<?=$this->Form->create(null,['type' => 'file', 'id' => 'form', 'class' => 'row'])?>
<div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'add'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Custodian">
        New Custodian
    </a>
</div>

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card p-3">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Property Number</th>
                    <th>Serial Number</th>
                    <th>Inventory Number</th>
                    <th>Supply</th>
                    <th>Personel</th>
                    <th>Office</th>
                    <th>Account</th>
                    <th>Ledger</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th>Is Returned</th>
                    <th>Process By</th>
                    <th>Modified</th>
                    <th>Options</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<?=$this->Form->end()?>

<?=$this->Html->script('admin/items/month')?>

