<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Item> $items
 */
?>

<?=$this->Form->create(null,['type' => 'file', 'id' => 'form', 'class' => 'row'])?>
<div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'add'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Custodian">
        New Custodian
    </a>
</div>

<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
    <?=$this->Form->label('start_date', ucwords('Start Date'))?>
    <?=$this->Form->date('start_date',[
        'class' => 'form-control rounded-0',
        'id' => 'start-date',
        'title' => ucwords('Please Fill Out This Field'),
        'required' => true,
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')
    ])?>
</div>

<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
    <?=$this->Form->label('end_date', ucwords('End Date'))?>
    <?=$this->Form->date('end_date',[
        'class' => 'form-control rounded-0',
        'id' => 'end-date',
        'title' => ucwords('Please Fill Out This Field'),
        'required' => true,
        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
    ])?>
</div>

<div class="col-sm-12 col-md-3 col-lg-2 mb-3">
    <?=$this->Form->label('limit', ucwords('limit'))?>
    <?=$this->Form->number('limit',[
        'class' => 'form-control rounded-0',
        'id' => 'limit',
        'title' => ucwords('Please Fill Out This Field'),
        'required' => true,
        'min' => 10000,
        'value' => 10000,
        'max' => 50000,
        'placeholder' => ucwords('Limit')
    ])?>
</div>

<div class="col-sm-12 col-md-1 col-lg-4 d-flex justify-content-start align-items-end mb-3">
    <?=$this->Form->button('Search',[
        'class' => 'btn btn-primary rounded-0',
        'type' => 'submit',
    ])?>
</div>

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card p-3">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Property Number</th>
                    <th>Serial Number</th>
                    <th>Inventory Number</th>
                    <th>Supply</th>
                    <th>Personel</th>
                    <th>Office</th>
                    <th>Account</th>
                    <th>Ledger</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th>Is Returned</th>
                    <th>Process By</th>
                    <th>Modified</th>
                    <th>Options</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<?=$this->Form->end()?>

<?=$this->Html->script('admin/items/index')?>

