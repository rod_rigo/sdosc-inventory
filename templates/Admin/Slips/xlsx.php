<?php
/**
 * @var \App\View\AppView $this
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$cell = intval(12);

$path = WWW_ROOT. 'templates'. DS. 'Appendix-59.xlsx';
$date = date('m/d/Y', strtotime($slip->created));
$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
$worksheet = $spreadsheet->getActiveSheet();

$worksheet
    ->setCellValue('C6', $slip->custodian->institution->institution);

$worksheet
    ->setCellValue('C7', $slip->custodian->fund_cluster->fund_cluster);

$worksheet
    ->setCellValue('H7', $slip->code);

foreach ($slip->items as $key => $item){

    $worksheet
        ->setCellValue(strval('A'.$cell), $item->quantity);
    $worksheet
        ->getStyle(strval('A'.$cell))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $worksheet
        ->getStyle(strval('A'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $worksheet
        ->setCellValue(strval('B'.$cell), strtolower($item->supply->unit->unit));
    $worksheet
        ->getStyle(strval('B'.$cell))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $worksheet
        ->getStyle(strval('B'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $worksheet
        ->setCellValue(strval('C'.$cell), number_format($item->price,2));
    $worksheet
        ->getStyle(strval('C'.$cell))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $worksheet
        ->getStyle(strval('C'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $worksheet
        ->setCellValue(strval('D'.$cell), number_format($item->total,2));
    $worksheet
        ->getStyle(strval('D'.$cell))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $worksheet
        ->getStyle(strval('D'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $worksheet
        ->setCellValue(strval('E'.$cell), strtoupper($item->supply->supply));
    $worksheet
        ->mergeCells(strval('E'.$cell.':F'.$cell))
        ->getStyle(strval('E'.$cell))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $worksheet
        ->getStyle(strval('E'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $worksheet
        ->getStyle(strval('F'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $worksheet
        ->setCellValue(strval('G'.$cell), strtoupper($item->inventory_number));
    $worksheet
        ->getStyle(strval('G'.$cell))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $worksheet
        ->getStyle(strval('G'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $worksheet
        ->setCellValue(strval('H'.$cell), strtoupper($item->lifespan));
    $worksheet
        ->getStyle(strval('H'.$cell))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $worksheet
        ->getStyle(strval('H'.$cell))
        ->getBorders()
        ->getRight()
        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);

    $cell++;

}

$worksheet
    ->setCellValue(strval('A'.$cell), strtoupper('***Nothing Follows***'));
$worksheet
    ->mergeCells(strval('A'.$cell.':H'.$cell))
    ->getStyle(strval('A'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$worksheet
    ->getStyle(strval('A'.$cell))
    ->getBorders()
    ->getRight()
    ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
$styleArray = [
    'borders' => [
        'top' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
    ],
];
$worksheet->getStyle(strval('A'.($cell).':H'.($cell)))->applyFromArray($styleArray);

$cell++;
$worksheet
    ->setCellValue(strval('A'.$cell), 'Received  from:');
$worksheet
    ->setCellValue(strval('F'.$cell), 'Received by:');
$styleArray = [
    'borders' => [
        'top' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
    ],
];
$worksheet->getStyle(strval('A'.($cell).':H'.($cell)))->applyFromArray($styleArray);

$styleArray = [
    'borders' => [
        'right' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
    ],
];
$worksheet->getStyle(strval('E'.($cell).':E'.(intval($cell) + intval(6))))->applyFromArray($styleArray);
$worksheet->getStyle(strval('H'.($cell).':H'.(intval($cell) + intval(6))))->applyFromArray($styleArray);

$cell++;
$cell++;
$worksheet
    ->setCellValue(strval('A'.$cell), strtoupper($slip->custodian->user->name))
    ->getStyle(strval('A'.$cell))
    ->getFont()
    ->setBold(true)
    ->setUnderline(true);
$worksheet
    ->mergeCells(strval('A'.$cell.':E'.$cell))
    ->getStyle(strval('A'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$worksheet
    ->setCellValue(strval('F'.$cell), strtoupper($slip->custodian->personel->personel))
    ->getStyle(strval('F'.$cell))
    ->getFont()
    ->setBold(true)
    ->setUnderline(true);
$worksheet
    ->mergeCells(strval('F'.$cell.':H'.$cell))
    ->getStyle(strval('F'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$cell++;
$worksheet
    ->setCellValue(strval('A'.$cell), ucwords('Signature Over Printed Name'))
    ->getStyle(strval('A'.$cell))
    ->getFont()
    ->setBold(false)
    ->setUnderline(false);
$worksheet
    ->mergeCells(strval('A'.$cell.':E'.$cell))
    ->getStyle(strval('A'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$worksheet
    ->setCellValue(strval('F'.$cell), ucwords('Signature Over Printed Name'))
    ->getStyle(strval('F'.$cell))
    ->getFont()
    ->setBold(false)
    ->setUnderline(false);
$worksheet
    ->mergeCells(strval('F'.$cell.':H'.$cell))
    ->getStyle(strval('F'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$cell++;
$worksheet
    ->setCellValue(strval('A'.$cell), ($slip->custodian->user->position->position))
    ->getStyle(strval('A'.$cell))
    ->getFont()
    ->setBold(false)
    ->setUnderline(true);
$worksheet
    ->mergeCells(strval('A'.$cell.':E'.$cell))
    ->getStyle(strval('A'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$worksheet
    ->setCellValue(strval('F'.$cell), ($slip->custodian->personel->position->position))
    ->getStyle(strval('F'.$cell))
    ->getFont()
    ->setBold(true)
    ->setUnderline(true);
$worksheet
    ->mergeCells(strval('F'.$cell.':H'.$cell))
    ->getStyle(strval('F'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$cell++;
$worksheet
    ->setCellValue(strval('A'.$cell), $date)
    ->getStyle(strval('A'.$cell))
    ->getFont()
    ->setBold(false)
    ->setUnderline(false);
$worksheet
    ->mergeCells(strval('A'.$cell.':E'.$cell))
    ->getStyle(strval('A'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$worksheet
    ->setCellValue(strval('F'.$cell), $date)
    ->getStyle(strval('F'.$cell))
    ->getFont()
    ->setBold(false)
    ->setUnderline(false);
$worksheet
    ->mergeCells(strval('F'.$cell.':H'.$cell))
    ->getStyle(strval('F'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$cell++;
$worksheet
    ->setCellValue(strval('A'.$cell), 'Date')
    ->getStyle(strval('A'.$cell))
    ->getFont()
    ->setBold(false)
    ->setUnderline(false);
$worksheet
    ->mergeCells(strval('A'.$cell.':E'.$cell))
    ->getStyle(strval('A'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
$worksheet
    ->setCellValue(strval('F'.$cell), 'Date')
    ->getStyle(strval('F'.$cell))
    ->getFont()
    ->setBold(false)
    ->setUnderline(false);
$worksheet
    ->mergeCells(strval('F'.$cell.':H'.$cell))
    ->getStyle(strval('F'.$cell))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$styleArray = [
    'borders' => [
        'bottom' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
    ],
];
$worksheet->getStyle(strval('A'.($cell).':H'.($cell)))->applyFromArray($styleArray);

$worksheet->getPageSetup()->setPrintArea('A1:H'.($cell));

$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->export = $filename;
$export->mime = strtoupper('XLSX');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)){
// Save the spreadsheet
    $writer = new Xlsx($spreadsheet);
    header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
    $writer->save('php://output');
    exit(0);
}