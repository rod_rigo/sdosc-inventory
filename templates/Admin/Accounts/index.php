<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Account> $accounts
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                        <?=$this->Form->label('account', ucwords('account'))?>
                        <?=$this->Form->text('account',[
                            'class' => 'form-control rounded-0',
                            'id' => 'account',
                            'required' => true,
                            'placeholder' => ucwords('account'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                        <?=$this->Form->label('legend', ucwords('legend'))?>
                        <?=$this->Form->text('legend',[
                            'class' => 'form-control rounded-0',
                            'id' => 'legend',
                            'required' => true,
                            'placeholder' => ucwords('legend'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                        <?=$this->Form->label('start_price', ucwords('start price'))?>
                        <?=$this->Form->number('start_price',[
                            'class' => 'form-control rounded-0',
                            'id' => 'start-price',
                            'required' => true,
                            'placeholder' => ucwords('start price'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'min' => intval(0),
                            'step' => 'any'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                        <?=$this->Form->label('end_price', ucwords('end price'))?>
                        <?=$this->Form->number('end_price',[
                            'class' => 'form-control rounded-0',
                            'id' => 'end-price',
                            'required' => true,
                            'placeholder' => ucwords('end price'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'min' => intval(0),
                            'step' => 'any'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                        <?php
                            $isTypes = [
                                'None',
                                'A',
                                'B'
                            ];
                        ?>
                        <?php foreach ($isTypes as $key => $value):?>
                            <div class="icheck-primary d-inline m-2">
                                <?=$this->Form->checkbox('type.'.($key),[
                                    'id' => 'type-'.(intval($key)),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => (intval($key) == intval(0)),
                                    'value' => intval($key),
                                    'class' => 'types'
                                ])?>
                                <?=$this->Form->label('type.'.($key), ucwords($value))?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                        <?php
                        $isAppendixes = [
                            'None',
                            'Appendix-59',
                            'Appendix-73',
                        ];
                        ?>
                        <?php foreach ($isAppendixes as $key => $value):?>
                            <div class="icheck-primary d-inline m-2">
                                <?=$this->Form->checkbox('appendix.'.($key),[
                                    'id' => 'appendix-'.(intval($key)),
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => (intval($key) == intval(0)),
                                    'value' => intval($key),
                                    'class' => 'appendixes'
                                ])?>
                                <?=$this->Form->label('appendix.'.($key), ucwords($value))?>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 d-flex justify-content-start align-items-end my-2">
                        <?=$this->Form->label('color', ucwords('color'), [
                            'class' => 'mr-3'
                        ])?>
                        <?=$this->Form->text('color',[
                            'class' => 'form-control rounded-0',
                            'id' => 'color',
                            'required' => true,
                            'placeholder' => ucwords('color'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'value' => '#fff'
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 d-flex justify-content-start align-items-end my-2">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('labeled',[
                                'id' => 'labeled',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => true,
                            ])?>
                            <?=$this->Form->label('labeled', ucwords('Labeled'))?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 d-flex justify-content-start align-items-end my-2">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => true,
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('is_type',[
                    'id' => 'is-type',
                    'value' => intval(0)
                ]);?>
                <?= $this->Form->hidden('is_appendix',[
                    'id' => 'is-appendix',
                    'value' => intval(0)
                ]);?>
                <?= $this->Form->hidden('is_labeled',[
                    'id' => 'is-labeled',
                    'value' => intval(0)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Account">
            New Account
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Account</th>
                        <th>Legend</th>
                        <th>Color</th>
                        <th>Price Range</th>
                        <th>Is Labeled</th>
                        <th>Is Type</th>
                        <th>Is Appendix</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/accounts/index')?>

