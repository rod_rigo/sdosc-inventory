<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

    <script>
        var id = parseInt(<?=intval($user->id)?>);
    </script>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-account-tab" data-toggle="pill" href="#custom-tabs-account" role="tab" aria-controls="custom-tabs-account" aria-selected="true">
                                Account
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-four-password-tab" data-toggle="pill" href="#custom-tabs-four-password" role="tab" aria-controls="custom-tabs-four-password" aria-selected="false">
                                Password
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade show active" id="custom-tabs-account" role="tabpanel" aria-labelledby="custom-tabs-account-tab">
                            <?= $this->Form->create($user,['class' => 'row', 'type' => 'file', 'id' => 'form']);?>
                            <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                                <?=$this->Form->label('name', ucwords('name'))?>
                                <?=$this->Form->text('name',[
                                    'class' => 'form-control rounded-0',
                                    'id' => 'name',
                                    'required' => true,
                                    'placeholder' => ucwords('name'),
                                    'pattern' => '(.){1,}',
                                    'title' => ucwords('Please Fill Out This Field')
                                ])?>
                                <small></small>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                                <?=$this->Form->label('username', ucwords('username'))?>
                                <?=$this->Form->text('username',[
                                    'class' => 'form-control rounded-0',
                                    'id' => 'username',
                                    'required' => true,
                                    'placeholder' => ucwords('username'),
                                    'pattern' => '(.){1,}',
                                    'title' => ucwords('Please Fill Out This Field')
                                ])?>
                                <small></small>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                                <?=$this->Form->label('email', ucwords('email'))?>
                                <?=$this->Form->email('email',[
                                    'class' => 'form-control rounded-0',
                                    'id' => 'email',
                                    'required' => true,
                                    'placeholder' => ucwords('email'),
                                    'pattern' => '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}',
                                    'title' => ucwords('Please Fill Out This Field')
                                ])?>
                                <small></small>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                                <?=$this->Form->label('position_id', ucwords('position'))?>
                                <?=$this->Form->select('position_id', $positions,[
                                    'class' => 'form-control rounded-0',
                                    'id' => 'position-id',
                                    'required' => true,
                                    'empty' => ucwords('position'),
                                    'pattern' => '(.){1,}',
                                    'title' => ucwords('Please Fill Out This Field')
                                ])?>
                                <small></small>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                                <div class="icheck-primary d-inline">
                                    <?=$this->Form->checkbox('admin',[
                                        'id' => 'admin',
                                        'label' => false,
                                        'hiddenField' => false,
                                        'checked' => boolval($user->is_admin),
                                    ])?>
                                    <?=$this->Form->label('admin', ucwords('Admin'))?>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                                <div class="icheck-primary d-inline">
                                    <?=$this->Form->checkbox('active',[
                                        'id' => 'active',
                                        'label' => false,
                                        'hiddenField' => false,
                                        'checked' => boolval($user->is_active),
                                    ])?>
                                    <?=$this->Form->label('active', ucwords('Active'))?>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                                <?= $this->Form->hidden('is_admin',[
                                    'id' => 'is-admin',
                                    'value' => intval($user->is_admin)
                                ]);?>
                                <?= $this->Form->hidden('is_active',[
                                    'id' => 'is-active',
                                    'value' => intval($user->is_active)
                                ]);?>
                                <?= $this->Form->hidden('token',[
                                    'id' => 'token',
                                    'value' => uniqid()
                                ]);?>
                                <?=$this->Form->button(ucwords('Submit'),[
                                    'class' => 'btn btn-success rounded-0 m-1',
                                    'type' => 'submit',
                                    'title' => ucwords('Submit')
                                ])?>
                            </div>
                            <?= $this->Form->end();?>
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-four-password" role="tabpanel" aria-labelledby="custom-tabs-four-password-tab">
                            <?= $this->Form->create($user,['class' => 'row', 'type' => 'file', 'id' => 'password-form']);?>

                            <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                                <?=$this->Form->label('password', ucwords('password'))?>
                                <?=$this->Form->password('password',[
                                    'class' => 'form-control rounded-0',
                                    'id' => 'password',
                                    'required' => true,
                                    'placeholder' => ucwords('password'),
                                    'pattern' => '(.){1,}',
                                    'title' => ucwords('Please Fill Out This Field'),
                                    'value' => ''
                                ])?>
                                <small></small>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                                <?=$this->Form->label('confirm_password', ucwords('password (Confirm)'))?>
                                <?=$this->Form->password('confirm_password',[
                                    'class' => 'form-control rounded-0',
                                    'id' => 'confirm-password',
                                    'required' => true,
                                    'placeholder' => ucwords('password (Confirm)'),
                                    'pattern' => '(.){1,}',
                                    'title' => ucwords('Please Fill Out This Field')
                                ])?>
                                <small></small>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center">
                                <?= $this->Form->hidden('token',[
                                    'id' => 'token',
                                    'value' => uniqid()
                                ]);?>
                                <?=$this->Form->button(ucwords('Submit'),[
                                    'class' => 'btn btn-success rounded-0 m-1',
                                    'type' => 'submit',
                                    'title' => ucwords('Submit')
                                ])?>
                            </div>
                            <?= $this->Form->end();?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?=$this->Html->script('admin/users/view')?>