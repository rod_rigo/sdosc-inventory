<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Supply">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($user,['class' => 'card card-primary rounded-0', 'type' => 'file', 'id' => 'form']);?>
        <div class="card-header">
            <h3 class="card-title">User Form</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <?=$this->Form->label('name', ucwords('name'))?>
                    <?=$this->Form->text('name',[
                        'class' => 'form-control rounded-0',
                        'id' => 'name',
                        'required' => true,
                        'placeholder' => ucwords('name'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <?=$this->Form->label('username', ucwords('username'))?>
                    <?=$this->Form->text('username',[
                        'class' => 'form-control rounded-0',
                        'id' => 'username',
                        'required' => true,
                        'placeholder' => ucwords('username'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <?=$this->Form->label('email', ucwords('email'))?>
                    <?=$this->Form->email('email',[
                        'class' => 'form-control rounded-0',
                        'id' => 'email',
                        'required' => true,
                        'placeholder' => ucwords('email'),
                        'pattern' => '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <?=$this->Form->label('position_id', ucwords('position'))?>
                    <?=$this->Form->select('position_id', $positions,[
                        'class' => 'form-control rounded-0',
                        'id' => 'position-id',
                        'required' => true,
                        'empty' => ucwords('position'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <?=$this->Form->label('password', ucwords('password'))?>
                    <?=$this->Form->password('password',[
                        'class' => 'form-control rounded-0',
                        'id' => 'password',
                        'required' => true,
                        'placeholder' => ucwords('password'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <?=$this->Form->label('confirm_password', ucwords('password (Confirm)'))?>
                    <?=$this->Form->password('confirm_password',[
                        'class' => 'form-control rounded-0',
                        'id' => 'confirm-password',
                        'required' => true,
                        'placeholder' => ucwords('password (Confirm)'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                    <div class="icheck-primary d-inline">
                        <?=$this->Form->checkbox('admin',[
                            'id' => 'admin',
                            'label' => false,
                            'hiddenField' => false,
                            'checked' => true,
                        ])?>
                        <?=$this->Form->label('admin', ucwords('Admin'))?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                    <div class="icheck-primary d-inline">
                        <?=$this->Form->checkbox('active',[
                            'id' => 'active',
                            'label' => false,
                            'hiddenField' => false,
                            'checked' => true,
                        ])?>
                        <?=$this->Form->label('active', ucwords('Active'))?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?= $this->Form->hidden('is_admin',[
                'id' => 'is-admin',
                'value' => intval(1)
            ]);?>
            <?= $this->Form->hidden('is_active',[
                'id' => 'is-active',
                'value' => intval(1)
            ]);?>
            <?= $this->Form->hidden('token',[
                'id' => 'token',
                'value' => uniqid()
            ]);?>
            <?=$this->Form->button(ucwords('Reset'),[
                'class' => 'btn btn-danger rounded-0 m-1',
                'type' => 'reset',
                'title' => ucwords('Reset')
            ])?>
            <?=$this->Form->button(ucwords('Submit'),[
                'class' => 'btn btn-success rounded-0 m-1',
                'type' => 'submit',
                'title' => ucwords('Submit')
            ])?>
        </div>
        <?= $this->Form->end();?>
    </div>
</div>

<?=$this->Html->script('admin/users/add')?>
