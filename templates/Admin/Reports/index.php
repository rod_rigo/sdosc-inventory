<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Item> $items
 */
?>

<div class="modal fade" id="modal" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create(null,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-5 col-lg-6 my-2">
                        <?=$this->Form->label('start_date', ucwords('Start Date'))?>
                        <?=$this->Form->date('start_date',[
                            'class' => 'form-control rounded-0 required',
                            'id' => 'start-date',
                            'title' => ucwords('Please Fill Out This Field'),
                            'required' => true,
                            'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-6 my-2">
                        <?=$this->Form->label('end_date', ucwords('End Date'))?>
                        <?=$this->Form->date('end_date',[
                            'class' => 'form-control rounded-0 required',
                            'id' => 'end-date',
                            'title' => ucwords('Please Fill Out This Field'),
                            'required' => true,
                            'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                        <?=$this->Form->label('account', ucwords('Account'))?>
                        <?=$this->Form->select('account', $accounts,[
                            'class' => 'form-control rounded-0 required',
                            'id' => 'account',
                            'title' => ucwords('Please Fill Out This Field'),
                            'required' => true,
                            'empty' => ucwords('Account')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                        <?=$this->Form->label('ledger', ucwords('ledger'))?>
                        <?=$this->Form->select('ledger', [],[
                            'class' => 'form-control rounded-0 required',
                            'id' => 'ledger',
                            'title' => ucwords('Please Fill Out This Field'),
                            'required' => true,
                            'empty' => ucwords('ledger')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                        <?=$this->Form->label('institution', ucwords('institution'))?>
                        <?=$this->Form->select('institution', $institutions,[
                            'class' => 'form-control rounded-0 required',
                            'id' => 'institution',
                            'title' => ucwords('Please Fill Out This Field'),
                            'required' => false,
                            'empty' => ucwords('institution')
                        ])?>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between align-items-center">
                <button type="button" class="btn btn-success rounded-0" id="xlsx" title="XLSX">
                    XLSX
                </button>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Search'),[
                    'class' => 'btn btn-primary rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button id="toggle-modal" class="btn btn-primary rounded-0" title="Configuration">
            Configuration
        </button>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Property Number</th>
                        <th>Serial Number</th>
                        <th>Inventory Number</th>
                        <th>Supply</th>
                        <th>Personel</th>
                        <th>Office</th>
                        <th>Account</th>
                        <th>Ledger</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Is Returned</th>
                        <th>Process By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/reports/index')?>

