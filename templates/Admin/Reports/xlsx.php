<?php
/**
 * @var \App\View\AppView $this
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(intval($account->is_appendix) == intval(1)){

    $cell = intval(15);

    $path = WWW_ROOT. 'templates'. DS. 'Annex-A.8.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
    $worksheet = $spreadsheet->getActiveSheet();

    $title =$worksheet
        ->getCell('A3')
        ->getValue();
    $title = preg_replace('/{ACCOUNT}/i',strtoupper(@$account->account), $title);
    $worksheet
        ->setCellValue('A3', $title);

    $worksheet
        ->setCellValue('A4', strtoupper(@$ledger->ledger));
    $worksheet
        ->getStyle(strval('A4'))
        ->getFont()
        ->setBold(true)
        ->setUnderline(true);

    $title = $worksheet
        ->getCell('A5')
        ->getValue();
    $title = preg_replace('/{LEDGER}/i',strtoupper(@$ledger->ledger), $title);
    $worksheet
        ->setCellValue('A5', $title);
    $worksheet
        ->getStyle(strval('A5'))
        ->getFont()
        ->setBold(false)
        ->setUnderline(false);
    $worksheet
        ->getStyle(strval('A5'))
        ->getFont()
        ->setName('Times New Roman');

    $richText = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
    $richText->createText('As at ');
    $asAt = $richText
        ->createTextRun($date);
    $asAt
        ->getFont()
        ->setBold(true);
    $asAt->getFont()->setUnderline(true);
    $worksheet->getCell('A6')->setValue($richText);

    $worksheet
        ->setCellValue('A10', null);

    $richText = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
    $richText->createText('For which ');
    $title = $richText
        ->createTextRun('___'.strtoupper($institution->personel->personel).'___');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun(', ');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman');
    $title = $richText
        ->createTextRun('___'.strtoupper($institution->position->position).'___');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun(', ');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman');
    $title = $richText
        ->createTextRun('__________'.strtoupper($institution->institution).'__________');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $richText->createText(' ');
    $title = $richText
        ->createTextRun('is accountable, having assumed such accountability on');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun('___'.strtoupper($institution->date).'___');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun('.');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman');
    $worksheet
        ->getCell('A10')
        ->setValue($richText);

    $worksheet
        ->getStyle(strval('A10'))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM)
        ->setWrapText(true);

    foreach ($items as $key => $item){

        $worksheet
            ->setCellValue(strval('A'.$cell), $item->supply->category->category);
        $worksheet
            ->getStyle(strval('A'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('A'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('A'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('B'.$cell), strtoupper($item->supply->supply));
        $worksheet
            ->getStyle(strval('B'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('B'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('B'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('C'.$cell), $item->property_number);
        $worksheet
            ->getStyle(strval('C'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('C'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('C'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(intval(11));

        $worksheet
            ->setCellValue(strval('D'.$cell), $item->supply->unit->unit);
        $worksheet
            ->getStyle(strval('D'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('D'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('D'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('E'.$cell), number_format(doubleval($item->total),2));
        $worksheet
            ->getStyle(strval('E'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('E'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('E'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('F'.$cell), intval($item->quantity));
        $worksheet
            ->getStyle(strval('F'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('F'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('F'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('G'.$cell), intval($item->quantity));
        $worksheet
            ->getStyle(strval('G'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('G'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('G'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->getStyle(strval('H'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('H'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->getStyle(strval('I'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('I'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('J'.$cell), $item->remarks);
        $worksheet
            ->getStyle(strval('J'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('J'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('J'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $cell++;

    }

    $styleArray = [
        'borders' => [
            'top' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            ],
        ],
    ];
    $worksheet->getStyle(strval('A'.($cell).':J'.($cell)))->applyFromArray($styleArray);

    $cell++;
    $column = 'A';

    $row = intval($cell);
    foreach ($headings as $key => $heading){

        $worksheet
            ->setCellValue(strval($column.$row), ucwords($heading['heading']));
        $worksheet
            ->getStyle(strval($column.$row))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval($column.$row))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $column++;

        foreach ($heading['signatories'] as $signatory){
            $worksheet
                ->setCellValue(strval($column.(intval($row) + intval(2))), strtoupper($signatory['signatory']))
                ->getStyle(strval($column.(intval($row) + intval(2))))
                ->getFont()
                ->setBold(true)
                ->setUnderline(true);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(2))))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(2))))
                ->getFont()
                ->setName('Times New Roman')
                ->setSize(11);

            $worksheet
                ->setCellValue(strval($column.(intval($row) + intval(3))), ucwords($signatory['description']))
                ->getStyle(strval($column.(intval($row) + intval(3))))
                ->getFont()
                ->setBold(false)
                ->setUnderline(false);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(3))))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP)
                ->setWrapText(true);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(3))))
                ->getFont()
                ->setName('Times New Roman')
                ->setSize(11);

            $startMerge = $column;
            $endMerge = $column;
            if(intval($signatory['merge']) > intval(0)){

                for($i = intval(0); intval($i) < intval($signatory['merge']); $i++){
                    $endMerge++;
                }

                $worksheet->mergeCells(strval($startMerge.(intval($row) + intval(3)).':'.$endMerge.(intval($row) + intval(3))));
            }

            for($i = intval(0); intval($i) < intval($signatory['gap']); $i++){
                $column++;
            }

        }

    }

    $cell++;
    $cell++;
    $cell++;
    $cell++;

    $worksheet->getPageSetup()->setPrintArea('A1:J'.($cell));

    $export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->newEmptyEntity();
    $export->user_id = intval(@$auth['id']);
    $export->export = $filename;
    $export->mime = strtoupper('XLSX');
    if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)){
// Save the spreadsheet
        $writer = new Xlsx($spreadsheet);
        header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
        $writer->save('php://output');
        exit(0);
    }

}elseif (intval($account->is_appendix) == intval(2)){

    $cell = intval(15);

    $path = WWW_ROOT. 'templates'. DS. 'Appendix-73.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
    $worksheet = $spreadsheet->getActiveSheet();

    $title =$worksheet
        ->getCell('A3')
        ->getValue();
    $title = preg_replace('/{ACCOUNT}/i',strtoupper(@$account->account), $title);
    $worksheet
        ->setCellValue('A3', $title);

    $worksheet
        ->setCellValue('A4', strtoupper(@$ledger->ledger));
    $worksheet
        ->getStyle(strval('A4'))
        ->getFont()
        ->setBold(true)
        ->setUnderline(true);

    $title = $worksheet
        ->getCell('A5')
        ->getValue();
    $title = preg_replace('/{LEDGER}/i',strtoupper(@$ledger->ledger), $title);
    $worksheet
        ->setCellValue('A5', $title);
    $worksheet
        ->getStyle(strval('A5'))
        ->getFont()
        ->setBold(false)
        ->setUnderline(false);
    $worksheet
        ->getStyle(strval('A5'))
        ->getFont()
        ->setName('Times New Roman');

    $richText = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
    $richText->createText('As at ');
    $asAt = $richText
        ->createTextRun($date);
    $asAt
        ->getFont()
        ->setBold(true);
    $asAt->getFont()->setUnderline(true);
    $worksheet->getCell('A6')->setValue($richText);

    $worksheet
        ->setCellValue('A10', null);

    $richText = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
    $richText->createText('For which ');
    $title = $richText
        ->createTextRun('___'.strtoupper($institution->personel->personel).'___');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun(', ');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman');
    $title = $richText
        ->createTextRun('___'.strtoupper($institution->position->position).'___');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun(', ');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman');
    $title = $richText
        ->createTextRun('__________'.strtoupper($institution->institution).'__________');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $richText->createText(' ');
    $title = $richText
        ->createTextRun('is accountable, having assumed such accountability on');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun('___'.strtoupper($institution->date).'___');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(true)
        ->setUnderline(true)
        ->setName('Times New Roman')
        ->setSize(10);
    $title = $richText
        ->createTextRun('.');
    $title
        ->getFont()
        ->setBold(true)
        ->setItalic(false)
        ->setUnderline(false)
        ->setName('Times New Roman');
    $worksheet
        ->getCell('A10')
        ->setValue($richText);

    $worksheet
        ->getStyle(strval('A10'))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM)
        ->setWrapText(true);

    foreach ($items as $key => $item){

        $worksheet
            ->setCellValue(strval('A'.$cell), $item->supply->category->category);
        $worksheet
            ->getStyle(strval('A'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('A'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('A'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('B'.$cell), strtoupper($item->supply->supply));
        $worksheet
            ->getStyle(strval('B'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('B'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('B'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('C'.$cell), $item->property_number);
        $worksheet
            ->getStyle(strval('C'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('C'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('C'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(intval(11));

        $worksheet
            ->setCellValue(strval('D'.$cell), $item->supply->unit->unit);
        $worksheet
            ->getStyle(strval('D'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('D'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('D'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('E'.$cell), number_format(doubleval($item->total),2));
        $worksheet
            ->getStyle(strval('E'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('E'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('E'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('F'.$cell), intval($item->quantity));
        $worksheet
            ->getStyle(strval('F'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('F'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('F'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('G'.$cell), intval($item->quantity));
        $worksheet
            ->getStyle(strval('G'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('G'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('G'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->getStyle(strval('H'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('H'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->getStyle(strval('I'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('I'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $worksheet
            ->setCellValue(strval('J'.$cell), $item->remarks);
        $worksheet
            ->getStyle(strval('J'.$cell))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval('J'.$cell))
            ->getBorders()
            ->getRight()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);
        $worksheet
            ->getStyle(strval('J'.$cell))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $cell++;

    }

    $styleArray = [
        'borders' => [
            'top' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            ],
        ],
    ];
    $worksheet->getStyle(strval('A'.($cell).':J'.($cell)))->applyFromArray($styleArray);

    $cell++;
    $column = 'A';

    $row = intval($cell);
    foreach ($headings as $key => $heading){

        $worksheet
            ->setCellValue(strval($column.$row), ucwords($heading['heading']));
        $worksheet
            ->getStyle(strval($column.$row))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $worksheet
            ->getStyle(strval($column.$row))
            ->getFont()
            ->setName('Times New Roman')
            ->setSize(11);

        $column++;

        foreach ($heading['signatories'] as $signatory){
            $worksheet
                ->setCellValue(strval($column.(intval($row) + intval(2))), strtoupper($signatory['signatory']))
                ->getStyle(strval($column.(intval($row) + intval(2))))
                ->getFont()
                ->setBold(true)
                ->setUnderline(true);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(2))))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(2))))
                ->getFont()
                ->setName('Times New Roman')
                ->setSize(11);

            $worksheet
                ->setCellValue(strval($column.(intval($row) + intval(3))), ucwords($signatory['description']))
                ->getStyle(strval($column.(intval($row) + intval(3))))
                ->getFont()
                ->setBold(false)
                ->setUnderline(false);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(3))))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP)
                ->setWrapText(true);
            $worksheet
                ->getStyle(strval($column.(intval($row) + intval(3))))
                ->getFont()
                ->setName('Times New Roman')
                ->setSize(11);

            $startMerge = $column;
            $endMerge = $column;
            if(intval($signatory['merge']) > intval(0)){

                for($i = intval(0); intval($i) < intval($signatory['merge']); $i++){
                    $endMerge++;
                }

                $worksheet->mergeCells(strval($startMerge.(intval($row) + intval(3)).':'.$endMerge.(intval($row) + intval(3))));
            }

            for($i = intval(0); intval($i) < intval($signatory['gap']); $i++){
                $column++;
            }

        }

    }
    $cell++;
    $cell++;
    $cell++;
    $cell++;

    $worksheet->getPageSetup()->setPrintArea('A1:J'.($cell));

    $export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->newEmptyEntity();
    $export->user_id = intval(@$auth['id']);
    $export->export = $filename;
    $export->mime = strtoupper('XLSX');
    if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)){
// Save the spreadsheet
        $writer = new Xlsx($spreadsheet);
        header('Content-Disposition: attachment; filename="' . urlencode('' . ucwords(preg_replace('/\s+/', '_', $filename)) . '.xlsx') . '"');
        $writer->save('php://output');
        exit(0);
    }

}else{
    exit(0);
}