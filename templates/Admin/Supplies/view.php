<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supply $supply
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $brands
 * @var \Cake\Collection\CollectionInterface|string[] $categories
 * @var \Cake\Collection\CollectionInterface|string[] $units
 * @var \Cake\Collection\CollectionInterface|string[] $accounts
 * @var \Cake\Collection\CollectionInterface|string[] $ledgers
 */
?>

<script>
    var id = parseInt(<?=intval($supply->id)?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'index'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Supply">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-9 col-lg-9">
        <?= $this->Form->create($supply,['class' => 'card card-primary rounded-0', 'type' => 'file', 'id' => 'form']);?>
        <div class="card-header">
            <h3 class="card-title">Supply Form</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-6 my-2">
                    <?=$this->Form->label('supply', ucwords('supply'))?>
                    <?=$this->Form->text('supply',[
                        'class' => 'form-control rounded-0',
                        'id' => 'supply',
                        'required' => true,
                        'placeholder' => ucwords('supply'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2 my-2">
                    <?=$this->Form->label('price', ucwords('price'))?>
                    <?=$this->Form->number('price',[
                        'class' => 'form-control rounded-0',
                        'id' => 'price',
                        'required' => true,
                        'placeholder' => ucwords('price'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                        'min' => 0,
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2 my-2">
                    <?=$this->Form->label('stocks', ucwords('stocks'))?>
                    <?=$this->Form->number('stocks',[
                        'class' => 'form-control rounded-0',
                        'id' => 'stocks',
                        'required' => true,
                        'placeholder' => ucwords('stocks'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                        'min' => 0,
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-2 my-2">
                    <?=$this->Form->label('issued', ucwords('issued'))?>
                    <?=$this->Form->number('issued',[
                        'class' => 'form-control rounded-0',
                        'id' => 'issued',
                        'required' => true,
                        'placeholder' => ucwords('issued'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                        'min' => 0,
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 my-2">
                    <?=$this->Form->label('brand_id', ucwords('brand'))?>
                    <?=$this->Form->select('brand_id', $brands,[
                        'class' => 'form-control rounded-0',
                        'id' => 'brand-id',
                        'required' => true,
                        'empty' => ucwords('brand'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 my-2">
                    <?=$this->Form->label('category_id', ucwords('category'))?>
                    <?=$this->Form->select('category_id', $categories,[
                        'class' => 'form-control rounded-0',
                        'id' => 'category-id',
                        'required' => true,
                        'empty' => ucwords('category'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 my-2">
                    <?=$this->Form->label('unit_id', ucwords('unit'))?>
                    <?=$this->Form->select('unit_id', $units,[
                        'class' => 'form-control rounded-0',
                        'id' => 'unit-id',
                        'required' => true,
                        'empty' => ucwords('unit'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 my-2">
                    <?=$this->Form->label('account_id', ucwords('account'))?>
                    <?=$this->Form->select('account_id', $accounts,[
                        'class' => 'form-control rounded-0',
                        'id' => 'account-id',
                        'required' => true,
                        'empty' => ucwords('account'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-3 col-lg-3 my-2">
                    <?=$this->Form->label('ledger_id', ucwords('ledger'))?>
                    <?=$this->Form->select('ledger_id', $ledgers,[
                        'class' => 'form-control rounded-0',
                        'id' => 'ledger-id',
                        'required' => true,
                        'empty' => ucwords('Choose account'),
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-5 my-2">
                    <?=$this->Form->label('lifespan', ucwords('lifespan'))?>
                    <?=$this->Form->text('lifespan',[
                        'class' => 'form-control rounded-0',
                        'id' => 'lifespan',
                        'required' => false,
                        'placeholder' => ucwords('lifespan'),
                        'title' => ucwords('Please Fill Out This Field')
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-center my-2">
                    <div class="icheck-primary d-inline">
                        <?=$this->Form->checkbox('expired',[
                            'id' => 'expired',
                            'label' => false,
                            'hiddenField' => false,
                            'checked' => boolval($supply->is_expired),
                        ])?>
                        <?=$this->Form->label('expired', ucwords('expired'))?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-4 my-2">
                    <?=$this->Form->label('expired_at', ucwords('Expired At'))?>
                    <?=$this->Form->date('expired_at',[
                        'class' => 'form-control rounded-0',
                        'id' => 'expired-at',
                        'required' => boolval($supply->is_expired),
                        'readonly' => !boolval($supply->is_expired),
                        'title' => ucwords('This Field Is Required When Expired Is Checked'),
                        'empty' => !boolval($supply->is_expired)
                    ])?>
                    <small>This Field Is Required When Expired Is Checked</small>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                    <div class="icheck-primary d-inline">
                        <?=$this->Form->checkbox('active',[
                            'id' => 'active',
                            'label' => false,
                            'hiddenField' => false,
                            'checked' => boolval($supply->is_active),
                        ])?>
                        <?=$this->Form->label('active', ucwords('Active'))?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->hidden('user_id',[
                'id' => 'user-id',
                'required' => true,
                'readonly' => true,
                'value' => intval(@$auth['id'])
            ])?>
            <?= $this->Form->hidden('is_active',[
                'id' => 'is-active',
                'value' => intval($supply->is_active)
            ]);?>
            <?= $this->Form->hidden('is_expired',[
                'id' => 'is-expired',
                'value' => intval($supply->is_expired)
            ]);?>
            <?=$this->Form->button(ucwords('Reset'),[
                'class' => 'btn btn-danger rounded-0 m-1',
                'type' => 'reset',
                'title' => ucwords('Reset')
            ])?>
            <?=$this->Form->button(ucwords('Submit'),[
                'class' => 'btn btn-success rounded-0 m-1',
                'type' => 'submit',
                'title' => ucwords('Submit')
            ])?>
        </div>
        <?= $this->Form->end();?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="card card-primary rounded-0">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex flex-column justify-content-center align-items-center">
                        <?php
                            use CodeItNow\BarcodeBundle\Utils\QrCode;

                            $qr = new QrCode();
                            $qr->setText(strval($supply->id))
                                ->setSize(300)
                                ->setPadding(10)
                                ->setErrorCorrection('high')
                                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                                ->setLabel(null)
                                ->setLabelFontSize(16)
                                ->setImageType(QrCode::IMAGE_TYPE_PNG);

                            $qrSrc = 'data:'.($qr->getContentType()).';base64,'.($qr->generate()).'';
                        ?>
                        <img src="<?=($qrSrc)?>" alt="QR CODE" class="img-thumbnail my-2" id="qr-image-preview" loading="lazy">

                        <?php
                            use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;

                            $bar = new BarcodeGenerator();
                            $bar->setText(strval($supply->id));
                            $bar->setType(BarcodeGenerator::Gs1128);
                            $bar->setNoLengthLimit(true);;
                            $bar->setAllowsUnknownIdentifier(true);
                            $bar->setScale(5);
                            $bar->setThickness(25);
                            $bar->setFontSize(10);
                            $barSrc = 'data:image/png;base64,'.($bar->generate()).'';

                        ?>
                        <img src="<?=($barSrc)?>" alt="Bar CODE" class="img-thumbnail my-2" id="bar-image-preview" loading="lazy">

                        <button type="button" class="btn btn-primary btn-block rounded-0 my-2" id="qr-download">Download | <i class="fa fa-qrcode"></i></button>
                        <button type="button" class="btn btn-primary btn-block rounded-0 my-2" id="bar-download">Download | <i class="fa fa-barcode"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/supplies/view')?>
