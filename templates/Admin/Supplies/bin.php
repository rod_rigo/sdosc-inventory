<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Supply> $supplies
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Supply</th>
                        <th>Price</th>
                        <th>Stocks</th>
                        <th>Issued</th>
                        <th>Brand</th>
                        <th>Category</th>
                        <th>Unit</th>
                        <th>Account</th>
                        <th>Ledger</th>
                        <th>Lifespan</th>
                        <th>Expired At</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Deleted</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/supplies/bin')?>

