<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = (!empty($logos))? strtoupper($logos->title): 'Title';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?> |
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('img/'.(@$logos->logo),'/img/'.(@$logos->logo), ['type'=>'icon']); ?>
    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">
    <?= $this->Html->meta('csrf-token',$this->request->getAttribute('csrfToken')) ?>

    <?=$this->Html->css('https://fonts.googleapis.com/css?family=Material+Icons')?>
    <?=$this->Html->css([
        '/plugins/fontawesome-free/css/all.min',
        '/dist/css/adminlte.min',
        '/jquery/css/jquery-ui',
        '/datatables/css/jquery.dataTables.min',
        '/datatables/css/responsive.bootstrap4.min',
        '/datatables/css/buttons/buttons.dataTables.min',
        '/datatables/css/buttons/buttons.bootstrap4.min',
        '/datatables/css/dataTables.bootstrap4.min',
        '/i-check/css/icheck-bootstrap',
        '/print-js/css/print',
        '/select2/css/select2',
        '/pivot/css/c3.min',
        '/pivot/css/pivot',
        '/pivot/css/pivot.min',
        '/cropper/css/cropper',
        '/jqspreadsheet/css/jexcel',
        '/jqspreadsheet/css/jsuites',
        '/select2/css/select2',
        '/jq-spectrum/css/spectrum',
        'custom',
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/moment/js/moment',
        '/chartjs/js/chart',
        '/ck-editor/js/ckeditor',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/sweet-alert/js/sweetalert2.all',
        '/sweet-alert/js/sweetalert2',
        '/turbo-links/js/turbolinks',
        '/datatables/js/jquery.dataTables.min',
        '/datatables/js/dataTables.responsive.min',
        '/datatables/js/responsive.bootstrap4.min',
        '/datatables/js/buttons/buttons.colVis.min',
        '/datatables/js/buttons/buttons.print.min',
        '/datatables/js/buttons/dataTables.buttons.min',
        '/datatables/js/buttons/jszip.min',
        '/datatables/js/buttons/pdfmake.min',
        '/datatables/js/buttons/vfs_fonts',
        '/datatables/js/buttons/html2pdf.bundle.min',
        '/datatables/js/buttons/buttons.html5.min',
        '/datatables/js/buttons/buttons.bootstrap4.min',
        '/excel-js/js/exceljs',
        '/print-js/js/print',
        '/select2/js/select2',
        '/pivot/js/jquery.ui.touch-punch.min',
        '/pivot/js/pivot',
        '/pivot/js/pivot.min',
        '/pivot/js/c3.min',
        '/pivot/js/c3_renderers.min',
        '/pivot/js/gchart_renderers',
        '/pivot/js/d3.min',
        '/pivot/js/d3_renderers',
        '/pivot/js/plotly.min.js',
        '/pivot/js/plotly_renderers',
        '/jqspreadsheet/js/jexcel',
        '/jqspreadsheet/js/jsuites',
        '/filesaver-js/js/FileSaver',
        '/cropper/js/cropper',
        '/html5-qrcode/js/html5-qrcode.min',
        '/select2/js/select2',
        '/jq-spectrum/js/spectrum',
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var mainurl = window.location.origin+'/sdosc-inventory/secretariat/';
        var auth = JSON.stringify(<?=json_encode($auth)?>);
    </script>

</head>
<body class="sidebar-mini sidebar-collapse hold-transition layout-fixed">

<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <?=$this->element('secretariat/navbar')?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?=$this->element('secretariat/sidebar')?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?=$this->element('secretariat/header')?>

        <!-- Main content -->
        <section class="content">

            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->

<?=$this->Html->script([
    '/plugins/bootstrap/js/bootstrap.bundle.min',
    '/dist/js/adminlte.min',
    'secretariat'
])?>
</body>
</html>
