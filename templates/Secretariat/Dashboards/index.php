<?php
/**
 * @var \App\View\AppView $this
 */
?>

    <div class="row">

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-boxes"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Supplies</span>
                    <span class="info-box-number" id="total-supplies">
                        0
                    </span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-building"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Offices</span>
                    <span class="info-box-number" id="total-offices">
                        0
                    </span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-users"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Personels</span>
                    <span class="info-box-number" id="total-personels">
                        0
                    </span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-user-cog"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Users</span>
                    <span class="info-box-number" id="total-users">
                        0
                    </span>
                </div>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-briefcase"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Items (Today)</span>
                    <span class="info-box-number" id="items-today">
                        0
                    </span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-briefcase"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Items (Week)</span>
                    <span class="info-box-number" id="items-week">
                        0
                    </span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-briefcase"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Items (Month)</span>
                    <span class="info-box-number" id="items-month">
                        0
                    </span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="fas fa-briefcase"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Items (Year)</span>
                    <span class="info-box-number" id="items-year">
                        0
                    </span>
                </div>
            </div>
        </div>

    </div>

    <?=$this->Form->create(null,['id' => 'form', 'class' => 'row', 'type' => 'file'])?>
        <div class="col-sm-12 col-md-5 col-lg-5 my-2">
            <?=$this->Form->date('start_date',[
                'id' => 'start-date',
                'class' => 'form-control rounded-0',
                'required' => true,
                'title' => ucwords('please fill out this field'),
                'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
            ])?>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-5 my-2">
            <?=$this->Form->date('end_date',[
                'id' => 'end-date',
                'class' => 'form-control rounded-0',
                'required' => true,
                'title' => ucwords('please fill out this field'),
                'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d'),
            ])?>
        </div>
        <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end my-2">
            <?=$this->Form->button('Submit',[
                'class' => 'btn btn-primary btn-block rounded-0',
                'type' => 'submit'
            ])?>
        </div>
    <?=$this->Form->end()?>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-7 my-2">
            <div class="card border-0 rounded-0">
                <div class="card-body">
                    <canvas id="custodian-month-chart" style="height: 450px; width: 100%;"></canvas>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-4 col-lg-5 my-2">
            <div class="card border-0 rounded-0">
                <div class="card-body">
                    <canvas id="custodian-week-chart" style="height: 450px; width: 100%;"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 my-2">
            <div class="card border-0 rounded-0">
                <div class="card-body">
                    <canvas id="item-ledger-chart" style="height: 480px; width: 100%;"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8 my-2">
            <div class="card border-0 rounded-0">
                <div class="card-body">
                    <canvas id="item-office-chart" style="height: 450px; width: 100%;"></canvas>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card border-0 rounded-0">
                <div class="card-body">
                    <canvas id="item-slip-chart" style="height: 450px; width: 100%;"></canvas>
                </div>
            </div>
        </div>
    </div>

    <?=$this->Html->script('secretariat/dashboards/index')?>