<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 6/28/2024
 * Time: 10:17 AM
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Item> $item
 */

use Dompdf\Dompdf;
use Dompdf\Options;
use CodeItNow\BarcodeBundle\Utils\QrCode;

// Initialize Dompdf with options
$options = new Options();
$options->set('isPhpEnabled', true);
$options->set('isRemoteEnabled', true);
$options->set('isHtml5ParserEnabled', true);
$options->set('defaultFont', 'Arial');
$dompdf = new Dompdf($options);

$content = '';
$path = WWW_ROOT .'img'. DS. 'logo-deped.png';
$file = new \Cake\Filesystem\File($path);
$stamp = '';
if($file->exists()){
    $stamp = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $stamp = 'data:'.($mime).';base64,'.($stamp);
}

foreach ($custodian->items as $item){

    $qrVal = [
        $item->property_number,
        $item->account->account,
        $item->supply->supply.' - '.$item->supply->brand->brand,
        number_format($item->total,2),
        date('m/d/Y', strtotime($item->created)),
        $custodian->personel->personel
    ];
    $qr = new QrCode();
    $qr
        ->setText(strval($item->property_number))
        ->setSize(300)
        ->setPadding(10)
        ->setErrorCorrection('high')
        ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
        ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
        ->setLabel(null)
        ->setLabelFontSize(16)
        ->setImageType(QrCode::IMAGE_TYPE_PNG);
    $qrSrc = 'data:'.$qr->getContentType().';base64,'.$qr->generate().'';

    if(in_array(intval($item->account->is_type), [intval(1)])){
        $content .='<div style="height: 100%; width: 100%; background-color: '.($item->account->color).'">';
        $content.= '<div style="width: 25%; height: 100%; position: relative; float: left">';
        $content.= '<img style="height: 40px; width: 80px; margin-top:10px; margin-left:10px;" src="'.($stamp).'">';
        $content.= '<img style="height: 73px; width: 73px; margin-left:12px; margin-top:5px;" src="'.($qrSrc).'">';
        $content.= '</div>';
        $content.= '<div style="width: 75%; height: 100%;position: relative; float: right;">';
        $content .= '<table>';
        $content .= '<tbody>';

        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Property Number</td><td style="text-align: right; width: 170px !important; border-right: 1px solid #000;font-size: '.((intval(strlen($item->property_number)) >= intval(32))? '6px': '10px').';">'.(strval($item->property_number)).'</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Asset Classification</td><td style="text-align: right; font-size: '.((intval(strlen($item->ledger->ledger)) >= intval(32))? '6px': '10px').'; width: 170px !important;border-right: 1px solid #000;">'.(strval($item->ledger->ledger)).'</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Item/Brand/Model</td><td style="text-align: right; font-size: '.((intval(strlen(strval($item->supply->supply.' - '.$item->supply->brand->brand))) >= intval(32))? '6px': '10px').'; width: 170px !important;border-right: 1px solid #000;">'.(strval($item->supply->supply.' - '.$item->supply->brand->brand)).'</td>';
        $content .= '</t7>';
        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Serial Number</td><td style="text-align: right; font-size: 10px; width: 170px !important;border-right: 1px solid #000;">'.($item->serial_number).'</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Acquisition Cost</td><td style="text-align: right; font-size: '.((intval(strlen(strval(number_format($item->total,2)))) >= intval(32))? '6px': '10px').'; width: 170px !important;border-right: 1px solid #000;">'.(number_format($item->total,2)).'</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Acquisition Date</td><td style="text-align: right; font-size: 10px; width: 170px !important;border-right: 1px solid #000;">'.(date('m/d/Y', strtotime($item->created))).'</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Person Accountable</td><td style="text-align: right; font-size: '.((intval(strlen($custodian->personel->personel)) >= intval(32))? '6px': '10px').'; width: 170px !important;border-right: 1px solid #000;">'.(strtoupper($custodian->personel->personel)).'</td>';
        $content .= '</tr>';
        $content .= '<tr>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 120px !important;border-right: 1px solid #000;">Validation/Signature</td><td style="text-align: right; font-size: 10px; width: 170px !important;border-right: 1px solid #000;"></td>';
        $content .= '</tr>';
        $content .= '<tr style="border: none !important;">';
        $content .= '<td style="font-size: 6px; text-align: center;" colspan="2"><i>*Removing or tampering of this sticker is punishable by Law*</i></td><td style="border: none;" </td>';
        $content .= '</tr>';

        $content .= '</tbody>';
        $content .= '</table>';
        $content.= '</div>';
        $content .='</div>';
        $content .='<br>';
        $content .= '<div style="page-break-after: avoid;"></div>';
    }else{
        $content .='<div style="height: 100%; width: 100%; background-color: '.($item->account->color).'">';
        $content.= '<div style="width: 25%; height: 100%; position: relative; float: left">';
        $content.= '<img style="height: 40px; width: 80px; margin-top:10px; margin-left:10px;" src="'.($stamp).'">';
        $content.= '<img style="height: 73px; width: 73px; margin-left:12px; margin-top:5px;" src="'.($qrSrc).'">';
        $content.= '</div>';
        $content.= '<div style="width: 75%; height: 100%;position: relative; float: right;">';
        $content.= '<h2 style="text-align: center; margin-top: 15px;">'.($item->property_number).'</h2>';
        $margin = '';
        if(intval(strlen($item->supply->supply)) >= intval(18)){
            $content.= '<h3 style="text-align: center;">'.(strtoupper($item->supply->supply)).'</h3>';
            $margin = '45';
        }else{
            $content.= '<h2 style="text-align: center;">'.(strtoupper($item->supply->supply)).'</h2>';
            $margin = '35';
        }
        $content .= '<table style="margin-top: '.($margin).'px;">';
        $content .= '<tbody>';
        $content .= '<td style="font-size: 10px;  text-align: left; padding: 1.2px; width: 50px; text-transform: none !important;">Validation/Signature</td><td style="text-align: right; font-size: 10px;;border-bottom: 1px solid #000; width: 190px;"></td>';
        $content .= '</tr>';
        $content .= '<tr style="border: none !important;">';
        $content .= '<td style="font-size: 8px; text-align: left;" colspan="2"><i>*Removing or tampering of this sticker is punishable by Law*</i></td><td style="border: none;" </td>';
        $content .= '</tr>';

        $content .= '</tbody>';
        $content .= '</table>';
        $content.= '</div>';
        $content .='</div>';
        $content .='<br>';
        $content .= '<div style="page-break-after: avoid;"></div>';
    }
}

// Load HTML content
$html = '
<!DOCTYPE html>
<html>
<head>
    <title>'.(strval($custodian->code)).'</title>
</head>
<style>
    *{
        margin:  0;
        padding: 0;
        box-sizing: border-box;
    }
    table{ 
        width: 100%;
        border-collapse: collapse; 
     }
     table tr{
         border: 1px solid #000;
     }
     table td{ 
         text-transform:  uppercase;
         font-weight: bold;
     }
</style>
<body>
    '.($content).'
</body>
</html>
';

$dompdf->loadHtml($html);

// Set the paper size and orientation
$height = (doubleval(108) * doubleval(2.83465)); // 108 mm to points
$width= (doubleval(36) * doubleval(2.83465)); // 36 mm to points
$dompdf->setPaper([0, 0, $width, $height], 'landscape');

// Render the HTML as PDF
$dompdf->render();

$export = \Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->newEmptyEntity();
$export->user_id = intval(@$auth['id']);
$export->export = (strval($custodian->code)).'.pdf';
$export->mime = strtoupper('PDF');
if(\Cake\ORM\TableRegistry::getTableLocator()->get('Exports')->save($export)){
    // Output the generated PDF to Browser
    $dompdf->stream((strval($custodian->code)).'.pdf', ['Attachment' => false]);
    exit(0);
}
?>
