<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Signatory> $signatories
 */
?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                        <?=$this->Form->label('position_id', ucwords('position'))?>
                        <?=$this->Form->select('position_id', $positions,[
                            'class' => 'form-control rounded-0',
                            'id' => 'position-id',
                            'required' => true,
                            'empty' => ucwords('position'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 my-2">
                        <?=$this->Form->label('heading_id', ucwords('heading'))?>
                        <?=$this->Form->select('heading_id', $headings,[
                            'class' => 'form-control rounded-0',
                            'id' => 'heading-id',
                            'required' => true,
                            'empty' => ucwords('heading'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-8 my-2">
                        <?=$this->Form->label('signatory', ucwords('signatory'))?>
                        <?=$this->Form->text('signatory',[
                            'class' => 'form-control rounded-0',
                            'id' => 'signatory',
                            'required' => true,
                            'placeholder' => ucwords('signatory'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-2 my-2">
                        <?=$this->Form->label('gap', ucwords('gap'))?>
                        <?=$this->Form->number('gap',[
                            'class' => 'form-control rounded-0',
                            'id' => 'gap',
                            'required' => true,
                            'placeholder' => ucwords('gap'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'value' => 0,
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-2 my-2">
                        <?=$this->Form->label('merge', ucwords('merge'))?>
                        <?=$this->Form->number('merge',[
                            'class' => 'form-control rounded-0',
                            'id' => 'merge',
                            'required' => true,
                            'placeholder' => ucwords('merge'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'value' => 0,
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                        <?=$this->Form->label('description', ucwords('description'))?>
                        <?=$this->Form->textarea('description',[
                            'class' => 'form-control rounded-0',
                            'id' => 'description',
                            'required' => true,
                            'placeholder' => ucwords('description'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end my-2">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => true,
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('order_position',[
                    'id' => 'order-position',
                    'value' => intval(0)
                ]);?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="New Signatory">
            New Signatory
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Position</th>
                        <th>Heading</th>
                        <th>Signatory</th>
                        <th>Description</th>
                        <th>Gap</th>
                        <th>Merge</th>
                        <th>Is Active</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('secretariat/signatories/index')?>
