<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Logbook $logbook
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $supplies
 */
?>

<script>
    var stocks = parseInt(<?=intval($supply->stocks)?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'index'])?>" link id="toggle-modal" class="btn btn-primary rounded-0" title="New Supply">
            Return
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <?= $this->Form->create($logbook,['class' => 'card card-primary rounded-0', 'type' => 'file', 'id' => 'form']);?>
        <div class="card-header">
            <h3 class="card-title">Logbook Form</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-9 my-2">
                    <?=$this->Form->label('quantity', ucwords('quantity ('.(intval($supply->stocks)).')'))?>
                    <?=$this->Form->number('quantity',[
                        'class' => 'form-control rounded-0',
                        'id' => 'quantity',
                        'required' => true,
                        'placeholder' => ucwords('quantity (').(intval($supply->stocks)).')',
                        'pattern' => '(.){1,}',
                        'title' => ucwords('Please Fill Out This Field'),
                        'min' => 1
                    ])?>
                    <small></small>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-start align-items-end my-2">
                    <div class="icheck-primary d-inline">
                        <?=$this->Form->checkbox('stored',[
                            'id' => 'stored',
                            'label' => false,
                            'hiddenField' => false,
                            'checked' => true,
                        ])?>
                        <?=$this->Form->label('stored', ucwords('Stored'))?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->hidden('user_id',[
                'id' => 'user-id',
                'required' => true,
                'readonly' => true,
                'value' => intval(@$auth['id'])
            ])?>
            <?=$this->Form->hidden('supply_id',[
                'id' => 'supply-id',
                'required' => true,
                'readonly' => true,
                'value' => intval($supply->id)
            ])?>
            <?= $this->Form->hidden('is_stored',[
                'id' => 'is-stored',
                'value' => intval(1)
            ]);?>
            <?=$this->Form->button(ucwords('Reset'),[
                'class' => 'btn btn-danger rounded-0 m-1',
                'type' => 'reset',
                'title' => ucwords('Reset')
            ])?>
            <?=$this->Form->button(ucwords('Submit'),[
                'class' => 'btn btn-success rounded-0 m-1',
                'type' => 'submit',
                'title' => ucwords('Submit')
            ])?>
        </div>
        <?= $this->Form->end();?>
    </div>
</div>

<?=$this->Html->script('secretariat/logbooks/add')?>
