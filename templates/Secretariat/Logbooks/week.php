<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Logbook $logbook
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $supplies
 */
?>

<?=$this->Form->create(null,['type' => 'file', 'id' => 'form', 'class' => 'row'])?>
<div class="col-sm-12 col-md-12 col-lg-12 mt-3">
    <div class="card p-3">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Supply</th>
                    <th>Logbook</th>
                    <th>Quantity</th>
                    <th>Old Stocks</th>
                    <th>Total Stocks</th>
                    <th>Is Stored</th>
                    <th>Process By</th>
                    <th>Modified</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<?=$this->Form->end()?>

<?=$this->Html->script('secretariat/logbooks/week')?>
