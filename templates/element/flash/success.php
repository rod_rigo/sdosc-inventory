<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>

<script>
    'use strict';
    $(function (e) {
        Swal.fire({
            icon: 'success',
            title: '<?= ucwords($message) ?>',
            text: null,
            allowOutsideClick: false,
            showConfirmButton: true,
            timerProgressBar: true,
            timer:parseInt(3000)
        });
    });
</script>
