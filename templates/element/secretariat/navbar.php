<?php
/**
 * @var \App\View\AppView $this
 */
?>

<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="javascript:void(0);" role="button">
                <i class="fas fa-bars"></i>
            </a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Users', 'action' => 'account', intval(@$auth['id'])])?>" link class="nav-link">Account</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Custodians', 'action' => 'add'])?>" link class="nav-link">New Custodian</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="javascript:void(0);" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
    </ul>
</nav>
