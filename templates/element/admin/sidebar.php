<?php
/**
 * @var \App\View\AppView $this
 */
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="javascript:void(0);" class="brand-link">
        <img src="<?=$this->Url->assetUrl('img/'.(@$logos->logo))?>" alt="SDO Logo" class="brand-image img-thumbnail elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><?=ucwords(@$logos->title)?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar p-0">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 pl-2 mb-3 d-flex">
            <div class="image">
                <img src="<?=$this->Url->assetUrl('/img/user-avatar/'.(strval(@$auth['id'])).'.png')?>?time=<?=(time())?>" class="img-circle elevation-2" loading="lazy" alt="User Image">
            </div>
            <div class="info">
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'account', intval(@$auth['id'])])?>" turbolink class="d-block"><?=@($auth['name'])?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column justify-content-center align-items-center" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header text-left w-100">Navigation</li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Dashboards')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-header text-left w-100">Issuances</li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('Custodians'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('Custodians'))? 'active': null;?>">
                        <i class="nav-icon fas fa-file"></i>
                        <p>
                            Custodians
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Custodians') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Custodians') && strtolower($action) == strtolower('today'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Custodians') && strtolower($action) == strtolower('week'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Custodians') && strtolower($action) == strtolower('month'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Custodians', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Custodians') && strtolower($action) == strtolower('year'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('Slips'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('Slips'))? 'active': null;?>">
                        <i class="nav-icon fas fa-clone"></i>
                        <p>
                            Slips
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Slips', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Slips') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Slips', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Slips') && strtolower($action) == strtolower('today'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Slips', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Slips') && strtolower($action) == strtolower('week'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Slips', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Slips') && strtolower($action) == strtolower('month'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Slips', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Slips') && strtolower($action) == strtolower('year'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('Items'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('Items'))? 'active': null;?>">
                        <i class="nav-icon fas fa-briefcase"></i>
                        <p>
                            Items
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Items', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Items') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Items', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Items') && strtolower($action) == strtolower('today'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Items', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Items') && strtolower($action) == strtolower('week'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Items', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Items') && strtolower($action) == strtolower('month'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Items', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Items') && strtolower($action) == strtolower('year'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header text-left w-100">Reports</li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('Logbooks'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('Logbooks'))? 'active': null;?>">
                        <i class="nav-icon fa fa-file-pdf"></i>
                        <p>
                            Logbooks
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logbooks', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Logbooks') && strtolower($action) == strtolower('index'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logbooks', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Logbooks') && strtolower($action) == strtolower('today'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logbooks', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Logbooks') && strtolower($action) == strtolower('week'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logbooks', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Logbooks') && strtolower($action) == strtolower('month'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logbooks', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Logbooks') && strtolower($action) == strtolower('year'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Reports', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Reports')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-file-excel"></i>
                        <p>Reports</p>
                    </a>
                </li>

                <li class="nav-header text-left w-100">Inventory</li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Accounts', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('accounts')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-table"></i>
                        <p>Accounts</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ledgers', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Ledgers')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-th-large"></i>
                        <p>Ledgers</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Units', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('units')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-balance-scale"></i>
                        <p>Units</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Categories', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Categories')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>Categories</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Brands', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Brands')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>Brands</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('supplies')]) && in_array(strtolower($action),[strtolower('index'), strtolower('view')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-boxes"></i>
                        <p>Supplies</p>
                    </a>
                </li>

                <li class="nav-header text-left w-100">Management</li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Institutions', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Institutions')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Institutions</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Offices', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Offices')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>Offices</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Positions')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>Positions</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Personels', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Personels')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Personels</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'FundClusters', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('FundClusters')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-money-bill"></i>
                        <p>Fund Clusters</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Users')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($action) == strtolower('bin'))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($action) == strtolower('bin'))? 'active': null;?>">
                        <i class="nav-icon fas fa-trash"></i>
                        <p>
                            Bin
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Accounts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('accounts') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Accounts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Ledgers', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Ledgers') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ledgers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Units', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Units') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Units</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Categories', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Categories') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Brands', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Brands') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Brands</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Supplies', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Supplies') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Supplies</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Offices', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Offices') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Offices</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Positions') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Positions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Personels', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Personels') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Personels</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logos', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Logos') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Logos</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Headings', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Headings') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Headings</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Signatories', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Signatories') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Signatories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'FundClusters', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('FundClusters') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Fund Clusters</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Institutions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower($controller) == strtolower('Institutions') && strtolower($action) == strtolower('bin'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Institutions</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Settings', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('Settings')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>Settings</p>
                    </a>
                </li>

                <li class="nav-header text-left w-100">Others</li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Logos', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('logos')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-image"></i>
                        <p>Logos</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Headings', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('headings')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-paragraph"></i>
                        <p>Headings</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Signatories', 'action' => 'index'])?>" class="nav-link <?=(in_array(strtolower($controller),[strtolower('signatories')]) && in_array(strtolower($action),[strtolower('index')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>Signatories</p>
                    </a>
                </li>

                <li class="nav-header text-left w-100"></li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
